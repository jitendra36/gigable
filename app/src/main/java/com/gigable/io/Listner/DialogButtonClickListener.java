package com.gigable.io.Listner;

public interface DialogButtonClickListener {

	public void onPositiveButtonClicked(int dialogIdentifier);
	public void onNegativeButtonClicked(int dialogIdentifier);
}
