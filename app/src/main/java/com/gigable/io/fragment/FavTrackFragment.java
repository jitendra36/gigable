package com.gigable.io.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.Models.ServiceResponse.FavTrackResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.adapters.FavTrackListAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Citrsubug on 11/10/2016.
 */
public class FavTrackFragment extends Fragment implements ApiResponse{

    RecyclerView rv;
    public TextView total_track;

    FavTrackResponse favTrackResponse;
    FavTrackListAdapter adapter;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /** Inflating the layout for this fragment **/
        View v = inflater.inflate(R.layout.favtracklist,container,false);

        total_track = (TextView)v.findViewById(R.id.total_track);
        rv = (RecyclerView)v.findViewById(R.id.rv);
        rv.setHasFixedSize(true);


        final LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);


        PlayListRequest playListRequest = new PlayListRequest();
        playListRequest.setUsrplay_lgn_id("1");

        HTTPWebRequest.GetFavtrack(getActivity(), playListRequest, AppConstants.APICode.fav_trklst, FavTrackFragment.this);

        return v;
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.fav_trklst:
                        favTrackResponse = gson.fromJson(response, FavTrackResponse.class);

                        setTotal_track(favTrackResponse.getCOUNT());

                        if (favTrackResponse.getDATA().size() > 0) {

                            setupAdapter(favTrackResponse.getDATA());
                        }

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }


    public void setTotal_track(String track) {
        total_track.setText(track+" Track in my favourit");
    }

    private void setupAdapter(final ArrayList<Track> mData) {
        if(mData.size()>0) {
            adapter = new FavTrackListAdapter(context,getActivity(),FavTrackFragment.this, mData);
            rv.setAdapter(adapter);
        }
    }


    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }


}
