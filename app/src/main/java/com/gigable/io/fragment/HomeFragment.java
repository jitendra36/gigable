package com.gigable.io.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceResponse.PlaylistResponse;
import com.gigable.io.R;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Citrsubug on 11/10/2016.
 */
public class HomeFragment extends Fragment implements ApiResponse {


    private ImageButton btnPlaylist;
    PlaylistResponse playlistResponse = new PlaylistResponse();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /** Inflating the layout for this fragment **/
        View v = inflater.inflate(R.layout.player, null);


        PlayListRequest playListRequest = new PlayListRequest();
        playListRequest.setLgn_id("1");
        playListRequest.setDistance_type("2");
        playListRequest.setLatitude("72.565124");
        playListRequest.setLongitude("72.565124");
        playListRequest.setMonth_range("3");
        playListRequest.setPlaylist_distance("300");

        //HTTPWebRequest.GetPlaylistHome(getActivity(), playListRequest, AppConstants.APICode.playlist_home, HomeFragment.this);

        return v;
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {

                switch (apiCode) {
                    case AppConstants.APICode.playlist_home:

                        playlistResponse = gson.fromJson(response, PlaylistResponse.class);

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }
}
