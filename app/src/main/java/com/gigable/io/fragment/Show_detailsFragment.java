package com.gigable.io.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceResponse.ShowDetailsResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

/**
 * Created by Citrsubug on 11/10/2016.
 */
public class Show_detailsFragment extends Fragment implements ApiResponse{


    TextView show_title,show_venu,show_date,sho_dec;
    ImageView tkt;
    String art_id_intent;

    public Show_detailsFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /** Inflating the layout for this fragment **/
        View v = inflater.inflate(R.layout.show_details,container,false);

        show_title = (TextView)v.findViewById(R.id.show_title);
        show_venu = (TextView)v.findViewById(R.id.show_venu);
        show_date = (TextView)v.findViewById(R.id.show_date);
        sho_dec = (TextView)v.findViewById(R.id.sho_dec);
        tkt = (ImageView)v.findViewById(R.id.tkt);



        Bundle bundle = this.getArguments();
        if (bundle != null) {
            art_id_intent = bundle.getString("art_id", null);
        }
        Show show = new Show();
        show.setShow_id("2");
        show.setLgn_id("1");

        HTTPWebRequest.ShowDetails(getActivity(), show, AppConstants.APICode.show_detail, Show_detailsFragment.this);

        return v;
    }


    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.show_detail:

                        ShowDetailsResponse showDetailsResponse = new ShowDetailsResponse();
                        showDetailsResponse = gson.fromJson(response, ShowDetailsResponse.class);

                        if (showDetailsResponse.getSTATUS().equals("SUCCESS")) {
                            //Toast.makeText(LoginActivity.this, loginResponse2.getMESSAGES(), Toast.LENGTH_LONG).show();

                            //set responcer data object in loginrequest
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Show.class, new ShowDetailsResponse())
                                    .create();

                            Show show = gson.fromJson(response, Show.class);

                            if (show.getShow_title() != null)show_title.setText(show.getShow_title());
                            if (show.getVen_name() != null)show_venu.setText(show.getVen_name());
                            if (show.getShow_date() != null)show_date.setText(show.getShow_date());
                            if (show.getShow_desc() != null)sho_dec.setText(show.getShow_desc());
                        }

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }


    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }
}
