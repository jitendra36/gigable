package com.gigable.io.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Citrsubug on 14-02-2017.
 */

public class common extends Application {

    private static SharedPreferences mSharedPreferences;
    Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        //Application context.
        mContext = getApplicationContext();
        mSharedPreferences = this.getSharedPreferences("com.example.citrsubug.gigable", Context.MODE_PRIVATE);
    }
    public static SharedPreferences getSharedPreferences() {
        return mSharedPreferences;
    }

    public static boolean isEqualizerEnabled() {
        return getSharedPreferences().getBoolean("EQUALIZER_ENABLED", true);
    }
}
