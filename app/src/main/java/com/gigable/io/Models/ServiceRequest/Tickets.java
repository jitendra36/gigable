package com.gigable.io.Models.ServiceRequest;

/**
 * Created by Citrsubug on 11/30/2016.
 */

public class Tickets {

    public String ticket_id;
    public String ticket_type_id;
    public String ticket_type_label;
    public String ticket_price;
    public String ticket_qty;
    public String ticket_booked;
    public String show_title;



    public String utkt_lgn_id;
    public String utkt_tkt_id;
    public String utkt_qty;

    public String total_amount;

    public String lgn_id;
    public String utkt_id;

    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }

    public String getUtkt_id() {
        return utkt_id;
    }

    public void setUtkt_id(String utkt_id) {
        this.utkt_id = utkt_id;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getUtkt_lgn_id() {
        return utkt_lgn_id;
    }

    public void setUtkt_lgn_id(String utkt_lgn_id) {
        this.utkt_lgn_id = utkt_lgn_id;
    }

    public String getUtkt_tkt_id() {
        return utkt_tkt_id;
    }

    public void setUtkt_tkt_id(String utkt_tkt_id) {
        this.utkt_tkt_id = utkt_tkt_id;
    }

    public String getUtkt_qty() {
        return utkt_qty;
    }

    public void setUtkt_qty(String utkt_qty) {
        this.utkt_qty = utkt_qty;
    }

    public String getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(String ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getTicket_type_id() {
        return ticket_type_id;
    }

    public void setTicket_type_id(String ticket_type_id) {
        this.ticket_type_id = ticket_type_id;
    }

    public String getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(String ticket_price) {
        this.ticket_price = ticket_price;
    }

    public String getTicket_type_label() {
        return ticket_type_label;
    }

    public void setTicket_type_label(String ticket_type_label) {
        this.ticket_type_label = ticket_type_label;
    }

    public String getTicket_qty() {
        return ticket_qty;
    }

    public void setTicket_qty(String ticket_qty) {
        this.ticket_qty = ticket_qty;
    }

    public String getShow_title() {
        return show_title;
    }

    public void setShow_title(String show_title) {
        this.show_title = show_title;
    }

    public String getTicket_booked() {
        return ticket_booked;
    }

    public void setTicket_booked(String ticket_booked) {
        this.ticket_booked = ticket_booked;
    }
}
