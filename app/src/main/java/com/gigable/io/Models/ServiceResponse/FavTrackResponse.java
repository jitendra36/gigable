package com.gigable.io.Models.ServiceResponse;

import com.gigable.io.Models.ServiceRequest.Track;

import java.util.ArrayList;

public class FavTrackResponse {

    String STATUS;
    String MESSAGES;
    String COUNT;

    public String getCOUNT() {
        return COUNT;
    }

    public void setCOUNT(String COUNT) {
        this.COUNT = COUNT;
    }

    public String getMESSAGES() {
        return MESSAGES;
    }

    public void setMESSAGES(String MESSAGES) {
        this.MESSAGES = MESSAGES;
    }

    public ArrayList<Track> DATA=new ArrayList<>();

    public ArrayList<Track> getDATA() {
        return DATA;
    }

    public void setDATA(ArrayList<Track> DATA) {
        this.DATA = DATA;
    }


    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
