package com.gigable.io.Models.ServiceRequest;

import java.util.ArrayList;

/**
 * Created by Citrsubug on 11/22/2016.
 */

public class PlayListRequest {

    private String lgn_id;
    private String latitude;
    private String longitude;
    private String playlist_distance;
    private String distance_type;
    private String month_range;

    private String play_name;
    private String totaltrack;
    private String totalshow;
    private String play_id;
    private String utp_play_id;
    private String usrplay_lgn_id;
    private String image;

    private String loc_id;
    private String loc_latitude;
    private String loc_longitude;
    private String loc_name;
    private String artistname;
    private String trk_id;
    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTrk_id() {
        return trk_id;
    }

    public void setTrk_id(String trk_id) {
        this.trk_id = trk_id;
    }

    public ArrayList<Show> show=new ArrayList<>();

    public ArrayList<Show> getShow() {
        return show;
    }

    public void setShow(ArrayList<Show> show) {
        this.show = show;
    }

    public String getArtistname() {
        return artistname;
    }

    public void setArtistname(String artistname) {
        this.artistname = artistname;
    }

    public String getLoc_name() {
        return loc_name;
    }

    public void setLoc_name(String loc_name) {
        this.loc_name = loc_name;
    }

    public String getLoc_id() {
        return loc_id;
    }

    public void setLoc_id(String loc_id) {
        this.loc_id = loc_id;
    }

    public String getLoc_latitude() {
        return loc_latitude;
    }

    public void setLoc_latitude(String loc_latitude) {
        this.loc_latitude = loc_latitude;
    }

    public String getLoc_longitude() {
        return loc_longitude;
    }

    public void setLoc_longitude(String loc_longitude) {
        this.loc_longitude = loc_longitude;
    }

    public String getTotalshow() {
        return totalshow;
    }

    public void setTotalshow(String totalshow) {
        this.totalshow = totalshow;
    }

    public ArrayList<Track> artist = new ArrayList<>();

    public ArrayList<Track> getArtist() {
        return artist;
    }

    public void setArtist(ArrayList<Track> artist) {
        this.artist = artist;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsrplay_lgn_id() {
        return usrplay_lgn_id;
    }

    public void setUsrplay_lgn_id(String usrplay_lgn_id) {
        this.usrplay_lgn_id = usrplay_lgn_id;
    }


    public String getUtp_play_id() {
        return utp_play_id;
    }

    public void setUtp_play_id(String utp_play_id) {
        this.utp_play_id = utp_play_id;
    }

    public String getPlay_id() {
        return play_id;
    }

    public void setPlay_id(String play_id) {
        this.play_id = play_id;
    }

    public String getTotaltrack() {
        return totaltrack;
    }

    public void setTotaltrack(String totaltrack) {
        this.totaltrack = totaltrack;
    }

    public String getPlay_name() {
        return play_name;
    }

    public void setPlay_name(String play_name) {
        this.play_name = play_name;
    }

    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPlaylist_distance() {
        return playlist_distance;
    }

    public void setPlaylist_distance(String playlist_distance) {
        this.playlist_distance = playlist_distance;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance_type() {
        return distance_type;
    }

    public void setDistance_type(String distance_type) {
        this.distance_type = distance_type;
    }

    public String getMonth_range() {
        return month_range;
    }

    public void setMonth_range(String month_range) {
        this.month_range = month_range;
    }
}
