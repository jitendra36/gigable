package com.gigable.io.Models.ServiceResponse;

public class DelShowResponse {

    String STATUS;
    String MESSAGES;
    String DATA;


    public String getMESSAGES() {
        return MESSAGES;
    }

    public void setMESSAGES(String MESSAGES) {
        this.MESSAGES = MESSAGES;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getDATA() {
        return DATA;
    }

    public void setDATA(String DATA) {
        this.DATA = DATA;
    }
}
