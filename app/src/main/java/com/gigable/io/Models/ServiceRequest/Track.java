package com.gigable.io.Models.ServiceRequest;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Citrsubug on 11/23/2016.
 */

public class Track implements Serializable {


    public String trk_id;
    public String trk_audio;
    public String tkt_id;
    public String trk_title;
    public String totaltrack;

    public String art_id;
    public String art_name;
    public String art_image;
    public String art_shortbio;
    public String art_website;
    public String art_twitter;
    public String art_facebook;
    public String art_soundcloud;
    public String art_abclink;
    private String usrplay_lgn_id;
    private String name;
    private String image;
    private String play_name;
    private String artist_image;
    private String show_date;
    private String show_venue;
    private String show_price;

    public String favourite_status;
    public String fp_status;
    public String lgn_id;


    private boolean fav_status;

    public boolean isFav_status() {
        return fav_status;
    }

    public void setFav_status(boolean fav_status) {
        this.fav_status = fav_status;
    }

    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }

    public String getFp_status() {
        return fp_status;
    }

    public void setFp_status(String fp_status) {
        this.fp_status = fp_status;
    }

    public String getFavourite_status() {
        return favourite_status;
    }

    public void setFavourite_status(String favourite_status) {
        this.favourite_status = favourite_status;
    }

    public String getArtist_image() {
        return artist_image;
    }

    public void setArtist_image(String artist_image) {
        this.artist_image = artist_image;
    }

    public String getPlay_name() {
        return play_name;
    }

    public void setPlay_name(String play_name) {
        this.play_name = play_name;
    }

    public String getShow_date() {
        return show_date;
    }

    public void setShow_date(String show_date) {
        this.show_date = show_date;
    }

    public String getShow_venue() {
        return show_venue;
    }

    public void setShow_venue(String show_venue) {
        this.show_venue = show_venue;
    }

    public String getShow_price() {
        return show_price;
    }

    public void setShow_price(String show_price) {
        this.show_price = show_price;
    }

    public ArrayList<Show> Show=new ArrayList<>();
   // public ArrayList<Show> Track=new ArrayList<>();

    public ArrayList<Track> Track=new ArrayList<>();

    public ArrayList<Track> getTrack() {
        return Track;
    }

    public void setTrack(ArrayList<Track> track) {
        Track = track;
    }

    /* public ArrayList<Show> getTrack() {
        return Track;
    }

    public void setTrack(ArrayList<Show> track) {
        Track = track;
    }*/

    public ArrayList<Show> getShow() {
        return Show;
    }

    public void setShow(ArrayList<Show> show) {
        Show = show;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsrplay_lgn_id() {
        return usrplay_lgn_id;
    }

    public void setUsrplay_lgn_id(String usrplay_lgn_id) {
        this.usrplay_lgn_id = usrplay_lgn_id;
    }

    public String utp_trk_id;

    public String getUtp_trk_id() {
        return utp_trk_id;
    }

    public void setUtp_trk_id(String utp_trk_id) {
        this.utp_trk_id = utp_trk_id;
    }

    public String getTrk_id() {
        return trk_id;
    }

    public void setTrk_id(String trk_id) {
        this.trk_id = trk_id;
    }

    public String getArt_image() {
        return art_image;
    }

    public void setArt_image(String art_image) {
        this.art_image = art_image;
    }

    public String getArt_shortbio() {
        return art_shortbio;
    }

    public void setArt_shortbio(String art_shortbio) {
        this.art_shortbio = art_shortbio;
    }

    public String getArt_website() {
        return art_website;
    }

    public void setArt_website(String art_website) {
        this.art_website = art_website;
    }

    public String getArt_twitter() {
        return art_twitter;
    }

    public void setArt_twitter(String art_twitter) {
        this.art_twitter = art_twitter;
    }

    public String getArt_facebook() {
        return art_facebook;
    }

    public void setArt_facebook(String art_facebook) {
        this.art_facebook = art_facebook;
    }

    public String getArt_abclink() {
        return art_abclink;
    }

    public void setArt_abclink(String art_abclink) {
        this.art_abclink = art_abclink;
    }

    public String getArt_soundcloud() {
        return art_soundcloud;
    }

    public void setArt_soundcloud(String art_soundcloud) {
        this.art_soundcloud = art_soundcloud;
    }

    public String getArt_id() {
        return art_id;
    }

    public void setArt_id(String art_id) {
        this.art_id = art_id;
    }

    public String getTotaltrack() {
        return totaltrack;
    }

    public void setTotaltrack(String totaltrack) {
        this.totaltrack = totaltrack;
    }

    public String getArt_name() {
        return art_name;
    }

    public void setArt_name(String art_name) {
        this.art_name = art_name;
    }

    public String getTrk_audio() {
        return trk_audio;
    }

    public void setTrk_audio(String trk_audio) {
        this.trk_audio = trk_audio;
    }

    public String getTkt_id() {
        return tkt_id;
    }

    public void setTkt_id(String tkt_id) {
        this.tkt_id = tkt_id;
    }

    public String getTrk_title() {
        return trk_title;
    }

    public void setTrk_title(String trk_title) {
        this.trk_title = trk_title;
    }
}
