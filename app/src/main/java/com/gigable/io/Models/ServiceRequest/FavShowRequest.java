package com.gigable.io.Models.ServiceRequest;

/**
 * Created by Citrsubug on 12/17/2016.
 */

public class FavShowRequest {

    public String show_title;
    public String show_id;
    public String show_date;
    public String show_time;
    public String show_desc;
    public String ven_name;
    public String utkt_id;
    public String art_image;
    public String art_name;

    public String getArt_name() {
        return art_name;
    }

    public void setArt_name(String art_name) {
        this.art_name = art_name;
    }

    public String getArt_image() {
        return art_image;
    }

    public void setArt_image(String art_image) {
        this.art_image = art_image;
    }

    public String getUtkt_id() {
        return utkt_id;
    }

    public void setUtkt_id(String utkt_id) {
        this.utkt_id = utkt_id;
    }

    public String getShow_title() {
        return show_title;
    }

    public void setShow_title(String show_title) {
        this.show_title = show_title;
    }

    public String getShow_id() {
        return show_id;
    }

    public void setShow_id(String show_id) {
        this.show_id = show_id;
    }

    public String getShow_date() {
        return show_date;
    }

    public void setShow_date(String show_date) {
        this.show_date = show_date;
    }

    public String getShow_time() {
        return show_time;
    }

    public void setShow_time(String show_time) {
        this.show_time = show_time;
    }

    public String getShow_desc() {
        return show_desc;
    }

    public void setShow_desc(String show_desc) {
        this.show_desc = show_desc;
    }

    public String getVen_name() {
        return ven_name;
    }

    public void setVen_name(String ven_name) {
        this.ven_name = ven_name;
    }
}
