package com.gigable.io.Models.ServiceRequest;

public class DataRequest {
    private String lgn_id;
    private String lgn_fgtpass_verifycode;

    public String getLgn_fgtpass_verifycode() {
        return lgn_fgtpass_verifycode;
    }

    public void setLgn_fgtpass_verifycode(String lgn_fgtpass_verifycode) {
        this.lgn_fgtpass_verifycode = lgn_fgtpass_verifycode;
    }

    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }
}
