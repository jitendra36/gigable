package com.gigable.io.Models.ServiceResponse;

import com.gigable.io.Models.ServiceRequest.PlayListRequest;

import java.util.ArrayList;

public class ChangeLocationResponse {

    String STATUS;

    public ArrayList<PlayListRequest> DATA=new ArrayList<>();

    public ArrayList<PlayListRequest> getDATA() {
        return DATA;
    }

    public void setDATA(ArrayList<PlayListRequest> DATA) {
        this.DATA = DATA;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
}
