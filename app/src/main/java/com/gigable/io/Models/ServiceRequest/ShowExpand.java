package com.gigable.io.Models.ServiceRequest;

import com.gigable.io.ExpandableRecycle.Parent;

import java.util.List;

public class ShowExpand implements Parent<FavShowRequest> {

    private String mName;
    private String total_show;



    private List<FavShowRequest> favShowRequests;

    public ShowExpand(String name, List<FavShowRequest> favShowRequests, String size) {
        mName = name;
        this.favShowRequests = favShowRequests;
        total_show = size;
    }

    public String getName() {
        return mName;
    }

    public String getTotal_show() {
        return total_show;
    }

    @Override
    public List<FavShowRequest> getChildList() {
        return favShowRequests;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public FavShowRequest getIngredient(int position) {
        return favShowRequests.get(position);
    }


}
