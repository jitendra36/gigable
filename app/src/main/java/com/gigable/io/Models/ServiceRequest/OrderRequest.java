package com.gigable.io.Models.ServiceRequest;

/**
 * Created by Citrsubug on 12/2/2016.
 */

public class OrderRequest {

    private String OrderId;
    private String Price;
    private String Qty;

    private String lgn_id;
    private String od_id;
    private String token;

    public String getOd_id() {
        return od_id;
    }

    public void setOd_id(String od_id) {
        this.od_id = od_id;
    }

    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }
}
