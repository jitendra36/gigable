package com.gigable.io.Models.ServiceRequest;

public class RegisterRequest {

    private String usr_lgn_type;
    private String usr_name;
    private String usr_image;
    private String usr_phone;
    private String usr_gender;
    private String lgn_email;
    private String lgn_password;
    private String confirm_password;
    private String lgn_id;
    private String usr_imei_number;
    private String lgn_fb_auth_id;
    private String fr_response;
    private String lgn_google_auth_id;
    private String usr_id;
    private String usr_gcm_number;
    private String usr_type;

    public String getUsr_type() {
        return usr_type;
    }

    public void setUsr_type(String usr_type) {
        this.usr_type = usr_type;
    }

    public String getUsr_lgn_type() {
        return usr_lgn_type;
    }

    public void setUsr_lgn_type(String usr_lgn_type) {
        this.usr_lgn_type = usr_lgn_type;
    }

    public String getUsr_id() {
        return usr_id;
    }

    public void setUsr_id(String usr_id) {
        this.usr_id = usr_id;
    }

    public String getLgn_google_auth_id() {
        return lgn_google_auth_id;
    }

    public void setLgn_google_auth_id(String lgn_google_auth_id) {
        this.lgn_google_auth_id = lgn_google_auth_id;
    }

    public String getFr_response() {
        return fr_response;
    }

    public void setFr_response(String fr_response) {
        this.fr_response = fr_response;
    }

    public String getLgn_fb_auth_id() {
        return lgn_fb_auth_id;
    }

    public void setLgn_fb_auth_id(String lgn_fb_auth_id) {
        this.lgn_fb_auth_id = lgn_fb_auth_id;
    }

    public String getUsr_image() {
        return usr_image;
    }

    public void setUsr_image(String usr_image) {
        this.usr_image = usr_image;
    }

    public String getConfirm_password() {
        return confirm_password;
    }

    public void setConfirm_password(String confirm_password) {
        this.confirm_password = confirm_password;
    }



    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }

    public String getUsr_name() {
        return usr_name;
    }

    public void setUsr_name(String usr_name) {
        this.usr_name = usr_name;
    }

    public String getUsr_phone() {
        return usr_phone;
    }

    public void setUsr_phone(String usr_phone) {
        this.usr_phone = usr_phone;
    }

    public String getUsr_gender() {
        return usr_gender;
    }

    public void setUsr_gender(String usr_gender) {
        this.usr_gender = usr_gender;
    }

    public String getUsr_imei_number() {
        return usr_imei_number;
    }

    public void setUsr_imei_number(String usr_imei_number) {
        this.usr_imei_number = usr_imei_number;
    }

    public String getUsr_gcm_number() {
        return usr_gcm_number;
    }

    public void setUsr_gcm_number(String usr_gcm_number) {
        this.usr_gcm_number = usr_gcm_number;
    }

    public String getLgn_email() {
        return lgn_email;
    }

    public void setLgn_email(String lgn_email) {
        this.lgn_email = lgn_email;
    }

    public String getLgn_password() {
        return lgn_password;
    }

    public void setLgn_password(String lgn_password) {
        this.lgn_password = lgn_password;
    }

}
