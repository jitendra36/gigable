package com.gigable.io.Models.ServiceResponse;

import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Track;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PlaylistResponse  implements JsonDeserializer<PlayListRequest> {

    String STATUS;

    public ArrayList<PlayListRequest> DATA=new ArrayList<>();

    public ArrayList<PlayListRequest> getDATA() {
        return DATA;
    }

    public void setDATA(ArrayList<PlayListRequest> DATA) {
        this.DATA = DATA;
    }

    public ArrayList<Track> FEATURE=new ArrayList<>();

    public ArrayList<Track> getFEATURE()
    {
        return FEATURE;
    }

    public void setFEATURE(ArrayList<Track> FEATURE)
    {
        this.FEATURE = FEATURE;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    @Override
    public PlayListRequest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement content = json.getAsJsonObject().get("UPCOMING_CONCERTS");

        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer
        return new Gson().fromJson(content, PlayListRequest.class);
    }
}
