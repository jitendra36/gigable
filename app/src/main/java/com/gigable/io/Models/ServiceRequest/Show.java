package com.gigable.io.Models.ServiceRequest;

import java.util.ArrayList;

/**
 * Created by Citrsubug on 11/23/2016.
 */

public class Show {

    public String show_id;
    public String lgn_id;
    public String show_time;
    public String show_title;
    public String ven_name;
    public String ven_location;
    public String ven_city;
    public String show_date;
    public String show_desc;
    public String msf_lgn_id;
    public String msf_show_id;
    public String date;
    public String venue;
    public String price;
    public String msf_show_fav_status;

    public String show_price;
    public String show_venue;


    public String trk_title;
    public String play_name;

    public String art_name;
    public String art_image;

    public String utkt_qty;
    public String utkt_price;


    public String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUtkt_price() {
        return utkt_price;
    }

    public void setUtkt_price(String utkt_price) {
        this.utkt_price = utkt_price;
    }

    public String getUtkt_qty() {
        return utkt_qty;
    }

    public void setUtkt_qty(String utkt_qty) {
        this.utkt_qty = utkt_qty;
    }

    public String getArt_name() {
        return art_name;
    }

    public void setArt_name(String art_name) {
        this.art_name = art_name;
    }

    public String getArt_image() {
        return art_image;
    }

    public void setArt_image(String art_image) {
        this.art_image = art_image;
    }

    public String getTrk_title() {
        return trk_title;
    }

    public void setTrk_title(String trk_title) {
        this.trk_title = trk_title;
    }

    public String getPlay_name() {
        return play_name;
    }

    public void setPlay_name(String play_name) {
        this.play_name = play_name;
    }

    public String getShow_venue() {
        return show_venue;
    }

    public void setShow_venue(String show_venue) {
        this.show_venue = show_venue;
    }

    public String getShow_price() {
        return show_price;
    }

    public void setShow_price(String show_price) {
        this.show_price = show_price;
    }

    //  public String artist;


    public ArrayList<Track> artist=new ArrayList<>();

    public ArrayList<Track> getArtist() {
        return artist;
    }

    public void setArtist(ArrayList<Track> artist) {
        this.artist = artist;
    }

    public String getMsf_show_fav_status() {
        return msf_show_fav_status;
    }

    public void setMsf_show_fav_status(String msf_show_fav_status) {
        this.msf_show_fav_status = msf_show_fav_status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVen_location() {
        return ven_location;
    }

    public void setVen_location(String ven_location) {
        this.ven_location = ven_location;
    }

    public String getVen_city() {
        return ven_city;
    }

    public void setVen_city(String ven_city) {
        this.ven_city = ven_city;
    }

//    public String getArtist() {
   //     return artist;
  //  }

//    public void setArtist(String artist) {
  //      this.artist = artist;
 //   }

    public ArrayList<Tickets> Tickets = new ArrayList<>();

    public ArrayList<FavShowRequest> ticketArray = new ArrayList<>();
    public ArrayList<FavShowRequest> currentArray = new ArrayList<>();
    public ArrayList<FavShowRequest> pastArray = new ArrayList<>();

    public String getShow_desc() {
        return show_desc;
    }

    public void setShow_desc(String show_desc) {
        this.show_desc = show_desc;
    }

    public ArrayList<FavShowRequest> getTicketArray() {
        return ticketArray;
    }

    public void setTicketArray(ArrayList<FavShowRequest> ticketArray) {
        this.ticketArray = ticketArray;
    }

    public ArrayList<FavShowRequest> getCurrentArray() {
        return currentArray;
    }

    public void setCurrentArray(ArrayList<FavShowRequest> currentArray) {
        this.currentArray = currentArray;
    }

    public ArrayList<FavShowRequest> getPastArray() {
        return pastArray;
    }

    public void setPastArray(ArrayList<FavShowRequest> pastArray) {
        this.pastArray = pastArray;
    }

    public String getMsf_lgn_id() {
        return msf_lgn_id;
    }

    public void setMsf_lgn_id(String msf_lgn_id) {
        this.msf_lgn_id = msf_lgn_id;
    }

    public ArrayList<Tickets> getTickets() {
        return Tickets;
    }

    public void setTickets(ArrayList<Tickets> tickets) {
        Tickets = tickets;
    }

    public String getShow_title() {
        return show_title;
    }

    public void setShow_title(String show_title) {
        this.show_title = show_title;
    }

    public String getVen_name() {
        return ven_name;
    }

    public void setVen_name(String ven_name) {
        this.ven_name = ven_name;
    }

    public String getShow_date() {
        return show_date;
    }

    public void setShow_date(String show_date) {
        this.show_date = show_date;
    }

    public String getShow_id() {
        return show_id;
    }

    public void setShow_id(String show_id) {
        this.show_id = show_id;
    }

    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }

    public String getMsf_show_id() {
        return msf_show_id;
    }

    public void setMsf_show_id(String msf_show_id) {
        this.msf_show_id = msf_show_id;
    }

    public String getShow_time() {
        return show_time;
    }

    public void setShow_time(String show_time) {
        this.show_time = show_time;
    }
}
