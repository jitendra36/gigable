package com.gigable.io.Models.ServiceResponse;

import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.Track;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Citrsubug on 11/24/2016.
 */
public class DATAContainer implements JsonDeserializer<Show>
{

    public ArrayList<Track> Track;

    public ArrayList<Show> Show;


    @Override
    public Show deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement content = json.getAsJsonObject().get("additional_info");

        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer
        return new Gson().fromJson(content, Show.class);
    }


}
