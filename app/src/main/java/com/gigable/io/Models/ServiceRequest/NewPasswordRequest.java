package com.gigable.io.Models.ServiceRequest;

public class NewPasswordRequest {

    private String new_password;
    private String lgn_id;
    private String cnfm_password;
    private String lgn_password;


    public String getLgn_password() {
        return lgn_password;
    }

    public void setLgn_password(String lgn_password) {
        this.lgn_password = lgn_password;
    }

    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getCnfm_password() {
        return cnfm_password;
    }

    public void setCnfm_password(String cnfm_password) {
        this.cnfm_password = cnfm_password;
    }
}
