package com.gigable.io.Models.ServiceResponse;

import com.gigable.io.Models.ServiceRequest.Show;

import java.util.ArrayList;

public class NotificationResponce {

    String STATUS;
    String MESSAGES;

    public ArrayList<Show> DATA=new ArrayList<>();

    public ArrayList<Show> getDATA() {
        return DATA;
    }

    public void setDATA(ArrayList<Show> DATA) {
        this.DATA = DATA;
    }
}
