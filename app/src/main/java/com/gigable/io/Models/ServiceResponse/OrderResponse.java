package com.gigable.io.Models.ServiceResponse;

import com.gigable.io.Models.ServiceRequest.LoginRequest;
import com.gigable.io.Models.ServiceRequest.OrderRequest;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class OrderResponse implements JsonDeserializer<OrderRequest> {

    String STATUS;
    String MESSAGES;


    public String getMESSAGES() {
        return MESSAGES;
    }

    public void setMESSAGES(String MESSAGES) {
        this.MESSAGES = MESSAGES;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }
    @Override
    public OrderRequest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement content = json.getAsJsonObject().get("DATA");

        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer
        return new Gson().fromJson(content, OrderRequest.class);
    }
}
