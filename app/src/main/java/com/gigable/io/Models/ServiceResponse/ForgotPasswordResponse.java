package com.gigable.io.Models.ServiceResponse;

import com.gigable.io.Models.ServiceRequest.DataRequest;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ForgotPasswordResponse implements JsonDeserializer<DataRequest> {

    String STATUS;
    String MESSAGES;

    public String getMESSAGES() {
        return MESSAGES;
    }

    public void setMESSAGES(String MESSAGES) {
        this.MESSAGES = MESSAGES;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    @Override
    public DataRequest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonElement content = json.getAsJsonObject().get("DATA");

        // Deserialize it. You use a new instance of Gson to avoid infinite recursion
        // to this deserializer
        return new Gson().fromJson(content, DataRequest.class);
    }
}
