package com.gigable.io.Models.ServiceRequest;

/**
 * Created by Citrsubug on 11/22/2016.
 */

public class TrackListRequest {

    private String lgn_id;
    private String latitude;
    private String longitude;
    private String playlist_distance;
    private String distance_type;
    private String month_range;

    private String play_name;
    private String totaltrack;
    private String play_id;
    private String utp_play_id;

    public String getUtp_play_id() {
        return utp_play_id;
    }

    public void setUtp_play_id(String utp_play_id) {
        this.utp_play_id = utp_play_id;
    }

    public String getPlay_id() {
        return play_id;
    }

    public void setPlay_id(String play_id) {
        this.play_id = play_id;
    }

    public String getTotaltrack() {
        return totaltrack;
    }

    public void setTotaltrack(String totaltrack) {
        this.totaltrack = totaltrack;
    }

    public String getPlay_name() {
        return play_name;
    }

    public void setPlay_name(String play_name) {
        this.play_name = play_name;
    }

    public String getLgn_id() {
        return lgn_id;
    }

    public void setLgn_id(String lgn_id) {
        this.lgn_id = lgn_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPlaylist_distance() {
        return playlist_distance;
    }

    public void setPlaylist_distance(String playlist_distance) {
        this.playlist_distance = playlist_distance;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance_type() {
        return distance_type;
    }

    public void setDistance_type(String distance_type) {
        this.distance_type = distance_type;
    }

    public String getMonth_range() {
        return month_range;
    }

    public void setMonth_range(String month_range) {
        this.month_range = month_range;
    }
}
