package com.gigable.io.Utility;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class Utility {
    static int pos = 0;
	Context _context;
	static Dialog dialog;
	public Utility(Context c){
		this._context=c;
	}

    public static int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;
        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage
        return percentage.intValue();
    }


    public static String getResponseString(String requestURL) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(35000);
            conn.setConnectTimeout(35000);
			/*conn.setReadTimeout(100);
			conn.setConnectTimeout(100);*/
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else
            {
                response="";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }
    public static String uploadFile(String requestURL, HashMap<String, String> postDataParams, ArrayList<HashMap<String, String>> imageArry) {
        // TODO Auto-generated method stub
        String req="data is sended successfully";
        String serverResponseMessage = "";
        String response_return = "";
        URL imageUrl = null;
        try {
            imageUrl = new URL(requestURL); // get WebService_URL
        } catch (MalformedURLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        // generating byte[] boundary here

        HttpURLConnection conn = null;
        DataOutputStream outputStream = null;

        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 10 * 1024 * 1024;

        try {

            int serverResponseCode;
            conn = (HttpURLConnection) imageUrl.openConnection();
            conn.setConnectTimeout(500000);
            conn.setReadTimeout(500000);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("http.keepAlive", "false");
            //conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            outputStream = new DataOutputStream(conn.getOutputStream());

            //////////////////////////////////////////////////////

            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            //outputStream.writeBytes("Content-Disposition: form-data; name=\"video\""+ lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"json\"" + lineEnd);
            outputStream.writeBytes("Content-Type: text/plain;charset=UTF-8" + lineEnd);
            outputStream.writeBytes("Content-Length: " + req.length() + lineEnd);
            //outputStream.writeBytes(""+("Title=Hi").getBytes());
            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(req + lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            // json String request

            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            Log.i("images path==>", "" + imageArry); // get photo
            for (int i = 0; i < imageArry.size(); i++) {
                try {
                    String file_extension = imageArry.get(i).get("file_extension");
                    String filePath = imageArry.get(i).get("uploadedPath");
                    Log.d("filePath==>", imageArry.get(i).get("uploadedPath") + "");
                    FileInputStream fileInputStream = new FileInputStream(filePath);
                    String lastOne = "Image";

                    /////////////////////////////////////////////
                    outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                    outputStream.writeBytes("Content-Disposition: attachment;  name=usr_image;" + "filename=" + lastOne + i + file_extension + lineEnd); // pass key & value of photo

                    outputStream.writeBytes(lineEnd);
                    bytesAvailable = fileInputStream.available(); // photo size bytes
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    // Read file
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    while (bytesRead > 0) {
                        outputStream.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }
                    outputStream.writeBytes(lineEnd);

                    Log.d("posttemplate", "connection outputstream size is " + outputStream.size());
                    fileInputStream.close();
                } catch (OutOfMemoryError o) {
                    continue;
                }
            }

            //========== upload post data =========

            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            // for (Map.Entry<String,String> entry : postDataParams.entrySet()) {
            //System.out.printf("%s -> %s%n", entry.getKey(), entry.getValue());
            Iterator myVeryOwnIterator = postDataParams.keySet().iterator();
            while(myVeryOwnIterator.hasNext()) {
                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                String key=(String)myVeryOwnIterator.next();
                String value=(String)postDataParams.get(key);

                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(value);
                outputStream.writeBytes(lineEnd);
            }

            //=======================================

            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            serverResponseCode = conn.getResponseCode();
            serverResponseMessage = conn.getResponseMessage();
            InputStream is = conn.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response1 = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response1.append(line);
                response1.append('\r');
            }
            rd.close();
            response_return = response1.toString();

            Log.d("posttemplate", "server response code " + serverResponseCode);
            Log.d("posttemplate", "server response message "
                    + serverResponseMessage);

            outputStream.flush();
            outputStream.close();

            conn.disconnect();

        } catch (MalformedURLException e) {
            Log.d("posttemplate", "malformed url", e);
        } catch (IOException e) {
            Log.d("posttemplate", "ioexception", e);
        }
        Log.d("response--->", "****" + response_return);
        return response_return;
    }

    public static String getResponseStringUsingPostMethod(String requestURL, HashMap<String, String> postDataParams) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(35000);
            conn.setConnectTimeout(35000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else
            {
                response="";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }


    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        Log.d("post string","post string=="+result.toString());
        return result.toString();
    }

    public static final ProgressDialog getProgressDialog(Context context, String dialogMessage) {

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(dialogMessage);
        return progressDialog;
    }
    public static void RedirectToActivity(Activity yourActivity, Class SecondActivity,
                                          boolean isfinish, Bundle b)
    {
        Intent intent = new Intent(yourActivity, SecondActivity);

        if (b != null)
            intent.putExtras(b);

        yourActivity.startActivity(intent);

        yourActivity.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        if (isfinish) {
            yourActivity.finish();
        }

    }
    public static void showToast(View view, String message)
    {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public static int getPxFromDp(Context context, float dpUnits) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpUnits, context.getResources().getDisplayMetrics());
    }


    public static String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        //Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Pre appending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double) progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }


    public static boolean currentVersionSupportBigNotification() {
        int sdkVersion = android.os.Build.VERSION.SDK_INT;
        if(sdkVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN){
            return true;
        }
        return false;
    }

    public static boolean currentVersionSupportLockScreenControls() {
        int sdkVersion = android.os.Build.VERSION.SDK_INT;
        if(sdkVersion >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            return true;
        }
        return false;
    }

    public static String getDuration(long milliseconds) {
        long sec = (milliseconds / 1000) % 60;
        long min = (milliseconds / (60 * 1000))%60;
        long hour = milliseconds / (60 * 60 * 1000);

        String s = (sec < 10) ? "0" + sec : "" + sec;
        String m = (min < 10) ? "0" + min : "" + min;
        String h = "" + hour;

        String time = "";
        if(hour > 0) {
            time = h + ":" + m + ":" + s;
        } else {
            time = m + ":" + s;
        }
        return time;
    }


    /**
     * Check if service is running or not
     * @param serviceName
     * @param context
     * @return
     */
    public static boolean isServiceRunning(String serviceName, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for(ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if(serviceName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


}
