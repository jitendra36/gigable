package com.gigable.io.Utility;

import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;


import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.activity.MediaPlayerService;

/**
 * Created by Citrsubug on 15-02-2017.
 */

public class Common extends Application {
    //Context.
    private Context mContext;

    //Service reference and flags.
    private MediaPlayerService mService;
    private boolean mIsServiceRunning = false;

    public SeekBar songProgressBar;
    public TextView songCurrentDurationLabel;
    public TextView songTotalDurationLabel;

    public static final String UPDATE_UI_BROADCAST1 = "com.gigable.io.NEW_SONG_UPDATE_UI";
    public static final String UPDATE_SEEKBAR_DURATION = "UpdateSeekbarDuration";
    public static final String UPDATE_TRACK_TITLE = "UpdateTrackTitle";
    public static final String UPDATE_ARTIST_NAME = "UpdateArtistName";
    public static final String UPDATE_TRACK_ID = "UpdateTrackId";
    public static final String UPDATE_FAVOURITE = "UpdateFavourite";
    public static final String UPDATE_TRACKURL = "UpdateTrackUrl";
    public static final String UPDATE_PLAYERUI_NEXT = "UpdatePlayerUINext";
    public static final String UPDATE_HIDE_PROGRESS_DIALOG = "UpdateHideProgressDialog";

    public static final String UPDATE_SONG_COMPLETE = "UpdateSongComplete";

    //Broadcast elements.
    private LocalBroadcastManager mLocalBroadcastManager;


    private static boolean songPlay;

    public static boolean isSongPlay() {
        return songPlay;
    }

    public static void setSongPlay(boolean songPlay) {
        Common.songPlay = songPlay;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //Application context.
        mContext = getApplicationContext();

        AppConstants.CONTEXT = mContext;

    }


    /**
     * Sends out a local broadcast that notifies all receivers to update
     * their respective UI elements.
     */
    public void broadcastUpdateUICommand(String[] updateFlags, String[] flagValues) {
        Intent intent = new Intent(UPDATE_UI_BROADCAST1);
        for (int i=0; i < updateFlags.length; i++) {
            intent.putExtra(updateFlags[i], flagValues[i]);
        }

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(mContext);
        mLocalBroadcastManager.sendBroadcast(intent);

    }



    /**
     * Converts milliseconds to hh:mm:ss format.
     */
    public String convertMillisToMinsSecs(long milliseconds) {

        int secondsValue = (int) (milliseconds / 1000) % 60 ;
        int minutesValue = (int) ((milliseconds / (1000*60)) % 60);
        int hoursValue  = (int) ((milliseconds / (1000*60*60)) % 24);

        String seconds = "";
        String minutes = "";
        String hours = "";

        if (secondsValue < 10) {
            seconds = "0" + secondsValue;
        } else {
            seconds = "" + secondsValue;
        }

        if (minutesValue < 10) {
            minutes = "0" + minutesValue;
        } else {
            minutes = "" + minutesValue;
        }

        if (hoursValue < 10) {
            hours = "0" + hoursValue;
        } else {
            hours = "" + hoursValue;
        }

        String output = "";
        if (hoursValue!=0) {
            output = hours + ":" + minutes + ":" + seconds;
        } else {
            output = minutes + ":" + seconds;
        }

        return output;
    }

    public void startService() {
        Intent intent = new Intent(mContext, MediaPlayerService.class);
        mContext.startService(intent);
    }


    public MediaPlayerService getService() {
        return mService;
    }

    public void setService(MediaPlayerService service) {
        this.mService = service;
    }

    public void setIsServiceRunning(boolean running) {
        this.mIsServiceRunning = running;
    }

    public boolean ismIsServiceRunning() {
        return mIsServiceRunning;
    }
}
