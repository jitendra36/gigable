package com.gigable.io.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.gigable.io.Models.ServiceRequest.GNotification;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.gigable.io.database.GigableAppDatabase.GigableAppColumn.NOTI_ID;

public class GigableDatabaseHelper
{
    private DatabaseHelper dataHelper;
    private SQLiteDatabase mDb;

    // Database Path
     private static String DATABASE_PATH = "/data/data/com.example.citrusbug.gigable/databases/";

    // Database Name
    public static final String DATABASE_NAME = "GigableDatabase.db";

    // Database Version
    public static final int DATABASE_VERSION = 1;

    public boolean isOpen = false;

    private static String TAG = "DbManager";

    public GigableDatabaseHelper(Context ctx)
    {
        dataHelper = new DatabaseHelper(ctx);
    }

    public static class DatabaseHelper extends SQLiteOpenHelper
    {
        private Context context = null;

        private boolean checkDataBase()
        {
            File f = new File(DATABASE_PATH + DATABASE_NAME);
            return f.exists();
        }

        // Copy Database into package from Assets Folder
        private void copyDataBase() throws IOException
        {
            try
            {
                // Open your local db as the input stream
                InputStream myInput = context.getAssets().open(DATABASE_NAME);
                // Path to the just created empty db
                String outFileName = DATABASE_PATH + DATABASE_NAME;
                OutputStream myOutput = new FileOutputStream(outFileName);
                // transfer bytes from the inputfile to the outputfile
                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0)
                {
                    myOutput.write(buffer, 0, length);
                }
                // Close the streams
                myOutput.flush();
                myOutput.close();
                myInput.close();
            }
            catch (Exception e)
            {
                Log.e(TAG, e.getMessage());
            }
        }

        public DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            Log.w("DBHelper", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            onCreate(db);
        }
    }

    // Open Database
    public GigableDatabaseHelper open() throws SQLException
    {
        try
        {
            boolean isExist = dataHelper.checkDataBase();
            if (!isExist)
            {
                mDb = dataHelper.getWritableDatabase();
                dataHelper.copyDataBase();
                if (mDb.isOpen())
                {
                    mDb.close();
                }
            }
            mDb = dataHelper.getWritableDatabase();
            isOpen = true;
        }
        catch (Exception e)
        {
            isOpen = false;
        }
        return this;
    }

    // Close Database
    public void close()
    {
        try
        {
            mDb.close();
            Log.e("Data base close success fully=======>>", "Data base close success fully");
        }
        catch (Exception e)
        {
            Log.e("Exception is close data base=======>>", e.toString());
        }
    }

    public void clearDatabase()
    {

    }

    
    public void insertNotification(ContentValues insertValues)
    {
        long rowid = 0;
        try
        {    
           rowid = mDb.insert(GigableAppDatabase.GigableAppColumn.NOTIFICATION_MASTER_TABLE, null, insertValues);
            Log.d(TAG, "Inseert in database: " + insertValues + "Row id:" + rowid);
        }
        catch (Exception e)
        {
            Log.e("Error: ", "e.getMessage() :" + e.getMessage());
        }
    }
    
    public long deleteNotification(String notificationId)
    {
        String args[] = { notificationId.toString() };
        
        long rowid = mDb.delete(GigableAppDatabase.GigableAppColumn.NOTIFICATION_MASTER_TABLE, NOTI_ID + "=?", args);
        return rowid;     

    }
    
    public List<GNotification> getNotificationList()
    {
        Cursor notiCursor = null;
        List<GNotification> appNotificationList = new ArrayList<GNotification>();
        
        try
        {
            String str = "SELECT * from "+GigableAppDatabase.GigableAppColumn.NOTIFICATION_MASTER_TABLE+" ORDER BY noti_id DESC";
            notiCursor = mDb.rawQuery(str, null);
            notiCursor.moveToFirst();
            if (notiCursor != null && notiCursor.getCount() > 0)
            {
                while (!notiCursor.isAfterLast())
                {
                	GNotification mNotification=new GNotification();
                	mNotification.setNoti_Id(notiCursor.getString(0));
                	mNotification.setTitle(notiCursor.getString(1));
                	mNotification.setMessage(notiCursor.getString(2));
                	mNotification.setPromocode(notiCursor.getString(3));
                	mNotification.setFrom(notiCursor.getString(4));
                	mNotification.setDate(notiCursor.getString(5));
                	
                	appNotificationList.add(mNotification);
                	
                    notiCursor.moveToNext();	
                }
            }
        }
        catch (Exception e)
        {
            Log.e("Error: ", "e.getMessage() :" + e.getMessage());
        }
        return appNotificationList;
    } 
    
    
    public int getNotificationCount()
    {
        Cursor notiCursor = null;
        int count=0;
        
        try
        {
            String str = "SELECT * from "+GigableAppDatabase.GigableAppColumn.NOTIFICATION_MASTER_TABLE+" where "+GigableAppDatabase.GigableAppColumn.NOTI_IS_READ+"='false'";
            notiCursor = mDb.rawQuery(str, null);
            if(notiCursor!=null)
            	count=notiCursor.getCount();
        }
        catch (Exception e)
        {
            Log.e("Error: ", "e.getMessage() :" + e.getMessage());
        }
        return count;
    } 
    
    public int updateNotificationAsRead(String notificationId, ContentValues updateValues)
    {
        String args[] = { notificationId.toString() };
        int rowid=mDb.update(GigableAppDatabase.GigableAppColumn.NOTIFICATION_MASTER_TABLE, updateValues, GigableAppDatabase.GigableAppColumn.NOTI_ID+ "=?", args);
        return rowid;
    }
    
}
