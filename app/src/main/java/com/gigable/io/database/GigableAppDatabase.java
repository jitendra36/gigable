package com.gigable.io.database;

import android.provider.BaseColumns;

/**
 * Created by Citrsubug on 11-04-2017.
 */

public class GigableAppDatabase {

    // Recording Master Table
    public static final class GigableAppColumn implements BaseColumns {

        public static final String NOTIFICATION_MASTER_TABLE = "notification_master";
        public static final String NOTI_ID = "noti_id";
        public static final String NOTI_IS_READ = "noti_is_read";
        public static final String NOTI_TITLE = "noti_title";
        public static final String NOTI_MESSAGE = "noti_message";
        public static final String NOTI_PROMOCODE = "noti_promocode";
        public static final String NOTI_FROM = "noti_from";
        public static final String NOTI_DATE = "noti_date";


    }
}
