package com.gigable.io.Network;

import android.content.Context;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.ForgotPasswordRequest;
import com.gigable.io.Models.ServiceRequest.LoginRequest;
import com.gigable.io.Models.ServiceRequest.NewPasswordRequest;
import com.gigable.io.Models.ServiceRequest.OrderRequest;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.RegisterRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.Tickets;
import com.gigable.io.Models.ServiceRequest.Track;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by abc on 25-Jun-16.
 */
public class HTTPWebRequest {


    public static void GetPlaylist(Context context, PlayListRequest playListRequest, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, playListRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.latitude, playListRequest.getLatitude());
        postDataParams.put(AppConstants.RequestDataKey.longitude, playListRequest.getLongitude());
        postDataParams.put(AppConstants.RequestDataKey.playlist_distance, playListRequest.getPlaylist_distance());
        postDataParams.put(AppConstants.RequestDataKey.distance_type, playListRequest.getDistance_type());
        postDataParams.put(AppConstants.RequestDataKey.month_range, playListRequest.getMonth_range());


        new BackgroundAsyncTask(context, AppConstants.APIURL.getPlaylist, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }


    public static void GetPlaylistHome(Context context, PlayListRequest playListRequest, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, playListRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.latitude, playListRequest.getLatitude());
        postDataParams.put(AppConstants.RequestDataKey.longitude, playListRequest.getLongitude());
        postDataParams.put(AppConstants.RequestDataKey.playlist_distance, playListRequest.getPlaylist_distance());
        postDataParams.put(AppConstants.RequestDataKey.distance_type, playListRequest.getDistance_type());
        postDataParams.put(AppConstants.RequestDataKey.month_range, playListRequest.getMonth_range());

        new BackgroundAsyncTask(context, AppConstants.APIURL.getPlaylist,AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }


    public static void Gettrack(Context context, PlayListRequest playListRequest, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, playListRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.latitude, playListRequest.getLatitude());
        postDataParams.put(AppConstants.RequestDataKey.longitude, playListRequest.getLongitude());
        postDataParams.put(AppConstants.RequestDataKey.playlist_distance, playListRequest.getPlaylist_distance());
        postDataParams.put(AppConstants.RequestDataKey.distance_type, playListRequest.getDistance_type());
        postDataParams.put(AppConstants.RequestDataKey.month_range, playListRequest.getMonth_range());
        postDataParams.put(AppConstants.RequestDataKey.utp_play_id, playListRequest.getUtp_play_id());


        new BackgroundAsyncTask(context, AppConstants.APIURL.gettracklist, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void Login(Context context, LoginRequest loginRequest, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_email, loginRequest.getLgn_email());
        postDataParams.put(AppConstants.RequestDataKey.lgn_password, loginRequest.getLgn_password());
        postDataParams.put(AppConstants.RequestDataKey.usr_imei_number, loginRequest.getUsr_imei_number());
        postDataParams.put(AppConstants.RequestDataKey.usr_gcm_number, loginRequest.getUsr_gcm_number());
        postDataParams.put(AppConstants.RequestDataKey.usr_type, loginRequest.getUsr_type());
        new BackgroundAsyncTask(context, AppConstants.APIURL.login, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }


    public static void Register(Context context, RegisterRequest registerRequest, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();

        postDataParams.put(AppConstants.RequestDataKey.usr_name, registerRequest.getUsr_name());
        postDataParams.put(AppConstants.RequestDataKey.lgn_email, registerRequest.getLgn_email());
        postDataParams.put(AppConstants.RequestDataKey.usr_type, registerRequest.getUsr_type());
        postDataParams.put(AppConstants.RequestDataKey.usr_phone, registerRequest.getUsr_phone());
        postDataParams.put(AppConstants.RequestDataKey.lgn_password, registerRequest.getLgn_password());
        postDataParams.put(AppConstants.RequestDataKey.confirm_password, registerRequest.getConfirm_password());
        postDataParams.put(AppConstants.RequestDataKey.usr_imei_number, registerRequest.getUsr_imei_number());
        postDataParams.put(AppConstants.RequestDataKey.usr_gcm_number, registerRequest.getUsr_gcm_number());

        new BackgroundAsyncTask(context, AppConstants.APIURL.signup, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void ForgotPassword(Context context, LoginRequest loginRequest, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_email, loginRequest.getLgn_email());

        new BackgroundAsyncTask(context, AppConstants.APIURL.forgot_pwd, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }


    public static void ForgotPasswordVerification(Context context, ForgotPasswordRequest forgotPasswordRequest, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, forgotPasswordRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.lgn_fgtpass_verifycode, forgotPasswordRequest.getLgn_fgtpass_verifycode());
        new BackgroundAsyncTask(context, AppConstants.APIURL.forgot_pwd_verification, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);

    }


    public static void NewPassword(Context context, NewPasswordRequest newPasswordRequest, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, newPasswordRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.new_password, newPasswordRequest.getNew_password());
        postDataParams.put(AppConstants.RequestDataKey.cnfm_password, newPasswordRequest.getCnfm_password());
        new BackgroundAsyncTask(context, AppConstants.APIURL.newPassword, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void ChangePassword(Context context, NewPasswordRequest newPasswordRequest, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, newPasswordRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.lgn_password, newPasswordRequest.getLgn_password());
        postDataParams.put(AppConstants.RequestDataKey.new_password, newPasswordRequest.getNew_password());
        new BackgroundAsyncTask(context, AppConstants.APIURL.changepwd, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }


    public static void LoginFb(Context context, RegisterRequest registerRequest, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.usr_name, registerRequest.getUsr_name());
        postDataParams.put(AppConstants.RequestDataKey.usr_type, registerRequest.getUsr_type());
        postDataParams.put(AppConstants.RequestDataKey.usr_imei_number, registerRequest.getUsr_imei_number());
        postDataParams.put(AppConstants.RequestDataKey.usr_gcm_number, registerRequest.getUsr_gcm_number());
        postDataParams.put(AppConstants.RequestDataKey.lgn_fb_auth_id, registerRequest.getLgn_fb_auth_id());

        new BackgroundAsyncTask(context, AppConstants.APIURL.fblogin, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void LoginGoogle(Context context, RegisterRequest registerRequest, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.usr_name, registerRequest.getUsr_name());
        postDataParams.put(AppConstants.RequestDataKey.usr_type, registerRequest.getUsr_type());
        postDataParams.put(AppConstants.RequestDataKey.usr_imei_number, registerRequest.getUsr_imei_number());
        postDataParams.put(AppConstants.RequestDataKey.usr_gcm_number, registerRequest.getUsr_gcm_number());
        postDataParams.put(AppConstants.RequestDataKey.lgn_google_auth_id, registerRequest.getLgn_google_auth_id());
        postDataParams.put(AppConstants.RequestDataKey.lgn_email, registerRequest.getLgn_email());

        new BackgroundAsyncTask(context, AppConstants.APIURL.googlelogin, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }


    public static void UpdateProfile(Context context, RegisterRequest registerRequest, ArrayList<HashMap<String, String>> imageArry, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, registerRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.usr_name, registerRequest.getUsr_name());
        postDataParams.put(AppConstants.RequestDataKey.usr_phone, registerRequest.getUsr_phone());
        postDataParams.put(AppConstants.RequestDataKey.usr_gender, registerRequest.getUsr_gender());
        new BackgroundAsyncTask(context, AppConstants.APIURL.updateProfile, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void ShowDetails(Context context, Show show, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, show.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.show_id, show.getShow_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.showDetails, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void PurchaseTkt(Context context, Tickets tickets, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.utkt_lgn_id,tickets.getUtkt_lgn_id());
        postDataParams.put(AppConstants.RequestDataKey.utkt_tkt_id, tickets.getUtkt_tkt_id());
        postDataParams.put(AppConstants.RequestDataKey.utkt_qty, tickets.getUtkt_qty());
        postDataParams.put(AppConstants.RequestDataKey.ticket_type_id,tickets.getTicket_type_id());

        new BackgroundAsyncTask(context, AppConstants.APIURL.tkt_purchase, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void PayForTkt(Context context, OrderRequest orderRequest, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id,orderRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.od_id, orderRequest.getOd_id());
        postDataParams.put(AppConstants.RequestDataKey.token, orderRequest.getToken());

        new BackgroundAsyncTask(context, AppConstants.APIURL.tkt_pay, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void GetArtDetl(Context context, Track track, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.art_id, track.getArt_id());
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, track.getLgn_id());

        new BackgroundAsyncTask(context, AppConstants.APIURL.art_detl, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }
    public static void GetFavtrack(Context context, PlayListRequest playListRequest, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.usrplay_lgn_id, playListRequest.getUsrplay_lgn_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.get_favtracklist, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }
    public static void AddDelTrack(Context context, Track track, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.usrplay_lgn_id, track.getUsrplay_lgn_id());
        postDataParams.put(AppConstants.RequestDataKey.utp_trk_id, track.getUtp_trk_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.addDelTrack, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }
    public static void GetFavShow(Context context, Show show, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.msf_lgn_id, show.getMsf_lgn_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.get_favShowlist, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }
    public static void DelShow(Context context, Show show, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.msf_lgn_id, show.getMsf_lgn_id());
        postDataParams.put(AppConstants.RequestDataKey.msf_show_id, show.getMsf_show_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.delShow, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void GetLocation(Context context, int postcode, ApiResponse apiResponse) {
        new BackgroundAsyncTask(context, AppConstants.APIURL.get_location, AppConstants.DialogMessage.PLEASE_WAIT, postcode, false).execute(apiResponse);
    }
    public static void GetUpConcerts(Context context,PlayListRequest playListRequest, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.latitude, playListRequest.getLatitude());
        postDataParams.put(AppConstants.RequestDataKey.longitude, playListRequest.getLongitude());

        new BackgroundAsyncTask(context, AppConstants.APIURL.up_concerts, AppConstants.DialogMessage.PLEASE_WAIT, postcode,postDataParams, false).execute(apiResponse);
    }

    public static void AddShowFav(Context context, Show show, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.msf_lgn_id, show.getMsf_lgn_id());
        postDataParams.put(AppConstants.RequestDataKey.msf_show_id, show.getMsf_show_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.showFav, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void GetShow(Context context, PlayListRequest playListRequest, int postcode, ApiResponse apiResponse) {

        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id, playListRequest.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.latitude, playListRequest.getLatitude());
        postDataParams.put(AppConstants.RequestDataKey.longitude, playListRequest.getLongitude());
        postDataParams.put(AppConstants.RequestDataKey.trk_id, playListRequest.getTrk_id());


        new BackgroundAsyncTask(context, AppConstants.APIURL.getShowList, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void AddTrackFav(Context context, Track track, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.utp_trk_id,track.getUtp_trk_id());
        postDataParams.put(AppConstants.RequestDataKey.usrplay_lgn_id, track.getUsrplay_lgn_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.trackFav, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }


    public static void PymentInfo(Context context, Tickets tickets, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id,tickets.getLgn_id());
        postDataParams.put(AppConstants.RequestDataKey.utkt_id, tickets.getUtkt_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.ticket_info, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void GetNotification(Context context, PlayListRequest playListRequest, int postcode, ApiResponse apiResponse) {
        HashMap<String, String> postDataParams = new HashMap<>();
        postDataParams.put(AppConstants.RequestDataKey.lgn_id,playListRequest.getLgn_id());
        new BackgroundAsyncTask(context, AppConstants.APIURL.getNotification, AppConstants.DialogMessage.PLEASE_WAIT, postcode, postDataParams, false).execute(apiResponse);
    }

    public static void GetVersion(Context context, int postcode, ApiResponse apiResponse) {
        new BackgroundAsyncTask(context, AppConstants.APIURL.getVersion, AppConstants.DialogMessage.PLEASE_WAIT, postcode, false).execute(apiResponse);
    }

}