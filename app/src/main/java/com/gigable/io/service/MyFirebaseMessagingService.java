package com.gigable.io.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.R;
import com.gigable.io.Utility.NotificationUtils;
import com.gigable.io.Utility.Pref;
import com.gigable.io.activity.NotificationListActivity;
import com.gigable.io.activity.PlaylistActivity;
import com.gigable.io.activity.Show_detailsActivity;
import com.gigable.io.activity.SplashActivity;
import com.gigable.io.app.Config;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import static com.gigable.io.app.Config.NOTIFICATION_ID;

/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    private NotificationManager mNotificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
     /*  if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotif ication(remoteMessage.getNotification().getBody());
        }*/


      /*  if(NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            Log.e("bg notification","call");

            // app is in foreground, broadcast the push message
            if (remoteMessage.getData().size() > 0) {
                Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

                try {
                    JSONObject json = new JSONObject(remoteMessage.getData());
                    handleDataMessage(json);
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }
        }*/

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification

            Log.e(TAG, "Notificationn called: " + "In background state");

        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());
        String id = "";
        String title = "";
        String message = "";
        String type = "";
        String title_Main = "";
        String message_Main = "";

        try {
            type = json.getString("type");
            id = json.getString("id");
            title_Main = json.getString("title");
            try {
                title =  URLDecoder.decode(title_Main, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            message_Main = json.getString("message");
            try {
                message = URLDecoder.decode(message_Main, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e(TAG, "id: " + id);
        Log.e(TAG, "title: " + title);
        Log.e(TAG, "type: " + type);
        Log.e(TAG, "message: " + message);

        Intent intent = null;

        String lgn_id = Pref.getValue(AppConstants.PreftDataKey.lgn_id, "" );

        Log.e("login_id",lgn_id);
        if (lgn_id.equals("")){
            intent = new Intent(this, SplashActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        else if (type.equals("show")) {

            intent = new Intent(this, Show_detailsActivity.class);
            intent.putExtra(AppConstants.SetGetBundleData.show_id, id);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        } else if (type.equals("Playlist")) {

            // bundle.putString(AppConstants.SetGetBundleData.play_id, id);
            // Utility.RedirectToActivity((Activity) getApplicationContext(), PlaylistActivity.class, false, bundle);
            intent = new Intent(this, PlaylistActivity.class);
            intent.putExtra(AppConstants.SetGetBundleData.play_id, id);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else if (type.equals("notify")) {
            intent = new Intent(this, NotificationListActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent)
                .setContentText(message)
                .setSmallIcon(R.mipmap.icon_gigable);

        notificationBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        notificationBuilder.setLights(Color.YELLOW, 1000, 300);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());


       /* GigableDatabaseHelper mDatabaseHelper = new GigableDatabaseHelper(this);
        mDatabaseHelper.open();
        ContentValues values = new ContentValues();
        values.put(GigableAppDatabase.GigableAppColumn.NOTI_PROMOCODE, "id");
        values.put(GigableAppDatabase.GigableAppColumn.NOTI_FROM, "name");
        values.put(GigableAppDatabase.GigableAppColumn.NOTI_IS_READ, "false");

        String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
        values.put(GigableAppDatabase.GigableAppColumn.NOTI_DATE, mydate);
        mDatabaseHelper.insertNotification(values);
        mDatabaseHelper.close();*/


       /* if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // app is in background, show the notification in notification tray
            Intent resultIntent = new Intent(getApplicationContext(), PushNotification.class);
            resultIntent.putExtra("message", message);

            // check for image attachment
               /* if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl);
                }*/
        //}

    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}