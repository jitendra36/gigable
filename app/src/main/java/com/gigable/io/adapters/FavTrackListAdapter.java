package com.gigable.io.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.fragment.FavTrackFragment;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.Models.ServiceResponse.AddDelFavTrkResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by abc on 12-May-16.
 */
public class FavTrackListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ApiResponse {


    ArrayList<Track> data;
    private Context ctx;
    private Activity activity;
    View itemView;

    Track track;

    int trk_posi;

    FavTrackFragment fragment;

    public FavTrackListAdapter(Context context, Activity act, FavTrackFragment fragment, ArrayList<Track> mData) {
        this.ctx = context;
        this.data = mData;
        this.activity = act;
        this.fragment=fragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.row_fav_track, parent, false);

        //artist = playListRequest.getArtist();

        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        track = getItem(position);


        ((Viewholder) holder).trk_titl.setText(track.getTrk_title());

        ((Viewholder) holder).close.setTag(position);

        /*((Viewholder) holder).close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String itemLabel = data.get(position);
                TextView textView = (TextView) v;
                // Remove the item on remove/button click
                if (textView.getTag() == v.getTag()) {


                    String track_id = data.get(Integer.parseInt("" + textView.getTag())).getTrk_id().toString();

                    Log.v("position",textView.getTag().toString());

                    trk_posi=Integer.parseInt("" + textView.getTag());

                    Track track = new Track();

                    track.setUtp_trk_id(track_id);
                    track.setUsrplay_lgn_id("1");

                    HTTPWebRequest.AddDelTrack(activity, track, AppConstants.APICode.addDelTrack,FavTrackListAdapter.this);
                }
            }
        });*/


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Track getItem(int pos) {
        return data.get(pos);
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.addDelTrack:

                        AddDelFavTrkResponse addDelFavTrkResponse = new AddDelFavTrkResponse();
                        addDelFavTrkResponse = gson.fromJson(response, AddDelFavTrkResponse.class);

                        if (addDelFavTrkResponse.getSTATUS().equals("SUCCESS")) {
                            removeItem(trk_posi);
                            fragment.setTotal_track(String.valueOf(data.size()));
                        }

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }


    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }


    @Override
    public void networkError(int apiCode) {
    }

    @Override
    public void responseError(String responseError, int apiCode) {
    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {
    }


    private class Viewholder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected TextView trk_titl,close;


        public Viewholder(View itemView) {
            super(itemView);

            trk_titl = (TextView) itemView.findViewById(R.id.trk_titl);
            close = (TextView) itemView.findViewById(R.id.close);


            close.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            String track_id = data.get(Integer.parseInt("" + getAdapterPosition())).getTrk_id().toString();

            trk_posi=Integer.parseInt("" + getAdapterPosition());

            Track track = new Track();

            track.setUtp_trk_id(track_id);
            track.setUsrplay_lgn_id("1");

            HTTPWebRequest.AddDelTrack(activity, track, AppConstants.APICode.addDelTrack,FavTrackListAdapter.this);

        }
    }

}
