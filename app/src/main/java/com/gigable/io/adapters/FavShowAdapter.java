package com.gigable.io.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.ExpandableRecycle.ExpandableRecyclerAdapter;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.FavShowRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.ShowExpand;
import com.gigable.io.Models.ServiceResponse.DelShowResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.activity.ChangeLocationActivity;
import com.gigable.io.activity.PaymentInfoActivity;
import com.gigable.io.activity.PlaylistActivity;
import com.gigable.io.activity.PurchaseActivity;
import com.gigable.io.activity.ShowFragment;
import com.gigable.io.activity.Show_detailsActivity;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FavShowAdapter extends ExpandableRecyclerAdapter<ShowExpand, FavShowRequest, ShoExpandViewHolder, ShowRequestViewHolder> implements ApiResponse {

    private static final int PARENT_TICKET = 0;
    private static final int PARENT_CURRENT = 1;
    private static final int PARENT_PAST = 2;

    private static final int CHILD_TICKET = 3;
    private static final int CHILD_CURRENT = 4;
    private static final int CHILD_PAST = 5;

    private LayoutInflater mInflater;
    private List<ShowExpand> mRecipeList;
    private Context context;
    Activity activity;
    int show_posi;
    int parent_posi;
    private ExpandableRecyclerAdapter mExpandableItemManager;



    View recipeView;
    TextView textView1;


    public FavShowAdapter(Context context, Activity activity, @NonNull List<ShowExpand> recipeList) {
        super(recipeList);
        mRecipeList = recipeList;
        mInflater = LayoutInflater.from(activity);
        this.context = activity;
        this.activity = activity;
    }

    @UiThread
    @NonNull
    @Override
    public ShoExpandViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        switch (viewType) {
            default:
            case PARENT_TICKET:
                recipeView = mInflater.inflate(R.layout.show_expand_view, parentViewGroup, false);
                break;
            case PARENT_CURRENT:
                recipeView = mInflater.inflate(R.layout.show_expand_view, parentViewGroup, false);
                textView1 = (TextView) recipeView.findViewById(R.id.total_Show);
                break;
            case PARENT_PAST:
                recipeView = mInflater.inflate(R.layout.show_expand_view, parentViewGroup, false);
                break;
        }
        return new ShoExpandViewHolder(recipeView);
    }

    @UiThread
    @NonNull
    @Override
    public ShowRequestViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View showRequestView;
        switch (viewType) {
            default:
            case CHILD_TICKET:
                showRequestView = mInflater.inflate(R.layout.show_request_view, childViewGroup, false);
                showRequestView.findViewById(R.id.tkt).setVisibility(View.GONE);
                showRequestView.findViewById(R.id.close).setVisibility(View.GONE);
                break;
            case CHILD_CURRENT:
                showRequestView = mInflater.inflate(R.layout.show_request_view, childViewGroup, false);
                showRequestView.findViewById(R.id.tkt_active).setVisibility(View.GONE);
                break;
            case CHILD_PAST:
                showRequestView = mInflater.inflate(R.layout.show_request_view, childViewGroup, false);
                showRequestView.findViewById(R.id.tkt).setVisibility(View.GONE);
                showRequestView.findViewById(R.id.close).setVisibility(View.GONE);
                showRequestView.findViewById(R.id.tkt_active).setVisibility(View.GONE);
                break;
        }
        return new ShowRequestViewHolder(showRequestView);
    }

    @Override
    public void onBindParentViewHolder(@NonNull ShoExpandViewHolder parentViewHolder, int parentPosition, @NonNull ShowExpand parent) {
        parentViewHolder.bind(parent);
    }

    @Override
    public void onBindChildViewHolder(@NonNull ShowRequestViewHolder childViewHolder, final int parentPosition, final int childPosition, @NonNull final FavShowRequest child) {


        Picasso.with(context)
                .load(child.getArt_image())
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(((childViewHolder).art_img));


        (childViewHolder).tkt.setTag(childPosition);
        (childViewHolder).tkt_active.setTag(childPosition);
        (childViewHolder).close.setTag(childPosition);

        (childViewHolder).tkt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String itemLabel = data.get(position);
                ImageView imageView = (ImageView) v;
                // Remove the item on remove/button click
                if (imageView.getTag() == v.getTag()) {

                    String show_id = mRecipeList.get(parentPosition).getChildList().get(Integer.parseInt("" + imageView.getTag())).getShow_id().toString();
                    String art_img = mRecipeList.get(parentPosition).getChildList().get(Integer.parseInt("" + imageView.getTag())).getArt_image().toString();
                    String art_name = mRecipeList.get(parentPosition).getChildList().get(Integer.parseInt("" + imageView.getTag())).getArt_name().toString();

                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.SetGetBundleData.show_id, show_id);
                    bundle.putString(AppConstants.SetGetBundleData.art_name, art_name);
                    bundle.putString(AppConstants.SetGetBundleData.art_img, art_img);

                    Intent intent = new Intent(context, Show_detailsActivity.class);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            }
        });
        (childViewHolder).tkt_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String itemLabel = data.get(position);
                ImageView imageView = (ImageView) v;
                // Remove the item on remove/button click
                if (imageView.getTag() == v.getTag()) {

                    String show_id = mRecipeList.get(parentPosition).getChildList().get(Integer.parseInt("" + imageView.getTag())).getShow_id().toString();
                    Intent intent = new Intent(context, PaymentInfoActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.SetGetBundleData.show_place, mRecipeList.get(parentPosition).getChildList().get(Integer.parseInt("" + imageView.getTag())).getVen_name().toString());
                    bundle.putString(AppConstants.SetGetBundleData.utkt_id, mRecipeList.get(parentPosition).getChildList().get(Integer.parseInt("" + imageView.getTag())).getUtkt_id().toString());

                    intent.putExtras(bundle);
                    context.startActivity(intent);
                }
            }
        });


        (childViewHolder).close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String show_id = mRecipeList.get(parentPosition).getChildList().get(Integer.parseInt("" + childPosition)).getShow_id().toString();

                parent_posi = parentPosition;
                show_posi = childPosition;


                Show show = new Show();

                show.setMsf_lgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));
                show.setMsf_show_id(show_id);

                HTTPWebRequest.DelShow(activity, show, AppConstants.APICode.delShow, FavShowAdapter.this);
            }
        });

        childViewHolder.bind(child);
    }

    @Override
    public int getParentViewType(int parentPosition) {
        if (mRecipeList.get(parentPosition).getName().equals("Ticket")) {
            return PARENT_TICKET;
        } else if (mRecipeList.get(parentPosition).getName().equals("Watching . . . ")) {
            return PARENT_CURRENT;
        } else {
            return PARENT_PAST;
        }
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        FavShowRequest favShowRequest = mRecipeList.get(parentPosition).getIngredient(childPosition);
        if (parentPosition == 0) {
            return CHILD_TICKET;
        } else if (parentPosition == 1) {
            return CHILD_CURRENT;
        } else {
            return CHILD_PAST;
        }
    }

    @Override
    public boolean isParentViewType(int viewType) {
        return viewType == PARENT_TICKET || viewType == PARENT_CURRENT || viewType == PARENT_PAST;
    }


    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.delShow:

                        DelShowResponse delShowResponse = new DelShowResponse();
                        delShowResponse = gson.fromJson(response, DelShowResponse.class);

                        if (delShowResponse.getSTATUS().equals("SUCCESS")) {


                            List<FavShowRequest> mRecipeList1 = mRecipeList.get(1).getChildList();
                            //removeItem(parent_posi, show_posi);
                            mRecipeList1.remove(mRecipeList1.size()-1);
                            notifyChildRemoved(1,show_posi);
                            textView1.setText(""+"("+String.valueOf(mRecipeList.get(1).getChildList().size())+")");

                        }
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }
}
