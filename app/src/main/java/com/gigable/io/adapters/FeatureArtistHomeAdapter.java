package com.gigable.io.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.R;
import com.gigable.io.activity.HomeScreenActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by abc on 18-May-16.
 */
public class FeatureArtistHomeAdapter extends BaseAdapter {

    ArrayList<Track> data;
    Context context;

    public FeatureArtistHomeAdapter(Context ctx, HomeScreenActivity homeScreenActivity1, ArrayList<Track> mData) {
        this.context = ctx;
        this.data = mData;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_artist_gridview_items, parent, false);
            holder.thumb = (ImageView) convertView.findViewById(R.id.thumb);
            holder.artist_title = (TextView) convertView.findViewById(R.id.artist_title);
            holder.linear = (LinearLayout) convertView.findViewById(R.id.linear);

            holder.artist_title.setText(data.get(position).getArt_name());

            /*int width= context.getResources().getDisplayMetrics().widthPixels;
            com.squareup.picasso.Picasso
                    .with(context)
                    .load(data.get(position).getArt_image())
                    .centerCrop().resize(width/3,width/3)
                    .error(R.mipmap.error)
                    .placeholder(R.mipmap.placeholder)
                    .into((holder).thumb);*/

            Glide.with(context)
                    .load(data.get(position).getArt_image())
                    .thumbnail(0.5f)
                    .apply(new RequestOptions()
                            .placeholder(R.mipmap.placeholder)
                            .error(R.mipmap.placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into((holder).thumb);




        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        return convertView;
    }

    class ViewHolder {
        ImageView thumb;
        TextView artist_title;
        LinearLayout linear;
    }
}
