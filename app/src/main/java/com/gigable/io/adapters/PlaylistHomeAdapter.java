package com.gigable.io.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.R;
import com.gigable.io.activity.HomeScreenActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abc on 18-May-16.
 */
public class PlaylistHomeAdapter extends BaseAdapter {

    List<PlayListRequest> data;
    Context context;

    public PlaylistHomeAdapter(Context ctx, HomeScreenActivity homeScreenActivity1, ArrayList<PlayListRequest> mData) {
        this.context = ctx;
        this.data = mData.subList(1,mData.size());;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

       // data.remove(0);


        if(convertView == null){

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_playlist_gridview_items, parent, false);
            holder.img_icon = (ImageView) convertView.findViewById(R.id.img_icon);
            holder.total_track = (TextView) convertView.findViewById(R.id.total_track);
            holder.playlist_title = (TextView) convertView.findViewById(R.id.playlist_title);
            holder.linear =(LinearLayout)convertView.findViewById(R.id.linear);

            holder.playlist_title.setText(data.get(position).getPlay_name());
            holder.total_track.setText(data.get(position).getTotaltrack()+" Tracks");

          //  if (data.get(position).getImage().length()>0) {

               /* int width= context.getResources().getDisplayMetrics().widthPixels;
                com.squareup.picasso.Picasso
                        .with(context)
                        .load(data.get(position).getImage())
                        .centerCrop().resize(width/2,width/2)
                        .error(R.mipmap.error)
                        .placeholder(R.mipmap.placeholder)
                        .into((holder).img_icon);*/

            Glide.with(context)
                    .load(data.get(position).getImage())
                    .thumbnail(0.5f)
                    .apply(new RequestOptions()
                            .placeholder(R.mipmap.placeholder)
                            .error(R.mipmap.placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into((holder).img_icon);


          //  }
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }



        return convertView;
    }
        class ViewHolder {
            ImageView img_icon;
            TextView total_track;
            TextView playlist_title;
            LinearLayout linear;
        }
    }
