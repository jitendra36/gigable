package com.gigable.io.adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gigable.io.Models.ServiceRequest.GNotification;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.R;
import com.gigable.io.database.GigableDatabaseHelper;

import java.util.ArrayList;

/**
 * Created by abc on 12-May-16.
 */
public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    Context context;
    LayoutInflater inflater;
    private ArrayList<Show> mNotificationList = new ArrayList<>();
    private GigableDatabaseHelper mAppDatabaseHelper;
    ClipboardManager myClipboard;
    ClipData myClip;

    View itemView;

    public NotificationAdapter(Context context, ArrayList<Show> mNotificationList) {
        this.context = context;
        this.mNotificationList = (ArrayList<Show>) mNotificationList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mAppDatabaseHelper = new GigableDatabaseHelper(context);
        myClipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.list_notification_item, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final Show rowItem = (Show) getItem(position);

        ((Viewholder) holder).message.setText(rowItem.getMessage());
        ((Viewholder) holder).date.setText(rowItem.getDate());



    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }

    public Show getItem(int pos) {
        return mNotificationList.get(pos);
    }

    private class Viewholder extends RecyclerView.ViewHolder {

        TextView message;
        TextView date;



        public Viewholder(View itemView) {
            super(itemView);

            message = (TextView) itemView.findViewById(R.id.message);
            date = (TextView) itemView.findViewById(R.id.date);

        }


    }

}