package com.gigable.io.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.gigable.io.R;
import com.gigable.io.fragment.SingleViewFragment;

public class StartupImageAdapter extends FragmentPagerAdapter {

	

	public Context ctx;

	public StartupImageAdapter(FragmentManager fm, Context ctx) {
		super(fm);
		this.ctx=ctx;
	}

	public Integer[] mThumbIds = {
			R.mipmap.start1_2x, R.mipmap.start2_2x,
			R.mipmap.start3_2x,R.mipmap.start4_2x
	};

	@Override
	public Fragment getItem(int i) {
		Bundle args = new Bundle();
		args.putInt("image", mThumbIds[i]);

		SingleViewFragment fragment = new SingleViewFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public int getCount() {
		return mThumbIds.length;
	}
}