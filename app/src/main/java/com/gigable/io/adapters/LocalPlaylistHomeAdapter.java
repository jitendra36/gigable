package com.gigable.io.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.R;
import com.gigable.io.activity.HomeScreenActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by abc on 18-May-16.
 */
public class LocalPlaylistHomeAdapter extends BaseAdapter {

    ArrayList<PlayListRequest> data;
    Context context;
    PlayListRequest playListRequest;

    ArrayList<Track> arrayList;

    public LocalPlaylistHomeAdapter(Context ctx, HomeScreenActivity homeScreenActivity1, ArrayList<PlayListRequest> mData) {
        this.context = ctx;
        this.data = mData;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {

            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_localplaylist_gridview_items, parent, false);
            holder.img_icon = (ImageView) convertView.findViewById(R.id.img_icon);
            holder.linear = (LinearLayout) convertView.findViewById(R.id.linear);

            arrayList = data.get(0).getArtist();

             /*   Picasso.with(context)
                        .load(arrayList.get(position).getImage())
                        .placeholder(R.mipmap.placeholder)
                        .error(R.mipmap.placeholder)
                        .into((holder).img_icon);*/


            Glide.with(context)
                    .load(arrayList.get(position).getImage())
                    .thumbnail(0.5f)
                    .apply(new RequestOptions()
                            .placeholder(R.mipmap.placeholder)
                            .error(R.mipmap.placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into((holder).img_icon);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }

    class ViewHolder {
        ImageView img_icon;
        LinearLayout linear;
    }
}
