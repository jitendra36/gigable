package com.gigable.io.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Utility;
import com.gigable.io.activity.ArtistDetailsActivity;

import java.util.ArrayList;

/**
 * Created by abc on 12-May-16.
 */
public class ChangeLocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    ArrayList<PlayListRequest> data;
    private Context ctx;
    private Activity activity;
    View itemView;




    PlayListRequest playListRequest;

    public ChangeLocationAdapter(Context context, Activity act, ArrayList<PlayListRequest> mData) {
        this.ctx = context;
        this.data = mData;
        this.activity = act;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.row_location, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        playListRequest = getItem(position);

        ((Viewholder) holder).loc_name.setText(playListRequest.getLoc_name());

        ((Viewholder) holder).linear.setTag(position);

        ((ChangeLocationAdapter.Viewholder) holder).linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String itemLabel = data.get(position);
                LinearLayout linearLayout = (LinearLayout) v;
                // Remove the item on remove/button click
                if (linearLayout.getTag() == v.getTag()) {

                    Bundle bundle = new Bundle();

                  //  bundle.putString(AppConstants.SetGetBundleData.art_name, data.get(Integer.parseInt("" + imageView.getTag())).getArt_name());
                    Utility.RedirectToActivity((Activity) ctx, ArtistDetailsActivity.class, false, bundle);

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public PlayListRequest getItem(int pos) {
        return data.get(pos);
    }


    private class Viewholder extends RecyclerView.ViewHolder {
        protected TextView loc_name;
        protected LinearLayout linear;



        public Viewholder(View itemView) {
            super(itemView);

            loc_name = (TextView) itemView.findViewById(R.id.loc_name);
            linear = (LinearLayout) itemView.findViewById(R.id.linear);

        }


    }

}
