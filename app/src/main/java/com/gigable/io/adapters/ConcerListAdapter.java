package com.gigable.io.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.R;
import com.gigable.io.Utility.Utility;
import com.gigable.io.activity.Show_detailsActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by abc on 12-May-16.
 */
public class ConcerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    ArrayList<Show> data;
    private Context ctx;
    private Activity activity;
    View itemView;

    private SparseBooleanArray selectedItems;


    private static final String LIST_FRAGMENT_TAG = "list_fragment";
    private static final String TRACK_FRAGMENT_TAG = "TRACK_fragment1";

    public int focusedItem = 0;

    String play_id = "";
    String art_name = "";
    String art_img = "";
    int trk_posi;

    Show show;

    public ConcerListAdapter(Context context, Activity act, ArrayList<Show> mData,String art_name,String art_img) {
        this.ctx = context;
        this.data = mData;
        this.activity = act;
        this.art_name =art_name;
        this.art_img = art_img;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.row_concertlist, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        show = getItem(position);

        holder.itemView.setSelected(focusedItem == position);


        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr=show.getShow_date();
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);



        DateFormat inputFormat_time = new SimpleDateFormat("hh:mm:ss");
        DateFormat outputFormat_time = new SimpleDateFormat("hh:mm a");
        String inputDateStr_time = show.getShow_time();
        Date date1 = null;
        try {
            date1 = inputFormat_time.parse(inputDateStr_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr_time = outputFormat_time.format(date1);




        ((Viewholder) holder).date.setText(outputDateStr);
        ((Viewholder) holder).time.setText(outputDateStr_time);
        ((Viewholder) holder).place.setText(show.getShow_venue());
        ((Viewholder) holder).price.setText("$"+show.getShow_price()+"+");

        ((Viewholder) holder).details.setTag(position);


        ((Viewholder) holder).details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String itemLabel = data.get(position);
                TextView textView = (TextView) v;
                // Remove the item on remove/button click
                if (textView.getTag() == v.getTag()) {

                    String show_id = data.get(Integer.parseInt("" + textView.getTag())).getShow_id().toString();


                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.SetGetBundleData.show_id, show_id);
                    bundle.putString(AppConstants.SetGetBundleData.art_name, art_name);
                    bundle.putString(AppConstants.SetGetBundleData.art_img, art_img);
                    Utility.RedirectToActivity((Activity) ctx, Show_detailsActivity.class, false, bundle);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Show getItem(int pos) {
        return data.get(pos);
    }


    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    private class Viewholder extends RecyclerView.ViewHolder {


        protected TextView date;
        protected TextView place;
        protected TextView price;
        protected TextView details;
        protected TextView time;
        LinearLayout linearLayout;


        public Viewholder(View itemView) {
            super(itemView);

            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            place = (TextView) itemView.findViewById(R.id.place);
            price = (TextView) itemView.findViewById(R.id.price);
            details = (TextView) itemView.findViewById(R.id.details);


        }

    }

}
