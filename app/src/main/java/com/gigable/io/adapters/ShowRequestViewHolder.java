package com.gigable.io.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gigable.io.ExpandableRecycle.ChildViewHolder;
import com.gigable.io.Models.ServiceRequest.FavShowRequest;
import com.gigable.io.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ShowRequestViewHolder extends ChildViewHolder {

    private TextView date_main,time,title,plc;
    ImageView tkt,close,tkt_active;
    ImageView art_img;

    Context context;


    public ShowRequestViewHolder(@NonNull View itemView) {
        super(itemView);



        date_main = (TextView) itemView.findViewById(R.id.date);
        time = (TextView) itemView.findViewById(R.id.time);
        title = (TextView) itemView.findViewById(R.id.title);
        plc = (TextView) itemView.findViewById(R.id.plc);
        tkt = (ImageView) itemView.findViewById(R.id.tkt);
        tkt_active = (ImageView) itemView.findViewById(R.id.tkt_active);
        close = (ImageView) itemView.findViewById(R.id.close);
        art_img = (ImageView) itemView.findViewById(R.id.img_art);
    }

    public void bind(@NonNull FavShowRequest favShowRequest) {

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr=favShowRequest.getShow_date();
        Date date = null;
        try {
            date = inputFormat.parse(inputDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);



        DateFormat inputFormat_time = new SimpleDateFormat("hh:mm:ss");
        DateFormat outputFormat_time = new SimpleDateFormat("hh:mm a");
        String inputDateStr_time = favShowRequest.getShow_time();
        Date date1 = null;
        try {
            date1 = inputFormat_time.parse(inputDateStr_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr_time = outputFormat_time.format(date1);


        date_main.setText(outputDateStr);
        time.setText(outputDateStr_time);
        title.setText(favShowRequest.getShow_title());
        //plc.setText(favShowRequest.getShow_desc());



    }
}
