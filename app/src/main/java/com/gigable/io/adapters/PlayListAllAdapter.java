package com.gigable.io.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.Models.ServiceResponse.AddDelFavTrkResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.activity.ArtistDetailsActivity;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by abc on 12-May-16.
 */
public class PlayListAllAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ApiResponse {


    ArrayList<Track> data;
    private Context ctx;
    private Activity activity;
    View itemView;

    public int focusedItem = 0;

    String play_id = "";
    int trk_posi;

    Track track;


    int selectedPosition = -1;


    ClickListener clickListener;


    public PlayListAllAdapter(Context context, Activity act, ArrayList<Track> mData, String play_id) {
        this.ctx = context;
        this.data = mData;
        this.activity = act;
        this.play_id = play_id;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.row_playlistall, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        track = getItem(position);

        holder.itemView.setSelected(focusedItem == position);
        if (selectedPosition == position)
            holder.itemView.setBackgroundColor(Color.parseColor("#636363"));
        else
            holder.itemView.setBackgroundColor(Color.TRANSPARENT);

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");

        String outputDateStr = "";

        if (!track.getShow_date().equals("")) {
            String inputDateStr = track.getShow_date();
            Date date = null;
            try {
                date = inputFormat.parse(inputDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            outputDateStr = outputFormat.format(date);
        }
        ((Viewholder) holder).track_title.setText(track.getTrk_title());
        ((Viewholder) holder).art_name.setText(track.getArt_name());
        ((Viewholder) holder).date.setText(outputDateStr);
        ((Viewholder) holder).info.setText(track.getShow_venue());

        if (!track.getShow_price().equals("")) {
            ((Viewholder) holder).price.setText("$" + track.getShow_price() + "+");
        } else {
            ((Viewholder) holder).price.setText(track.getShow_price());
        }
        if (play_id.equals("fav_playlist")) {
            ((Viewholder) holder).img_cncl.setVisibility(View.VISIBLE);
            ((Viewholder) holder).img_info.setVisibility(View.GONE);
        }
        Glide.with(ctx)
                .load(track.getArtist_image())
                .thumbnail(0.5f)
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.placeholder)
                        .error(R.mipmap.placeholder)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(((Viewholder) holder).thumbnail);

        ((Viewholder) holder).img_info.setTag(position);
        ((Viewholder) holder).img_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView imageView = (ImageView) v;
                if (imageView.getTag() == v.getTag()) {

                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.SetGetBundleData.art_id, data.get(Integer.parseInt("" + imageView.getTag())).getArt_id());
                    bundle.putString(AppConstants.SetGetBundleData.art_name, data.get(Integer.parseInt("" + imageView.getTag())).getArt_name());
                    bundle.putString(AppConstants.SetGetBundleData.art_img, data.get(Integer.parseInt("" + imageView.getTag())).getArtist_image());
                    Utility.RedirectToActivity((Activity) ctx, ArtistDetailsActivity.class, false, bundle);
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    public Track getItem(int pos) {
        return data.get(pos);
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.addDelTrack:

                        AddDelFavTrkResponse addDelFavTrkResponse = new AddDelFavTrkResponse();
                        addDelFavTrkResponse = gson.fromJson(response, AddDelFavTrkResponse.class);

                        if (addDelFavTrkResponse.getSTATUS().equals("SUCCESS")) {
                            removeItem(trk_posi);
                            // fragment.setTotal_track(String.valueOf(data.size()));
                        }

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }


    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void ItemClicked(View v, int position);
    }

    public void shuffleList(ArrayList<Track> list) {
        data=list;
        notifyDataSetChanged();
    }

    public void changeItem(int position) {
        selectedPosition = position;
        notifyDataSetChanged();
    }


    public void removeItem(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void networkError(int apiCode) {

    }


    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }


    private class Viewholder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected TextView track_title;
        protected TextView art_name;
        protected TextView date;
        protected TextView info;
        protected TextView price;
        protected ImageView thumbnail;
        protected ImageView img_info;
        protected ImageView img_cncl;
        LinearLayout linearLayout;
        protected LinearLayout liner_item;


        public Viewholder(View itemView) {
            super(itemView);

            track_title = (TextView) itemView.findViewById(R.id.track_title);
            art_name = (TextView) itemView.findViewById(R.id.artist);
            date = (TextView) itemView.findViewById(R.id.date);
            info = (TextView) itemView.findViewById(R.id.info);
            price = (TextView) itemView.findViewById(R.id.price);
            thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            img_info = (ImageView) itemView.findViewById(R.id.img_info);
            img_cncl = (ImageView) itemView.findViewById(R.id.img_cncl);
            liner_item = (LinearLayout) itemView.findViewById(R.id.liner_item);

            img_cncl.setOnClickListener(this);
            liner_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null) {
                        clickListener.ItemClicked(v, getAdapterPosition());
                    }
                }
            });


        }

        @Override
        public void onClick(View v) {
            String track_id = data.get(Integer.parseInt("" + getAdapterPosition())).getTrk_id().toString();

            trk_posi = Integer.parseInt("" + getAdapterPosition());

            Track track = new Track();

            track.setUtp_trk_id(track_id);
            track.setUsrplay_lgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));

            HTTPWebRequest.AddDelTrack(activity, track, AppConstants.APICode.addDelTrack, PlayListAllAdapter.this);
        }
    }

}
