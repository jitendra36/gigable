package com.gigable.io.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.R;
import com.gigable.io.Utility.Utility;
import com.gigable.io.activity.Show_detailsActivity;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by abc on 12-May-16.
 */
public class UpcomingConcertstAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ApiResponse {



    ArrayList<Show> data;
    private Context ctx;
    private Activity activity;
    View itemView;


    Show show;

    public UpcomingConcertstAdapter(Context context, Activity act, ArrayList<Show> mData) {
        this.ctx = context;
        this.data = mData;
        this.activity = act;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.row_showlist, parent, false);
        return new Viewholder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        show = getItem(position);

        String outputDateStr = "";
        String outputDateStr_time= "";


        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");

        if (!show.getShow_date().equals("")) {

            String inputDateStr = show.getShow_date();
            Date date = null;
            try {
                date = inputFormat.parse(inputDateStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            outputDateStr = outputFormat.format(date);
        }



        DateFormat inputFormat_time = new SimpleDateFormat("hh:mm:ss");
        DateFormat outputFormat_time = new SimpleDateFormat("hh:mm a");

        if (!show.getShow_time().equals("")) {
            String inputDateStr_time = show.getShow_time();
            Date date1 = null;
            try {
                date1 = inputFormat_time.parse(inputDateStr_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            outputDateStr_time = outputFormat_time.format(date1);
        }


        ((UpcomingConcertstAdapter.Viewholder) holder).show_title.setText(show.getShow_title());
        ((UpcomingConcertstAdapter.Viewholder) holder).date.setText(outputDateStr);
        ((UpcomingConcertstAdapter.Viewholder) holder).time.setText(outputDateStr_time);

        //((UpcomingConcertstAdapter.Viewholder) holder).show_place.setText(show.getVen_name());

        Picasso.with(activity)
                .load(show.getArt_image())
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(((UpcomingConcertstAdapter.Viewholder) holder).art_img);


        ((Viewholder) holder).linearLayout.setTag(position);

        ((UpcomingConcertstAdapter.Viewholder) holder).linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String itemLabel = data.get(position);
                LinearLayout linearLayout = (LinearLayout) v;
                // Remove the item on remove/button click
                if (linearLayout.getTag() == v.getTag()) {

                    String show_id = data.get(Integer.parseInt("" + linearLayout.getTag())).getShow_id().toString();
                    String art_name = data.get(Integer.parseInt("" + linearLayout.getTag())).getArt_name().toString();
                    String art_img = data.get(Integer.parseInt("" + linearLayout.getTag())).getArt_image().toString();

                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.SetGetBundleData.show_id, show_id);
                    bundle.putString(AppConstants.SetGetBundleData.art_name, art_name);
                    bundle.putString(AppConstants.SetGetBundleData.art_img, art_img);
                    Utility.RedirectToActivity(activity, Show_detailsActivity.class, false, bundle);
                }
            }
        });


        ((UpcomingConcertstAdapter.Viewholder) holder).img_show.setTag(position);

        ((UpcomingConcertstAdapter.Viewholder) holder).img_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  String itemLabel = data.get(position);
                ImageView imageView = (ImageView) v;
                // Remove the item on remove/button click
                if (imageView.getTag() == v.getTag()) {

                    String show_id = data.get(Integer.parseInt("" + imageView.getTag())).getShow_id().toString();
                    String art_name = data.get(Integer.parseInt("" + imageView.getTag())).getArt_name().toString();
                    String art_img = data.get(Integer.parseInt("" + imageView.getTag())).getArt_image().toString();

                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.SetGetBundleData.show_id, show_id);
                    bundle.putString(AppConstants.SetGetBundleData.art_name, art_name);
                    bundle.putString(AppConstants.SetGetBundleData.art_img, art_img);
                    Utility.RedirectToActivity(activity, Show_detailsActivity.class, false, bundle);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Show getItem(int pos) {
        return data.get(pos);
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {

    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {
    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }


    private class Viewholder extends RecyclerView.ViewHolder {

        protected TextView show_title,show_place,date,time;
        ImageView img_show,art_img;
        LinearLayout linearLayout;


        public Viewholder(View itemView) {
            super(itemView);

            show_title = (TextView) itemView.findViewById(R.id.show_title);
            img_show = (ImageView) itemView.findViewById(R.id.img_show);
            art_img = (ImageView) itemView.findViewById(R.id.art_img);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.linear);

        }
    }

}
