package com.gigable.io.adapters;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.gigable.io.ExpandableRecycle.ParentViewHolder;
import com.gigable.io.Models.ServiceRequest.ShowExpand;
import com.gigable.io.R;

public class ShoExpandViewHolder extends ParentViewHolder {

    @NonNull
    private TextView title;
    public TextView total_Show;


    @NonNull
    public TextView getTitle() {
        return title;
    }

    public void setTitle(@NonNull TextView title) {
        this.title = title;
    }

    public TextView getTotal_Show() {
        return total_Show;
    }

    public void setTotal_Show(TextView total_Show) {
        this.total_Show = total_Show;
    }

    public ShoExpandViewHolder(@NonNull View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.title);
        total_Show = (TextView) itemView.findViewById(R.id.total_Show);

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isExpanded()) {
                    collapseView();
                } else {
                    expandView();
                }
            }
        });

    }

    public void bind(@NonNull ShowExpand showExpand) {
        title.setText(showExpand.getName());
        total_Show.setText("("+showExpand.getTotal_show()+")");
    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (expanded) {
               // mArrowExpandImageView.setRotation(ROTATED_POSITION);
            } else {
               // mArrowExpandImageView.setRotation(INITIAL_POSITION);
            }
        }
    }



    @Override
    public boolean shouldItemViewClickToggleExpansion() {
        return false;
    }

    @Override
    public void onExpansionToggled(boolean expanded) {
        super.onExpansionToggled(expanded);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            RotateAnimation rotateAnimation;
        }
    }
}
