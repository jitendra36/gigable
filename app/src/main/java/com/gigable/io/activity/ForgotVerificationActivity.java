package com.gigable.io.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.ForgotPasswordRequest;
import com.gigable.io.Models.ServiceResponse.ForgotPasswordResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class ForgotVerificationActivity extends Activity implements View.OnClickListener, ApiResponse, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    EditText verification_code;
    TextView submit, title, subtitle, submit_resendcode;
    String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_forgot_verification);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");


      /*  IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/

        init();
    }

    //initialization of elements
    private void init() {
        verification_code = (EditText) findViewById(R.id.pwd_verification);
        submit = (TextView) findViewById(R.id.submit_verificatiion);

        title = (TextView) findViewById(R.id.title);

        //submit_resendcode = (TextView) findViewById(R.id.submit_resendcode);

        submit.setOnClickListener(this);
        //submit_resendcode.setOnClickListener(this);



        //disable actionbar items(title,icon)
        //getActionBar().setDisplayShowHomeEnabled(false);
        //getActionBar().setDisplayShowTitleEnabled(false);
    }


    //click event of elements
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.submit_verificatiion:

                ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
                forgotPasswordRequest.setLgn_id(id);
                forgotPasswordRequest.setLgn_fgtpass_verifycode(verification_code.getText().toString());

                HTTPWebRequest.ForgotPasswordVerification(ForgotVerificationActivity.this, forgotPasswordRequest, AppConstants.APICode.forgot_pwd_verification, ForgotVerificationActivity.this);
                break;

            /*case R.id.submit_resendcode:

                ForgotPasswordRequest forgotPasswordRequest1 = new ForgotPasswordRequest();
                forgotPasswordRequest1.setLgn_id(id);
                //HTTPWebRequest.ResendVerificationCode(ForgotVerificationActivity.this, forgotPasswordRequest1, AppConstants.APICode.resend_verification_code, ForgotVerificationActivity.this);
                break;*/
        }
    }

    //here manage api responce
    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.forgot_pwd_verification:

                        ForgotPasswordResponse forgotPasswordResponse = new ForgotPasswordResponse();
                        forgotPasswordResponse = gson.fromJson(response, ForgotPasswordResponse.class);

                        if (forgotPasswordResponse.getSTATUS().equals("SUCCESS")) {
                            Toast.makeText(ForgotVerificationActivity.this, forgotPasswordResponse.getMESSAGES(), Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(ForgotVerificationActivity.this, NewPasswordActivity.class);
                            intent.putExtra("id", id);
                            startActivity(intent);

                        } else {
                            Toast.makeText(ForgotVerificationActivity.this, forgotPasswordResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;

                  /*  case AppConstants.APICode.resend_verification_code:

                        ForgotPasswordResponse forgotPasswordResponse1 = new ForgotPasswordResponse();
                        forgotPasswordResponse1 = gson.fromJson(response, ForgotPasswordResponse.class);

                        if (forgotPasswordResponse1.getSTATUS().equals("SUCCESS")) {
                            Toast.makeText(ForgotVerificationActivity.this, forgotPasswordResponse1.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;*/

                }
            } catch (Exception e) {

                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
