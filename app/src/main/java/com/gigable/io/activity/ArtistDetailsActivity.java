package com.gigable.io.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.circularprogressview.android.CircularProgressView;
import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.Models.ServiceResponse.AddDelFavTrkResponse;
import com.gigable.io.Models.ServiceResponse.ArtistResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Common;
import com.gigable.io.Utility.PlayerConstants;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.adapters.ConcerListAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import io.fabric.sdk.android.Fabric;

public class ArtistDetailsActivity extends AppCompatActivity implements ApiResponse, View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private TextView art_name;
    private TextView art_bio;
    private ImageView art_img;
    private ImageView back;
    private ImageView fb;
    private ImageView twitter;
    private ImageView img_play;
    private RecyclerView rv;
    View view;
    public ArrayList<Show> showArrayList = new ArrayList<>();
    private TextView artist_title;
    private TextView total_show;
    private TextView total_track;
    private TextView trck_title;
    private TextView playlist;
    private TextView concert_lable;
    private TextView fetur_track;
    RelativeLayout relativeLayout;
    Intent intent;
    String art_id = "";
    String art_name1 = "";
    String art_img1 = "";
    String fb_url = "";
    String twitter_url = "";
    ConcerListAdapter adapter;
    private Toolbar mToolbar;
    IntentFilter intentFilter;
    private static Common mApp;


    /**
     * Player elements
     */

    RelativeLayout rel_fetur;
    LinearLayout player1;
    private TextView play_title;
    private TextView track_title;
    private TextView art_name_player;
    static ImageView btnPlay;
    private ImageView btnNext;
    private ImageView tkt;
    private ImageView favrit;
    private ImageView share_app;
    public static SeekBar songProgressBar;
    public static TextView songCurrentDurationLabel;
    public static TextView songTotalDurationLabel;
    private ProgressBar mStreamingProgressBar;
    String lgn_id = "";


    String track_id = "";
    String track_url = "";

    /**
     * Track reqire fields
     */

    ArrayList<Track> trackArrayList = new ArrayList<>();
    public static final String Broadcast_PLAY_NEW_AUDIO = "com.gigable.io.PlayNewAudio";

    /**
     * Player play pause elements
     */
    int currant_index = 0;
    boolean click = false;


    private boolean mIsCreating = true;

    static Handler progressBarHandler = new Handler();
    private CircularProgressView mProgressDialog;

    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_artistdetails);

        mContext = this;

        AppConstants.CONTEXT = mContext;

        mApp = (Common) getApplicationContext();

        click = false;

        mProgressDialog = (CircularProgressView) findViewById(R.id.progress_view);
        mProgressDialog.startAnimation();

        // initialization of elements
        init();

        Track track = new Track();
        track.setArt_id(art_id);
        track.setLgn_id(lgn_id);
        HTTPWebRequest.GetArtDetl(this, track, AppConstants.APICode.art_detail, ArtistDetailsActivity.this);

    }

    private void init() {


        //Get value from prefarance
        lgn_id = Pref.getValue(AppConstants.PreftDataKey.lgn_id, "");

        //get intent value
        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            art_id = bundle.getString(AppConstants.SetGetBundleData.art_id);
            art_name1 = bundle.getString(AppConstants.SetGetBundleData.art_name);
            art_img1 = bundle.getString(AppConstants.SetGetBundleData.art_img);
        }

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);

        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setNestedScrollingEnabled(false);

        art_name = (TextView) findViewById(R.id.art_name);
        art_bio = (TextView) findViewById(R.id.art_bio);
        art_img = (ImageView) findViewById(R.id.art_img);
        back = (ImageView) findViewById(R.id.back);
        fb = (ImageView) findViewById(R.id.fb);
        twitter = (ImageView) findViewById(R.id.twitter);
        img_play = (ImageView) findViewById(R.id.img_play);
        artist_title = (TextView) findViewById(R.id.play_title);
        total_show = (TextView) findViewById(R.id.total_show);
        total_track = (TextView) findViewById(R.id.total_track);
        trck_title = (TextView) findViewById(R.id.trck_title);
        playlist = (TextView) findViewById(R.id.playlist);
        relativeLayout = (RelativeLayout) findViewById(R.id.rel_fetur);
        fetur_track = (TextView) findViewById(R.id.fetur_track);
        concert_lable = (TextView) findViewById(R.id.concert_lable);
        view = (View) findViewById(R.id.view);
        rel_fetur = (RelativeLayout) findViewById(R.id.rel_fetur);

        /**
         * Player elements
         */
        player1 = (LinearLayout) findViewById(R.id.player1);
        track_title = (TextView) findViewById(R.id.track_title);
        art_name_player = (TextView) findViewById(R.id.art_name_player);
        btnPlay = (ImageView) findViewById(R.id.btnPlay);
        btnNext = (ImageView) findViewById(R.id.btnNext);
        favrit = (ImageView) findViewById(R.id.favrit);
        tkt = (ImageView) findViewById(R.id.tkt);
        share_app = (ImageView) findViewById(R.id.share_app);

        mStreamingProgressBar = (ProgressBar) findViewById(R.id.startingStreamProgressBar);
        mStreamingProgressBar.setVisibility(View.GONE);

        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);

        songProgressBar.setOnSeekBarChangeListener(this);
        btnPlay.setOnClickListener(this);
        btnNext.setOnClickListener(this);


        if (!art_name1.equals("")) artist_title.setText(art_name1);

        Picasso.with(this)
                .load(art_img1)
                .placeholder(R.mipmap.placeholder)
                .error(R.mipmap.placeholder)
                .into(art_img);


        back.setOnClickListener(this);
        fb.setOnClickListener(this);
        twitter.setOnClickListener(this);
        rel_fetur.setOnClickListener(this);
        favrit.setOnClickListener(this);
        tkt.setOnClickListener(this);
        share_app.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //service is active

        if (mApp.ismIsServiceRunning()) {
            //Unregister the broadcast receivers.

            progressBarHandler.removeCallbacks(mUpdateTimeTask);

            LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReceiverSeekbar);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.back:
                finish();
                break;
            case R.id.fb:
                if (!fb_url.equals("")) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(fb_url));
                    startActivity(browserIntent);
                }
                break;
            case R.id.twitter:
                if (!twitter_url.equals("")) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(twitter_url));
                    startActivity(browserIntent);
                }
                break;
            case R.id.img_play:

                break;

            case R.id.rel_fetur:

                /**
                 * set preference called from playlistpage
                 */

                Pref.setValue(AppConstants.PreftDataKey.songCalledFrom, "PlaylistPage");

                PlayerConstants.SONG_PAUSED = false;

                Common.setSongPlay(false);
                playAudio(currant_index);


                int padding_in_dp = 175;
                final float scale = getResources().getDisplayMetrics().density;
                int padding_in_px = (int) (padding_in_dp * scale + 0.5f);

                player1.setVisibility(View.VISIBLE);
                rv.setPadding(0, 0, 0, padding_in_px);
                changeButton();


                break;

            case R.id.btnPlay:
                // playMusic();

                if (mApp.getService().mediaPlayer.isPlaying()) {

                    progressBarHandler.removeCallbacks(mUpdateTimeTask);
                    Controls.pauseControl(getApplicationContext());

                } else {
                    Controls.playControl(getApplicationContext());
                }


                break;
            case R.id.btnNext:

                callNextSong();

                break;

            case R.id.favrit:
                Track track = new Track();
                track.setUtp_trk_id(track_id);
                track.setUsrplay_lgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));
                HTTPWebRequest.AddTrackFav(ArtistDetailsActivity.this, track, AppConstants.APICode.track_fav, ArtistDetailsActivity.this);
                break;

            case R.id.tkt:
                Bundle bundle1 = new Bundle();
                bundle1.putString(AppConstants.SetGetBundleData.trk_id, track_id);
                Utility.RedirectToActivity(ArtistDetailsActivity.this, ConcertsList.class, false, bundle1);
                break;

            case R.id.share_app:

                String shareURL = Pref.getValue(AppConstants.PreftDataKey.android_link, "");

                if (!shareURL.equals("")) {

                    Intent newIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(Pref.getValue(AppConstants.PreftDataKey.android_link, "")));
                    startActivity(newIntent);
                }
                break;
        }
    }

    public static void changeButton() {

        if (PlayerConstants.SONG_PAUSED) {
            btnPlay.setImageResource(R.mipmap.paly_2x);

        } else {
            btnPlay.setImageResource(R.mipmap.pause_2x);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        // Pref.setValue(AppConstants.PreftDataKey.backgroundApp,"false");


        if (mIsCreating == false) {
            progressBarHandler.postDelayed(mUpdateTimeTask, 100);
            mIsCreating = false;
        }

        //Update the seekbar.

        if (Common.isSongPlay() == true) {
            try {
                setSeekbarDuration(mApp.getService().getMediaPlayer().getDuration() / 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void playAudio(int audioIndex) {

        //check play click or not
        if (click == false) {

            if (mApp.ismIsServiceRunning() == true) {

                Log.e("inside service call","true");

                progressBarHandler.removeCallbacks(mUpdateTimeTask);
                songProgressBar.setProgress(0);
                btnPlay.setVisibility(View.GONE);

                mProgressDialog.setVisibility(View.VISIBLE);
                Common.setSongPlay(false);

                mApp.getService().mediaPlayer.stop();


                new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();

                track_title.setText(trackArrayList.get(0).getTrk_title());
                //art_name_player.setText(trackArrayList.get(0).getArt_name());
                track_id = trackArrayList.get(0).getTrk_id();
                track_url = trackArrayList.get(0).getTrk_audio();


                if (trackArrayList.get(0).isFav_status() == false) {
                    favrit.setImageResource(R.mipmap.fav);
                } else {
                    favrit.setImageResource(R.mipmap.fav_fill);
                }

                StorageUtil storage = new StorageUtil(getApplicationContext());
                storage.storeAudio(trackArrayList);
                storage.storeAudioIndex(0);



               /* Intent broadcastIntent = new Intent(Broadcast_PLAY_NEW_AUDIO);
                sendBroadcast(broadcastIntent);*/

                Controls.nextPlaylistControl(getApplicationContext());

                click = true;

            } else {

                //progressBarHandler.removeCallbacks(mUpdateTimeTask);
                btnPlay.setVisibility(View.GONE);
                mProgressDialog.setVisibility(View.VISIBLE);

                new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();

                track_title.setText(trackArrayList.get(0).getTrk_title());
                art_name_player.setText(trackArrayList.get(0).getArt_name());
                track_id = trackArrayList.get(0).getTrk_id();
                track_url = trackArrayList.get(0).getTrk_audio();

                if (trackArrayList.get(0).isFav_status() == false) {
                    favrit.setImageResource(R.mipmap.fav);
                } else {
                    favrit.setImageResource(R.mipmap.fav_fill);
                }


                StorageUtil storage = new StorageUtil(getApplicationContext());
                storage.storeAudio(trackArrayList);
                storage.storeAudioIndex(0);

                Intent playerIntent = new Intent(this, MediaPlayerService.class);
                startService(playerIntent);
                //  bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);

                mApp.startService();
                click = true;
            }

        } else {
            Controls.playControl(getApplicationContext());
        }
    }

    private void setSeekbarDuration(int duration) {
        try {
            progressBarHandler.postDelayed(mUpdateTimeTask, 100);
        } catch (Exception e) {

        }
    }


    static Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = 0;
            long currentDuration = 0;


            Log.e("call update time","artis page");

            try {
                totalDuration = mApp.getService().getMediaPlayer().getDuration();
                currentDuration = mApp.getService().getMediaPlayer().getCurrentPosition();


                songCurrentDurationLabel.setText(Utility.milliSecondsToTimer(currentDuration));

                songTotalDurationLabel.setText(Utility.milliSecondsToTimer(totalDuration));


                int progress = (int) (Utility.getProgressPercentage(currentDuration, totalDuration));


                songProgressBar.setProgress(progress);

                progressBarHandler.postDelayed(this, 100);

            } catch (
                    Exception e
                    )

            {
                e.printStackTrace();
            }
        }
    };


    public void onStart() {
        super.onStart();

        //Initialize the broadcast manager that will listen for track changes.
        LocalBroadcastManager.getInstance(mContext)
                .registerReceiver((mReceiverSeekbar),
                        new IntentFilter(Common.UPDATE_UI_BROADCAST1));


        if (mApp.ismIsServiceRunning()) {

           // int padding = getResources().getDimensionPixelOffset(R.dimen.padding);

            int padding_in_dp = 175;
            final float scale = getResources().getDisplayMetrics().density;
            int padding_in_px = (int) (padding_in_dp * scale + 0.5f);


            player1.setVisibility(View.VISIBLE);
            btnPlay.setVisibility(View.VISIBLE);
            rv.setPadding(0, 0, 0, padding_in_px);


            if(Common.isSongPlay() == true){
                btnPlay.setVisibility(View.VISIBLE);
            } else {
                mProgressDialog.setVisibility(View.VISIBLE);
                btnPlay.setVisibility(View.GONE);
            }

                String[] updateFlags = new String[]{
                        //Common.UPDATE_SEEKBAR_DURATION,
                        Common.UPDATE_TRACK_TITLE,
                        Common.UPDATE_ARTIST_NAME,
                        Common.UPDATE_TRACK_ID,
                        Common.UPDATE_FAVOURITE,
                        Common.UPDATE_TRACKURL
                };

                String[] flagValues = new String[]{"" +
                        //mApp.getService().getMediaPlayer().getDuration(),
                        mApp.getService().audioList.get(mApp.getService().audioIndex).getTrk_title(),
                        mApp.getService().audioList.get(mApp.getService().audioIndex).getArt_name(),
                        mApp.getService().audioList.get(mApp.getService().audioIndex).getTrk_id(),
                        mApp.getService().audioList.get(mApp.getService().audioIndex).getFavourite_status(),
                        mApp.getService().audioList.get(mApp.getService().audioIndex).getTrk_audio(),
                };
                mApp.broadcastUpdateUICommand(updateFlags, flagValues);

                changeButton();

        }
    }


    /**
     * Updates this activity's UI elements based on the passed intent's
     * update flag(s).
     */
    BroadcastReceiver mReceiverSeekbar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //Grab the bundle from the intent.
            Bundle bundle = intent.getExtras();


            if (intent.hasExtra(Common.UPDATE_TRACK_TITLE)) {
                track_title.setText(bundle.getString(Common.UPDATE_TRACK_TITLE));
            }
            if (intent.hasExtra(Common.UPDATE_ARTIST_NAME)) {
                art_name.setText(bundle.getString(Common.UPDATE_ARTIST_NAME));
            }
            if (intent.hasExtra(Common.UPDATE_TRACK_ID)) {
                track_id = String.valueOf(bundle.get(Common.UPDATE_TRACK_ID));
            }
            if (intent.hasExtra(Common.UPDATE_TRACKURL)) {
                track_url = String.valueOf(bundle.get(Common.UPDATE_TRACKURL));
            }
            if (intent.hasExtra(Common.UPDATE_FAVOURITE)) {
                if (String.valueOf(bundle.get(Common.UPDATE_TRACK_ID)).equals("false")) {
                    favrit.setImageResource(R.mipmap.fav);
                } else {
                    favrit.setImageResource(R.mipmap.fav_fill);
                }
            }
            if (intent.hasExtra(Common.UPDATE_HIDE_PROGRESS_DIALOG)) {

                Log.e("get call of song play","true");

                mProgressDialog.setVisibility(View.GONE);
                btnPlay.setVisibility(View.VISIBLE);

                //set song is playing now
                Common.setSongPlay(true);
            }

            if (intent.hasExtra(Common.UPDATE_SONG_COMPLETE)) {
                callNextSong();
            }


            //Updates the duration of the SeekBar.
            if (intent.hasExtra(Common.UPDATE_SEEKBAR_DURATION)) {
                setSeekbarDuration(Integer.parseInt(
                        bundle.getString(
                                Common.UPDATE_SEEKBAR_DURATION))
                );

                Log.e("artist page","true");
            }
        }
    };

    private void callNextSong() {

        btnPlay.setVisibility(View.GONE);
        mProgressDialog.setVisibility(View.VISIBLE);
        Common.setSongPlay(false);

        songProgressBar.setProgress(0);
        progressBarHandler.removeCallbacks(mUpdateTimeTask);
        songCurrentDurationLabel.setText("0.00");
        songTotalDurationLabel.setText("0.00");

        Controls.nextControl(getApplicationContext());
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        progressBarHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        if (Common.isSongPlay() == true) {

            progressBarHandler.removeCallbacks(mUpdateTimeTask);
            int totalDuration = mApp.getService().mediaPlayer.getDuration();
            int currentPosition = Utility.progressToTimer(seekBar.getProgress(), totalDuration);
            mApp.getService().mediaPlayer.seekTo(currentPosition);
            setSeekbarDuration(mApp.getService().getMediaPlayer().getDuration() / 1000);
        }
    }


    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.art_detail:

                        ArtistResponse artistResponse = new ArtistResponse();
                        artistResponse = gson.fromJson(response, ArtistResponse.class);

                        if (artistResponse.getSTATUS().equals("SUCCESS")) {
                            //Toast.makeText(LoginActivity.this, loginResponse2.getMESSAGES(), Toast.LENGTH_LONG).show();

                            //set responcer data object in loginrequest
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Track.class, new ArtistResponse())
                                    .create();
                            Track track = gson.fromJson(response, Track.class);

                            total_show.setText("" + track.getShow().size() + " Concerts");
                            total_track.setText("" + track.getTrack().size() + " Tracks");

                            showArrayList = track.getShow();

                            fb_url = track.getArt_facebook();
                            twitter_url = track.getArt_twitter();

                            if (track.getArt_name() != null) art_name.setText(track.getArt_name());
                            if (track.getArt_shortbio() != null)
                                art_bio.setText(track.getArt_shortbio());

                           /* Picasso.with(this)
                                    .load(track.getArt_image())
                                    .placeholder(R.mipmap.placeholder)
                                    .error(R.mipmap.placeholder)
                                    .into(art_img);*/

                            /**
                             * Set array value in track arrayList
                             */
                            ArrayList<Track> results = track.getTrack();
                            trackArrayList = results;

                            if (track.getTrack().size() > 0) {

                                relativeLayout.setVisibility(View.VISIBLE);
                                fetur_track.setVisibility(View.VISIBLE);

                                if (!track.getTrack().get(0).getTrk_title().equals("")) {
                                    trck_title.setText(track.getTrack().get(0).getTrk_title());
                                }
                                if (!track.getTrack().get(0).getPlay_name().equals("")) {
                                    playlist.setText(track.getTrack().get(0).getPlay_name());
                                }

                            } else {
                                relativeLayout.setVisibility(View.GONE);
                                fetur_track.setVisibility(View.GONE);
                            }

                            if (showArrayList.size() > 0) {
                                concert_lable.setVisibility(View.VISIBLE);
                                view.setVisibility(View.VISIBLE);
                                setupAdapter(showArrayList, art_name1, track.getArt_image());
                            }
                        }
                        break;
                    case AppConstants.APICode.track_fav:

                        AddDelFavTrkResponse addDelFavTrkResponse = new AddDelFavTrkResponse();
                        addDelFavTrkResponse = gson.fromJson(response, AddDelFavTrkResponse.class);

                        if (addDelFavTrkResponse.getSTATUS().equals("SUCCESS")) {
                            //Toast.makeText(LoginActivity.this, loginResponse2.getMESSAGES(), Toast.LENGTH_LONG).show();

                            Toast.makeText(ArtistDetailsActivity.this, addDelFavTrkResponse.getMESSAGES(), Toast.LENGTH_LONG).show();

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Track.class, new AddDelFavTrkResponse())
                                    .create();

                            Track track = gson.fromJson(response, Track.class);
                            if (track.getFp_status().equals("2")) {
                                favrit.setImageResource(R.mipmap.fav);
                            } else {
                                favrit.setImageResource(R.mipmap.fav_fill);
                            }
                            /**
                             * set Praferance fro referesh Home page
                             */
                            Pref.setValue("Refresh", "0");
                        }
                        break;


                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    private void setupAdapter(final ArrayList<Show> mData, String art_name1, String art_img1) {

        adapter = new ConcerListAdapter(this, this, mData, art_name1, art_img1);
        rv.setAdapter(adapter);

    }


    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }


}
