package com.gigable.io.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

import static com.gigable.io.R.id.view;

public class StartScreen extends AppCompatActivity implements View.OnClickListener {

    TextView signin, signup;
    private SparseIntArray mErrorString;
    private static final int REQUEST_PERMISSIONS = 20;

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_start_screen);

        mContext = this;
        AppConstants.CONTEXT = mContext;


        mErrorString = new SparseIntArray();

        if (checkVersionMarsh()) {
            if (checkAndRequestPermissions() == false) {
                checkAndRequestPermissions();
            }
        }



        //this is for logout
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);

        init();
    }

    private void init() {
        signin = (TextView) findViewById(R.id.signin);
        signup = (TextView) findViewById(R.id.signup);

        signin.setOnClickListener(this);
        signup.setOnClickListener(this);
    }





    private boolean checkAndRequestPermissions() {


        int walkeLock = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WAKE_LOCK);

        int accessLocation = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        int contentControl = ContextCompat.checkSelfPermission(this,
                Manifest.permission.MEDIA_CONTENT_CONTROL);

        int setting = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_SETTINGS);

        int secureSetting = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_SECURE_SETTINGS);



        List<String> listPermissionsNeeded = new ArrayList<>();

        if (walkeLock != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WAKE_LOCK);
        }
        if (accessLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (contentControl != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.MEDIA_CONTENT_CONTROL);
        }
        if (setting != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_SETTINGS);
        }
        if (secureSetting != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_SECURE_SETTINGS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override  public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
                break;
        }
    }
    protected boolean checkVersionMarsh() {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.signin:

                Intent i = new Intent(StartScreen.this, LoginActivity.class);
                startActivity(i);

                break;

            case R.id.signup:

                Intent i1 = new Intent(StartScreen.this, RegisterActivity.class);
                startActivity(i1);
                break;
        }
    }
}
