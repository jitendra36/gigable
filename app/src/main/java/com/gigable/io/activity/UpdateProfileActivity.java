package com.gigable.io.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.LoginRequest;
import com.gigable.io.Models.ServiceRequest.RegisterRequest;
import com.gigable.io.Models.ServiceResponse.LoginResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;


public class UpdateProfileActivity extends BaseActivity implements View.OnClickListener, ApiResponse{
    EditText name, upload_img, phonenumber, email_upd;
    TextView Update, title;
    RadioButton gender_radio_male, gender_radio_female, gender_radio_other;
    ImageView mImage;
    Context ctx;


    private ArrayList<String> imageURIArray = new ArrayList<>();
    private ArrayList<HashMap<String, String>> imageArry = new ArrayList<>();
    String selectedImagePath;

    private static final int REQUEST_TAKE_GALLERY_IMAGE = 2;

    String numberToPass="";

    String id_preferance = null;
    String name_preferance;
    String email_preferance;


    boolean internetConnection = false;
    private SparseIntArray mErrorString;
    private static final int REQUEST_PERMISSIONS = 21;

    private static final int GALLERY_INTENT_CALLED = 21;
    private static final int GALLERY_KITKAT_INTENT_CALLED = 22;

    Context mContext;
    String usr_name = "";
    String lgn_email = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_updateprofile);

        mContext = this;
        AppConstants.CONTEXT = mContext;


        //overridePendingTransition(0, 0);

        //getLayoutInflater().inflate(R.layout.activity_updateprofile, frameLayout);
        //mDrawerList.setItemChecked(position, true);


        //setTitle(listArray[2]);

        //SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        id_preferance = SaveSharedPreference.getUserID(UpdateProfileActivity.this);


        usr_name = Pref.getValue(AppConstants.PreftDataKey.usr_name, "");
        lgn_email = Pref.getValue(AppConstants.PreftDataKey.lgn_email, "");

        //name_preferance = pref.getString("name", null);
        //phone_preferance = pref.getString("phone", null);
        //gender_preferance = pref.getString("gender", null);
        //email_preferance = pref.getString("email", null);
        //image_preferance = pref.getString("image",null);


        mErrorString = new SparseIntArray();


        //this is for logout
      /*  IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/

        init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Bundle bundle = new Bundle();
        Utility.RedirectToActivity(UpdateProfileActivity.this, HomeScreenActivity.class, false, bundle);
    }

    //iniotialization of elements
    private void init() {

        name = (EditText) findViewById(R.id.name_upd);
        email_upd = (EditText) findViewById(R.id.email_upd);

        Update = (TextView) findViewById(R.id.Update);
        Update.setOnClickListener(this);

        title = (TextView) findViewById(R.id.title);

        if (!usr_name.equals("")){
            name.setText(usr_name);
        }
        if (!lgn_email.equals("")){
            email_upd.setText(lgn_email);
        }
    }

    //click event of elements
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.Update:
                validation();

                if (validation() == true) {
                        if (id_preferance != null) {
                            RegisterRequest registerRequest = new RegisterRequest();
                            registerRequest.setLgn_id(id_preferance);
                            registerRequest.setUsr_name(name.getText().toString());
                            registerRequest.setUsr_phone("123456789");
                            registerRequest.setUsr_gender("1");

                            HTTPWebRequest.UpdateProfile(UpdateProfileActivity.this, registerRequest, imageArry, AppConstants.APICode.update_profile, UpdateProfileActivity.this);
                        } else {
                            Toast.makeText(UpdateProfileActivity.this, "You can not Update Profile", Toast.LENGTH_LONG).show();
                        }
                    }
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode== KeyEvent.KEYCODE_BACK) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    //manage api resdponce
    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.update_profile:
                        LoginResponse loginResponse3 = new LoginResponse();
                        loginResponse3 = gson.fromJson(response, LoginResponse.class);

                        if (loginResponse3.getSTATUS().equals("SUCCESS")) {

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(LoginRequest.class, new LoginResponse())
                                    .create();
                            LoginRequest loginRequest = gson.fromJson(response, LoginRequest.class);

                            Pref.setValue(AppConstants.PreftDataKey.usr_name, loginRequest.getUsr_name() + "");
                            Pref.setValue(AppConstants.PreftDataKey.lgn_email, loginRequest.getLgn_email() + "");

                            Toast.makeText(UpdateProfileActivity.this, loginResponse3.getMESSAGES(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(UpdateProfileActivity.this, HomeScreenActivity.class);
                            position = 1;
                            startActivity(i);
                        } else {
                            Toast.makeText(UpdateProfileActivity.this, loginResponse3.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            } catch (Exception e) {
            }
        }
    }

    //validation of form
    private boolean validation() {

        boolean value = true;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-z]+";

        String PhoneNo = "+123-456 7890";
        String Regex = "[^\\d]";
        String PhoneDigits = PhoneNo.replaceAll(Regex, "");

        if (!(name.getText().toString().length() > 0)) {
            name.setError("required field");
            value = false;
        }
        else {
            value = true;
        }
        return value;
    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            internetConnection = true;
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            internetConnection = false;
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }
        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void networkError(int apiCode) {
    }

    @Override
    public void responseError(String responseError, int apiCode) {
    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {
    }

}
