package com.gigable.io.activity;

import android.content.ContentValues;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.GNotification;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceResponse.NotificationResponce;
import com.gigable.io.Models.ServiceResponse.ShowByTrackResponce;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.adapters.NotificationAdapter;
import com.gigable.io.adapters.UpcomingConcertstAdapter;
import com.gigable.io.database.GigableDatabaseHelper;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class NotificationListActivity extends AppCompatActivity implements View.OnClickListener,ApiResponse {

    private GigableDatabaseHelper mDatabaseHelper;
    private Context mContext;
    private List<GNotification> notificationList;
    private NotificationAdapter mNotificationAdapter;
    private LinearLayout llEmptyNotifications;

    RecyclerView rv;
    private Toolbar mToolbar;

    private TextView title;
    private TextView total_show;
    private TextView total_track;
    private View view;
    private ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_notification_list);

        mContext = this;
        AppConstants.CONTEXT = mContext;

        //initialization of view
        initView();


        PlayListRequest playListRequest = new PlayListRequest();
        playListRequest.setLgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));

        HTTPWebRequest.GetNotification(this, playListRequest, AppConstants.APICode.notification, NotificationListActivity.this);
    }

    private void initView() {

        //Define recyclerview
        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);



        mDatabaseHelper=new GigableDatabaseHelper(mContext);

        llEmptyNotifications = (LinearLayout) findViewById(R.id.llEmptyNotifications);


        //Set toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);

        //Set title of the toolbar
        title = (TextView) findViewById(R.id.play_title);
        title.setText("Notification");

        //toolbar elements
        back = (ImageView) findViewById(R.id.back);
        total_show = (TextView) findViewById(R.id.total_show);
        total_track = (TextView) findViewById(R.id.total_track);
        view = (View) findViewById(R.id.viewCT);

        total_show.setVisibility(View.GONE);
        total_track.setVisibility(View.GONE);
        view.setVisibility(View.GONE);

        back.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.back:
                Bundle bundle = new Bundle();
                Utility.RedirectToActivity(NotificationListActivity.this, HomeScreenActivity.class, false, bundle);
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

     /*   mDatabaseHelper.open();
        notificationList=mDatabaseHelper.getNotificationList();
        mDatabaseHelper.close();

        if(notificationList!=null && !notificationList.isEmpty()){
            llEmptyNotifications.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
            mNotificationAdapter=new NotificationAdapter(mContext, notificationList);
            rv.setAdapter(mNotificationAdapter);
        } else {
            llEmptyNotifications.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }*/

    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.notification:

                        NotificationResponce notificationResponce = gson.fromJson(response, NotificationResponce.class);
                        if (notificationResponce.getDATA().size()>0) {
                            setupAdapter(notificationResponce.getDATA());
                        }
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    private void setupAdapter(final ArrayList<Show> mData) {
        if (mData.size() > 0) {
            mNotificationAdapter = new NotificationAdapter(this, mData);
            rv.setAdapter(mNotificationAdapter);
        }
    }


    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }
}
