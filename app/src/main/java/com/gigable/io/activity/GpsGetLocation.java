package com.gigable.io.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.Models.ServiceResponse.AddDelFavTrkResponse;
import com.gigable.io.Models.ServiceResponse.PlaylistResponse;
import com.gigable.io.Models.ServiceResponse.ShowResponse;
import com.gigable.io.Models.ServiceResponse.TrackResponse;
import com.gigable.io.Models.ServiceResponse.UpcomingConcertsResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;

public class GpsGetLocation extends Activity implements ApiResponse {

    double latitude = 0.0d;
    double longitude = 0.0d;
    GPSTracker gps;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;


    ProgressDialog progressdialog;

    private Context mContext;

    UpcomingConcertsResponse upcomingConcertsResponse;

    private String version = "";
    private String latest_version = "";

    private boolean resumeApp = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps_get_location);

        mContext = this;
        AppConstants.CONTEXT = mContext;

        progressdialog = new ProgressDialog(GpsGetLocation.this);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        version = pInfo.versionName;

        resumeApp = false;

        HTTPWebRequest.GetVersion(GpsGetLocation.this, AppConstants.APICode.getVersion, GpsGetLocation.this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (resumeApp == true) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    resumeApp = false;
                    Intent i = new Intent(GpsGetLocation.this, HomeScreenActivity.class);
                    startActivity(i);
                    finish();

                }
            }, SPLASH_TIME_OUT);
        }
    }


    private void startApp() {
        gps = new GPSTracker(GpsGetLocation.this);

        // check if GPS enabled
        if (gps.canGetLocation()) {


            latitude = gps.getLatitude();
            longitude = gps.getLongitude();


            // SaveSharedPreference.setLatitude(this, String.valueOf(latitude));
            // SaveSharedPreference.setLongitude(this, String.valueOf(longitude));

            Pref.setValue(AppConstants.PreftDataKey.latitude, latitude + "");
            Pref.setValue(AppConstants.PreftDataKey.longitude, longitude + "");

            //  Pref.setValue(AppConstants.PreftDataKey.INSTALL_DETAIL,"install");

            Intent i = new Intent(GpsGetLocation.this, HomeScreenActivity.class);
            startActivity(i);
            finish();

        } else {
            resumeApp = true;
            gps.showSettingsAlert();
        }
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.getVersion:

                        upcomingConcertsResponse = gson.fromJson(response, UpcomingConcertsResponse.class);


                        gson = new GsonBuilder()
                                .registerTypeAdapter(PlayListRequest.class, new UpcomingConcertsResponse())
                                .create();
                        PlayListRequest playListRequest = gson.fromJson(response, PlayListRequest.class);

                        latest_version = playListRequest.getVersion();

                        if (compareVersionNames(version, latest_version) != 0) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setCancelable(false);
                            builder.setMessage("There is newer version of this application available, click OK to upgrade now?")
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        //if the user agrees to upgrade
                                        public void onClick(DialogInterface dialog, int id) {
                                            //start downloading the file using the download manager
                                            Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                            try {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                            } catch (android.content.ActivityNotFoundException anfe) {
                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                            }
                                        }
                                    });
                            //show the alert message
                            builder.create().show();
                        } else {

                            if (Pref.getValue(AppConstants.PreftDataKey.latitude, "").equals("") || Pref.getValue(AppConstants.PreftDataKey.latitude, "").equals("")) {

                                Toast toast = Toast.makeText(this, "Getting Location", Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();


                                startApp();
                                // close this activity


                            } else {
                                Intent i = new Intent(GpsGetLocation.this, HomeScreenActivity.class);
                                startActivity(i);
                                finish();
                            }
                        }

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }


    public int compareVersionNames(String oldVersionName, String newVersionName) {
        int res = 0;

        String[] oldNumbers = oldVersionName.split("\\.");
        String[] newNumbers = newVersionName.split("\\.");

        // To avoid IndexOutOfBounds
        int maxIndex = Math.min(oldNumbers.length, newNumbers.length);

        for (int i = 0; i < maxIndex; i++) {
            int oldVersionPart = Integer.valueOf(oldNumbers[i]);
            int newVersionPart = Integer.valueOf(newNumbers[i]);

            if (oldVersionPart < newVersionPart) {
                res = -1;
                break;
            } else if (oldVersionPart > newVersionPart) {
                res = 1;
                break;
            }
        }

        // If versions are thes ame so far, but they have different length...
        if (res == 0 && oldNumbers.length != newNumbers.length) {
            res = (oldNumbers.length > newNumbers.length) ? 1 : -1;
        }

        return res;
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }
}
