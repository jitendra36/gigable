package com.gigable.io.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.Tickets;
import com.gigable.io.Models.ServiceResponse.ShowDetailsResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import io.fabric.sdk.android.Fabric;

public class PaymentInfoActivity extends AppCompatActivity implements ApiResponse, View.OnClickListener {

    TextView msg_pay, show_titl, show_plc, show_date, tkt_qty, totl_cost;
    private TextView purchase;
    private TextView tkt_info;
    private ImageView back;

    Intent intent;
    String show_titl_int, totl_cost_int, msg_pay_int, show_plc_int, show_date_int, tkt_qty_int, pay_status_int, tkt_id_int;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_payment_msg);


        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            tkt_id_int = bundle.getString(AppConstants.SetGetBundleData.utkt_id);
            show_plc_int = bundle.getString(AppConstants.SetGetBundleData.show_place);
        }


        Tickets tickets = new Tickets();
        tickets.setLgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));
        tickets.setUtkt_id(tkt_id_int);

        HTTPWebRequest.PymentInfo(PaymentInfoActivity.this, tickets, AppConstants.APICode.pay_info, PaymentInfoActivity.this);


        init();

        //this is for logout
      /*  IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/
    }

    // initailize elements
    private void init() {

        toolbar = (Toolbar) findViewById(R.id.action_bar);
        setSupportActionBar(toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);

        msg_pay = (TextView) findViewById(R.id.msg_pay);
        show_titl = (TextView) findViewById(R.id.show_titl);
        show_plc = (TextView) findViewById(R.id.show_plc);
        show_date = (TextView) findViewById(R.id.show_date);
        tkt_qty = (TextView) findViewById(R.id.tkt_qty);
        totl_cost = (TextView) findViewById(R.id.totl_cost);
        purchase = (TextView) findViewById(R.id.purchase);
        tkt_info = (TextView) findViewById(R.id.tkt_info);
        back = (ImageView) findViewById(R.id.back);

        purchase.setVisibility(View.GONE);
        tkt_info.setVisibility(View.VISIBLE);

        //msg_pay.setText(msg_pay_int);
        //show_titl.setText(show_titl_int);

        //show_date.setText(show_date_int);
        //tkt_qty.setText(tkt_qty_int + " Tickets,Best Available");
        //totl_cost.setText(totl_cost_int);

        back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back:

                Bundle bundle = new Bundle();

                Utility.RedirectToActivity(this, HomeScreenActivity.class, false, bundle);

                break;
        }
    }


    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {

                    case AppConstants.APICode.pay_info:

                        ShowDetailsResponse showDetailsResponse = new ShowDetailsResponse();
                        showDetailsResponse = gson.fromJson(response, ShowDetailsResponse.class);

                        if (showDetailsResponse.getSTATUS().equals("SUCCESS")) {

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Show.class, new ShowDetailsResponse())
                                    .create();
                            Show show = gson.fromJson(response, Show.class);

                            msg_pay.setText("Payment successful");
                            show_plc.setText(show_plc_int);

                            if (show.getShow_title() != null) {
                                show_titl.setText(show.getShow_title());
                            }
                            if (show.getVen_name() != null) {
                                show_plc.setText(show.getVen_name());
                            }
                            if (show.getShow_date() != null) {
                                show_date.setText(show.getShow_date());
                            }
                            if (show.getUtkt_qty() != null) {
                                tkt_qty.setText(show.getUtkt_qty() + " Tickets,Best Available");
                            }
                            if (show.getUtkt_price() != null) {
                                totl_cost.setText("$"+show.getUtkt_price());
                            }

                        } else {

                            Toast.makeText(PaymentInfoActivity.this, showDetailsResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }

                        break;

                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }


}
