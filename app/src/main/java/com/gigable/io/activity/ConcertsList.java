package com.gigable.io.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ActivityCommunicator;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceResponse.ShowByTrackResponce;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.adapters.UpcomingConcertstAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Citrsubug on 11/10/2016.
 */
public class ConcertsList extends AppCompatActivity implements ApiResponse,View.OnClickListener {


    UpcomingConcertstAdapter adapter;
    Context context;
    RecyclerView rv;
    Context mContext;

    String trk_id_intent;
    String total_track_intent;

    TextView total_show;
    Intent intent;


    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();

    private ActivityCommunicator activityCommunicator;

    ArrayList<Show> myList;
    String lgn_id = "";

    private Toolbar mToolbar;
    private ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.showlist);

        mContext = this;
        AppConstants.CONTEXT = mContext;

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);



      //  total_show = (TextView) findViewById(R.id.total_show);
        lgn_id = Pref.getValue(AppConstants.PreftDataKey.lgn_id, "");

        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            trk_id_intent = bundle.getString(AppConstants.SetGetBundleData.trk_id, null);
        }

        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        back = (ImageView) findViewById(R.id.back);

        back.setOnClickListener(this);



        //this is for logout
      /*  IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/


        getShow();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.back :

                finish();

                break;
        }
    }




        private void getShow() {
        PlayListRequest playListRequest = new PlayListRequest();
        playListRequest.setLgn_id(lgn_id);
        playListRequest.setLatitude(Pref.getValue(AppConstants.PreftDataKey.latitude, ""));
        playListRequest.setLongitude(Pref.getValue(AppConstants.PreftDataKey.longitude, ""));
        playListRequest.setTrk_id(trk_id_intent);

        HTTPWebRequest.GetShow(ConcertsList.this, playListRequest, AppConstants.APICode.showlist, ConcertsList.this);
    }

    private void setupAdapter(final ArrayList<Show> mData) {
        if (mData.size() > 0) {
            adapter = new UpcomingConcertstAdapter(context, this, mData);
            rv.setAdapter(adapter);
        }
    }


    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.showlist:

                        ShowByTrackResponce showByTrackResponce = gson.fromJson(response, ShowByTrackResponce.class);
                        if (showByTrackResponce.getDATA().size()>0) {
                            setupAdapter(showByTrackResponce.getDATA());
                        }
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }



}
