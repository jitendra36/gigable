package com.gigable.io.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceResponse.ChangeLocationResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.adapters.ChangeLocationAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class ChangeLocationActivity extends AppCompatActivity implements ApiResponse {


    private RecyclerView rv;
    ChangeLocationAdapter adapter;
    ArrayList<PlayListRequest> results = new ArrayList<>();
    Intent intent;
    ImageView close;

    String from_location = "";
    String totalshow = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_change_location);


        init();

        HTTPWebRequest.GetLocation(ChangeLocationActivity.this, AppConstants.APICode.change_location, ChangeLocationActivity.this);

        //this is for logout
     /*   IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/
    }

    private void init() {


        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            from_location = bundle.getString(AppConstants.SetGetBundleData.from_loc);
            totalshow = bundle.getString(AppConstants.SetGetBundleData.totalshow);
        }
        close = (ImageView) findViewById(R.id.close);
        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        rv.addOnItemTouchListener(
                new RecyclerItemClickListener(this, rv, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Pref.setValue(AppConstants.PreftDataKey.latitude, results.get(position).getLoc_latitude().toString() + "");
                        Pref.setValue(AppConstants.PreftDataKey.longitude, results.get(position).getLoc_longitude() + "");

                        Log.e("lat",results.get(position).getLoc_latitude().toString());
                        Log.e("long",results.get(position).getLoc_longitude().toString());

                        if (from_location.equals("1")) {

                            Pref.setValue("Refresh", "0");
                            Bundle bundle = new Bundle();
                            Utility.RedirectToActivity(ChangeLocationActivity.this, HomeScreenActivity.class, false, bundle);
                            finish();
                        }
                        else {
                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.SetGetBundleData.totalshow, totalshow);
                            Utility.RedirectToActivity(ChangeLocationActivity.this, UpcomingConcerts.class, false, bundle);
                            finish();
                        }
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                }));

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        if (from_location.equals("2")){
            UpcomingConcerts.fa.finish();
        }
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.change_location:

                        ChangeLocationResponse changeLocationResponse = gson.fromJson(response, ChangeLocationResponse.class);

                        results = changeLocationResponse.getDATA();

                        setupAdapter(results);

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    private void setupAdapter(final ArrayList<PlayListRequest> mData) {

        if (mData.size() > 0) {
            adapter = new ChangeLocationAdapter(this, this, mData);
            rv.setAdapter(adapter);
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }
}
