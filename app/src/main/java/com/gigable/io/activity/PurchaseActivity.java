package com.gigable.io.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.OrderRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.Tickets;
import com.gigable.io.Models.ServiceResponse.OrderResponse;
import com.gigable.io.Models.ServiceResponse.ShowDetailsResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import io.fabric.sdk.android.Fabric;

public class PurchaseActivity extends AppCompatActivity implements ApiResponse, AdapterView.OnItemSelectedListener, View.OnClickListener {

    TextView show_title, show_venue, show_date, num_tkt, totl_cost, pay;
    Spinner tkt_typ_spinner;
    ImageView pluse, minus;
    ImageView back;

    int counter = 0;

    int position_Spinner = 0;

    String[] array_rupes;
    String[] array_tkt_id;
    String[] array_tktTyp_id;

    String tkt_id;
    String tkt_tye_id;

    String rupee="";

    Intent intent;
    String show_id_int;

    String lgn_id;


    int tktTypSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_purchase);

        Toolbar toolbar = (Toolbar) findViewById(R.id.action_bar);
        setSupportActionBar(toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);

        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            show_id_int = bundle.getString(AppConstants.SetGetBundleData.show_id);
        }

        lgn_id = Pref.getValue(AppConstants.PreftDataKey.lgn_id, "");

        Show show = new Show();
        show.setLgn_id(lgn_id);
        show.setShow_id(show_id_int);

        HTTPWebRequest.ShowDetails(PurchaseActivity.this, show, AppConstants.APICode.show_details, PurchaseActivity.this);

        init();


        //this is for logout
     /*   IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/
    }

    private void init() {

        show_title = (TextView) findViewById(R.id.show_title);
        show_venue = (TextView) findViewById(R.id.show_venu);
        show_date = (TextView) findViewById(R.id.show_date);
        num_tkt = (TextView) findViewById(R.id.num_tkt);
        num_tkt = (TextView) findViewById(R.id.num_tkt);
        totl_cost = (TextView) findViewById(R.id.totl_cost);
        pay = (TextView) findViewById(R.id.pay);
        back = (ImageView) findViewById(R.id.back);

        tkt_typ_spinner = (Spinner) findViewById(R.id.tkt_typ);

        pluse = (ImageView) findViewById(R.id.plus);
        minus = (ImageView) findViewById(R.id.minus);

        pluse.setOnClickListener(this);
        minus.setOnClickListener(this);
        pay.setOnClickListener(this);
        back.setOnClickListener(this);
        tkt_typ_spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View v) {

        boolean showText = false;

        boolean totalcost_txt = false;

        switch (v.getId()) {
            case R.id.plus:

                if (tktTypSize > 0) {
                    counter++;
                    showText = true;
                    totalcost_txt = true;
                }
                break;

            case R.id.minus:
                if (tktTypSize > 0) {
                    if (counter > 0)
                        counter--;
                    showText = true;
                    totalcost_txt = true;
                }
                break;

            case R.id.pay:


                if (validation() == true) {
                    tkt_id = array_tkt_id[position_Spinner];
                    tkt_tye_id = array_tktTyp_id[position_Spinner];

                    Tickets tickets = new Tickets();
                    tickets.setUtkt_lgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));
                    tickets.setUtkt_tkt_id(tkt_id);
                    tickets.setUtkt_qty(String.valueOf(counter));
                    tickets.setTicket_type_id(tkt_tye_id);

                    HTTPWebRequest.PurchaseTkt(PurchaseActivity.this, tickets, AppConstants.APICode.tkt_purchase, PurchaseActivity.this);

                    //  if (counter > 0)

                    // else
                    //     Toast.makeText(PurchaseActivity.this, "Please Select number of ticket", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.back:
                finish();
                break;
        }
        if (showText)
            if (counter >= 0)
                num_tkt.setText(String.valueOf(counter));

        if (totalcost_txt)

            rupee = array_rupes[position_Spinner];
            if (!rupee.equals("")) {
                totl_cost.setText("$" + String.valueOf(counter * (Integer.parseInt(array_rupes[position_Spinner]))));
            }


         /*   if (position_Spinner == 0)
                totl_cost.setText("$" + String.valueOf(counter * (Integer.parseInt(array_rupes[0]))));
            else if (position_Spinner == 1)
                totl_cost.setText("$" + String.valueOf(counter * (Integer.parseInt(array_rupes[1]))));
            else if (position_Spinner == 2)
                totl_cost.setText("$" + String.valueOf(counter * (Integer.parseInt(array_rupes[2]))));
            else if (position_Spinner == 3)
                totl_cost.setText("$" + String.valueOf(counter * (Integer.parseInt(array_rupes[3]))));*/
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {

                    case AppConstants.APICode.show_details:

                        ShowDetailsResponse showDetailsResponse = new ShowDetailsResponse();
                        showDetailsResponse = gson.fromJson(response, ShowDetailsResponse.class);

                        if (showDetailsResponse.getSTATUS().equals("SUCCESS")) {

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Show.class, new ShowDetailsResponse())
                                    .create();
                            Show show = gson.fromJson(response, Show.class);
                            if (show.getShow_title() != null) {
                                show_title.setText(show.getShow_title());
                            }
                            if (show.getVen_name() != null) {
                                show_venue.setText(show.getVen_name());
                            }
                            if (show.getShow_date() != null) {

                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                                DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
                                String inputDateStr = show.getShow_date();
                                Date date = null;
                                try {
                                    date = inputFormat.parse(inputDateStr);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String outputDateStr = outputFormat.format(date);

                                show_date.setText(outputDateStr);
                            }

                            ArrayList<Tickets> ticket = show.getTickets();

                            ArrayList<String> tktTyp = new ArrayList<String>();


                            String[] values = new String[ticket.size()];
                            array_rupes = new String[ticket.size()];
                            array_tkt_id = new String[ticket.size()];
                            array_tktTyp_id = new String[ticket.size()];

                            for (int i = 0; i < ticket.size(); i++) {

                                values[i] = ticket.get(i).getTicket_type_label();

                                array_rupes[i] = ticket.get(i).getTicket_price();
                                array_tkt_id[i] = ticket.get(i).getTicket_id();
                                array_tktTyp_id[i] = ticket.get(i).getTicket_type_id();
                            }

                            tktTypSize = ticket.size();

                            Log.e("tkt type size", String.valueOf(tktTypSize));


                            //Toast.makeText(PurchaseActivity.this,"purchase",Toast.LENGTH_LONG).show();

                            ArrayAdapter<String> tkytpe_adapter = new ArrayAdapter<String>(this,
                                    R.layout.spinner_item, values);


                            tkytpe_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                            //tkytpe_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            tkt_typ_spinner.setAdapter(tkytpe_adapter);
                            tkt_typ_spinner.setSelection(0);
                            tkt_typ_spinner.setOnItemSelectedListener(this);


                        } else {

                            Toast.makeText(PurchaseActivity.this, showDetailsResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }

                        break;
                    case AppConstants.APICode.tkt_purchase:

                        OrderResponse orderResponse = new OrderResponse();
                        orderResponse = gson.fromJson(response, OrderResponse.class);

                        if (orderResponse.getSTATUS().equals("SUCCESS")) {
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(OrderRequest.class, new OrderResponse())
                                    .create();
                            OrderRequest orderRequest = gson.fromJson(response, OrderRequest.class);


                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.SetGetBundleData.show_title, show_title.getText().toString());
                            bundle.putString(AppConstants.SetGetBundleData.show_place, show_venue.getText().toString());
                            bundle.putString(AppConstants.SetGetBundleData.show_date, show_date.getText().toString());
                            bundle.putString(AppConstants.SetGetBundleData.total_cost, orderRequest.getPrice());
                            bundle.putString(AppConstants.SetGetBundleData.OrderId, orderRequest.getOrderId());

                            Intent intent = new Intent(PurchaseActivity.this, PaymentActivity.class);
                            intent.putExtras(bundle);
                            startActivity(intent);

                        } else {

                            Toast.makeText(PurchaseActivity.this, orderResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        counter = 0;
        num_tkt.setText("0");
        totl_cost.setText("$" + "0");

        tkt_typ_spinner.setSelection(position);

        this.position_Spinner = position;

        String selState = (String) tkt_typ_spinner.getSelectedItem();
        //Toast.makeText(PurchaseActivity.this,selState,Toast.LENGTH_LONG).show();
    }


    private boolean validation() {
        boolean value = true;

        if (!(tktTypSize > 0)) {
            Toast.makeText(PurchaseActivity.this, "No ticket type", Toast.LENGTH_LONG).show();
            value = false;
        } else if (!(counter > 0)) {

            Toast.makeText(PurchaseActivity.this, "Please Select number of ticket", Toast.LENGTH_LONG).show();
            value = false;
        } else {
            value = true;
        }
        return value;
    }


    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
