package com.gigable.io.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.NewPasswordRequest;
import com.gigable.io.Models.ServiceResponse.LoginResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener, ApiResponse, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    EditText login_pwd, new_pwd, pwd_new_confirm;
    TextView submit;
    String id = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

       // overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);

        setContentView(R.layout.activity_change_pwd);

        //this is for logout
     /*   IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/


        init();

        // SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        String id1 = SaveSharedPreference.getUserID(ChangePasswordActivity.this);

        if (id1 != null) {
            id = id1;
        }

    }

    //initialization of elements
    private void init() {

        login_pwd = (EditText) findViewById(R.id.pwd_login);
        new_pwd = (EditText) findViewById(R.id.pwd_new);
        pwd_new_confirm = (EditText) findViewById(R.id.pwd_new_confirm);

        submit = (TextView) findViewById(R.id.submit_new);

        submit.setOnClickListener(this);

    }


    //click event of elements
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.submit_new:

                validation();

                if (validation() == true) {

                    if (id != null) {
                        NewPasswordRequest newPasswordRequest = new NewPasswordRequest();
                        newPasswordRequest.setLgn_id(id);
                        newPasswordRequest.setLgn_password(login_pwd.getText().toString());
                        newPasswordRequest.setNew_password(new_pwd.getText().toString());
                        HTTPWebRequest.ChangePassword(ChangePasswordActivity.this, newPasswordRequest, AppConstants.APICode.change_pwd, ChangePasswordActivity.this);
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "You can not change Password", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Bundle bundle = new Bundle();
        Utility.RedirectToActivity(ChangePasswordActivity.this, HomeScreenActivity.class, false, bundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set title
        //setTitle(listArray[position]);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
       // if (hasFocus)
           // overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
    }

    @Override
    protected void onPause() {
       // overridePendingTransition(R.anim.hold, R.anim.push_out_to_left);
        super.onPause();
    }


    //here manage api responce
    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {

                    case AppConstants.APICode.change_pwd:
                        LoginResponse loginResponse = new LoginResponse();
                        loginResponse = gson.fromJson(response, LoginResponse.class);

                        if (loginResponse.getSTATUS().equals("SUCCESS")) {
                            Toast.makeText(ChangePasswordActivity.this, loginResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(ChangePasswordActivity.this, HomeScreenActivity.class);
                            startActivity(i);
                            position = 1;
                        } else {
                            Toast.makeText(ChangePasswordActivity.this, loginResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            } catch (Exception e) {

                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    //validation of form
    private boolean validation() {

        boolean value = true;

        if (!(login_pwd.getText().toString().length() >= 3)) {
            login_pwd.setError("minimum three digit required");
            value = false;
        } else if (!(new_pwd.getText().toString().length() >= 3)) {
            new_pwd.setError("minimum three digit required");
            value = false;
        } else if (!(pwd_new_confirm.getText().toString().equals(new_pwd.getText().toString()))) {
            pwd_new_confirm.setError("Not match the password");
            value = false;
        } else {
            value = true;
        }
        return value;
    }

    @Override
    public void networkError(int apiCode) {
    }

    @Override
    public void responseError(String responseError, int apiCode) {
    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
