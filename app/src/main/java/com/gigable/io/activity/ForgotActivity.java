package com.gigable.io.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.DataRequest;
import com.gigable.io.Models.ServiceRequest.LoginRequest;
import com.gigable.io.Models.ServiceResponse.ForgotPasswordResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class ForgotActivity extends Activity implements View.OnClickListener, ApiResponse{
    EditText email;
    TextView submit, login, title, subtitle;
    String id;
    boolean internetConnection = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_forgot);

      /*  IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/

        init();
    }


    //initialization of elements
    private void init() {
        email = (EditText) findViewById(R.id.email_forgot);
        submit = (TextView) findViewById(R.id.submit_forgot);
        //login = (TextView) findViewById(R.id.login);

        title = (TextView) findViewById(R.id.title);

        submit.setOnClickListener(this);
        //login.setOnClickListener(this);


        //disable actionbar items(title,icon)
        //getActionBar().setDisplayShowHomeEnabled(false);
       // getActionBar().setDisplayShowTitleEnabled(false);

    }

    //click event of elements
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.submit_forgot:
                validation();
                if (validation() == true) {
                        LoginRequest loginRequest = new LoginRequest();
                        loginRequest.setLgn_email(email.getText().toString());
                        HTTPWebRequest.ForgotPassword(ForgotActivity.this, loginRequest, AppConstants.APICode.forgot_pwd, ForgotActivity.this);
                    }

                break;
            case R.id.login:

                Intent i = new Intent(ForgotActivity.this, LoginActivity.class);
                startActivity(i);

                break;
        }
    }


    //api responce is get here
    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.forgot_pwd:
                        ForgotPasswordResponse forgotPasswordResponse = new ForgotPasswordResponse();
                        forgotPasswordResponse = gson.fromJson(response, ForgotPasswordResponse.class);

                        if (forgotPasswordResponse.getSTATUS().equals("SUCCESS")) {
                            Toast.makeText(ForgotActivity.this, forgotPasswordResponse.getMESSAGES(), Toast.LENGTH_LONG).show();

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(DataRequest.class, new ForgotPasswordResponse())
                                    .create();
                            DataRequest dataRequest = gson.fromJson(response, DataRequest.class);

                            String id = dataRequest.getLgn_id();

                            Intent intent = new Intent(ForgotActivity.this, ForgotVerificationActivity.class);
                            intent.putExtra("id", id);
                            startActivity(intent);
                        } else {
                            Toast.makeText(ForgotActivity.this, forgotPasswordResponse.getMESSAGES(), Toast.LENGTH_LONG).show();

                        }
                        break;
                }
            } catch (Exception e) {

                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    //validation of form
    private boolean validation() {

        boolean value = true;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-z]+";
        if (!(email.getText().toString().matches(emailPattern) && email.getText().toString().length() > 0)) {
            email.setError("email invalid");
            value = false;
        } else {
            value = true;
        }
        return value;
    }




    @Override
    public void networkError(int apiCode) {
    }

    @Override
    public void responseError(String responseError, int apiCode) {
    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {
    }


}
