package com.gigable.io.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.Models.ServiceResponse.AddDelFavTrkResponse;
import com.gigable.io.Models.ServiceResponse.ShowResponse;
import com.gigable.io.Models.ServiceResponse.TrackResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Common;
import com.gigable.io.Utility.PlayerConstants;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.TimeUtilities;
import com.gigable.io.Utility.Utility;
import com.gigable.io.adapters.PlayListAllAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.circularprogressview.android.CircularProgressView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class PlaylistActivity extends AppCompatActivity implements ApiResponse, View.OnClickListener, SeekBar.OnSeekBarChangeListener, PlayListAllAdapter.ClickListener {

    static TextView play;
    static TextView paush;
    private TextView play_title;
    private TextView total_show;
    private TextView track_title;
    private TextView art_name;
    private TextView total_track;
    private static ImageView btnPlay;
    private ImageView btnNext, btnSuffer;
    private ImageView back;
    private ImageView tkt;
    private ImageView favrit;
    private ImageView share_app;
    public static SeekBar songProgressBar;
    public static TextView songCurrentDurationLabel;
    public static TextView songTotalDurationLabel;
    private RecyclerView rv;
    LinearLayout playLayout;

    String play_id_intent = "";
    String latitude = "";
    String longitude = "";
    String lgn_id = "";

    Intent intent;
    Context mContext;
    LinearLayout player1;

    boolean serviceBound = false;

    private Toolbar mToolbar;
    PlayListAllAdapter adapter;
    private TimeUtilities utils;

    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
    ArrayList<Track> trackArrayList = new ArrayList<>();
    ArrayList<Track> originalTrackArrayList = new ArrayList<>();
    ArrayList<Track> shuffelTrackArrayList = new ArrayList<>();
    ArrayList<Show> showArrayList = new ArrayList<>();

    public static final String Broadcast_PLAY_NEW_AUDIO = "com.gigable.io.PlayNewAudio";
    public static Activity fa;


    public static final String UPDATE_UI_BROADCAST = "com.gigable.io.NEW_SONG_UPDATE_UI";
    public static final String UPDATE_UI_SEEKBAR = "com.gigable.io.NEW_SONG_UPDATE_SEEKBAR";

    int currant_index = 0;
    static boolean click = false;
    private ProgressBar mStreamingProgressBar;

    String track_id = "";
    String track_url = "";

    private static Common mApp;
    private boolean mIsCreating = true;

    static Handler progressBarHandler = new Handler();

    private CircularProgressView mProgressDialog;


    private ProgressBar progressBar;

    ProgressDialog progressdialog;

    private LinearLayout linMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_playlist);

        mApp = (Common) getApplicationContext();

        fa = this;

        mContext = this;
        AppConstants.CONTEXT = mContext;

        click = false;
        mIsCreating = true;


        mStreamingProgressBar = (ProgressBar) findViewById(R.id.startingStreamProgressBar);
        mStreamingProgressBar.setVisibility(View.GONE);

        mProgressDialog = (CircularProgressView) findViewById(R.id.progress_view);
        mProgressDialog.startAnimation();


        //initialization of elementas
        init();

        //get track list
        getTrack();

    }

    private void getTrack() {

        PlayListRequest playListRequest = new PlayListRequest();
        playListRequest.setLgn_id(lgn_id);
        playListRequest.setDistance_type("2");
        playListRequest.setLatitude(Pref.getValue(AppConstants.PreftDataKey.latitude, ""));
        playListRequest.setLongitude(Pref.getValue(AppConstants.PreftDataKey.longitude, ""));
        playListRequest.setMonth_range("12");
        playListRequest.setPlaylist_distance("1000");
        playListRequest.setUtp_play_id(play_id_intent);

        HTTPWebRequest.Gettrack(PlaylistActivity.this, playListRequest, AppConstants.APICode.tracklist, PlaylistActivity.this);
    }


    private void init() {

        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);

        progressdialog = new ProgressDialog(this);
        progressdialog.setMessage("Please Wait....");
        progressdialog.setCanceledOnTouchOutside(false);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        linMain = (LinearLayout) findViewById(R.id.linMain);

        player1 = (LinearLayout) findViewById(R.id.player1);
        play = (TextView) findViewById(R.id.play);
        paush = (TextView) findViewById(R.id.paush);
        track_title = (TextView) findViewById(R.id.track_title);
        art_name = (TextView) findViewById(R.id.art_name);
        btnPlay = (ImageView) findViewById(R.id.btnPlay);
        btnNext = (ImageView) findViewById(R.id.btnNext);
        btnSuffer = (ImageView) findViewById(R.id.btnSuffer);
        favrit = (ImageView) findViewById(R.id.favrit);
        tkt = (ImageView) findViewById(R.id.tkt);
        back = (ImageView) findViewById(R.id.back);
        share_app = (ImageView) findViewById(R.id.share_app);
        playLayout = findViewById(R.id.play_layout);

        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);


        utils = new TimeUtilities();


        //toolbar elements
        play_title = (TextView) findViewById(R.id.play_title);
        total_show = (TextView) findViewById(R.id.total_show);
        total_track = (TextView) findViewById(R.id.total_track);
        latitude = SaveSharedPreference.getLatitude(this);
        longitude = SaveSharedPreference.getLongitude(this);


        //Get value from intent
        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            play_id_intent = bundle.getString(AppConstants.SetGetBundleData.play_id, null);
        }
        lgn_id = Pref.getValue(AppConstants.PreftDataKey.lgn_id, "");


        //Onclick events of elements
        songProgressBar.setOnSeekBarChangeListener(this);
        play.setOnClickListener(this);
        paush.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        back.setOnClickListener(this);
        tkt.setOnClickListener(this);
        favrit.setOnClickListener(this);
        share_app.setOnClickListener(this);
        btnSuffer.setOnClickListener(this);
    }


    @Override
    public void ItemClicked(View v, int position) {
        switch (v.getId()) {
            case R.id.liner_item:
                playAudioItemclick(position);
                showPlayerUI();
                adapter.changeItem(position);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.play:
                playAudio(currant_index);
                adapter.changeItem(currant_index);
                showPlayerUI();

                break;
            case R.id.btnSuffer:
                if (btnSuffer.getTag().toString().equalsIgnoreCase(getResources().getString(R.string.simple))) {
                    btnSuffer.setTag(getResources().getString(R.string.shuffel));
                    Collections.shuffle(shuffelTrackArrayList);
                    adapter.shuffleList(shuffelTrackArrayList);
                    Toast.makeText(mContext, "shuffle", Toast.LENGTH_SHORT).show();
                } else {
                    btnSuffer.setTag(getResources().getString(R.string.simple));
                    adapter.shuffleList(trackArrayList);
                    Toast.makeText(mContext, " not shuffle", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.paush:
                progressBarHandler.removeCallbacks(mUpdateTimeTask);
                Controls.pauseControl(getApplicationContext());
                break;
            case R.id.btnPlay:

                if (mApp.getService().mediaPlayer.isPlaying()) {
                    progressBarHandler.removeCallbacks(mUpdateTimeTask);
                    Controls.pauseControl(getApplicationContext());
                } else {
                    Controls.playControl(getApplicationContext());
                }

                break;
            case R.id.btnNext:

                callNextSong();

                break;
            case R.id.back:

                if (mApp.ismIsServiceRunning()) {
                    mApp.getService().progressBarHandler.removeCallbacks(mUpdateTimeTask);
                }
                Bundle bundle2 = new Bundle();
                Utility.RedirectToActivity(PlaylistActivity.this, HomeScreenActivity.class, false, bundle2);

                break;

            case R.id.tkt:
                Bundle bundle1 = new Bundle();
                bundle1.putString(AppConstants.SetGetBundleData.trk_id, track_id);
                Utility.RedirectToActivity(PlaylistActivity.this, ConcertsList.class, false, bundle1);
                break;

            case R.id.favrit:
                Track track = new Track();
                track.setUtp_trk_id(track_id);
                track.setUsrplay_lgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));
                HTTPWebRequest.AddTrackFav(PlaylistActivity.this, track, AppConstants.APICode.track_fav, PlaylistActivity.this);
                break;

            case R.id.share_app:

                Log.e("track url", track_url);

                String songurl = "https://www.gigable.net/free-music-streaming-app/streamsong?";
                songurl = songurl.concat(track_url);
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
                i.putExtra(Intent.EXTRA_TEXT, songurl);
                startActivity(Intent.createChooser(i, "Share URL"));
                break;
        }
    }


    public static void changeButton() {
        if (PlayerConstants.SONG_PAUSED) {

            Log.e("song pause", "true");

            if (click == false) {
                btnPlay.setImageResource(R.mipmap.paly_2x);
            } else {
                paush.setVisibility(View.GONE);
                play.setVisibility(View.VISIBLE);
                btnPlay.setImageResource(R.mipmap.paly_2x);
            }

        } else {
            Log.e("song pause", "false");

            if (click == false) {
                btnPlay.setImageResource(R.mipmap.pause_2x);
            } else {
                paush.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);
                btnPlay.setImageResource(R.mipmap.pause_2x);
            }
        }
    }

    private void changePlayerUI(String index) {

        if (trackArrayList.size() > 0) {
            songProgressBar.setVisibility(View.VISIBLE);
            mStreamingProgressBar.setVisibility(View.INVISIBLE);

            track_title.setText(trackArrayList.get(Integer.parseInt(index)).getTrk_title());
            art_name.setText(trackArrayList.get(Integer.parseInt(index)).getArt_name());
            track_id = trackArrayList.get(Integer.parseInt(index)).getTrk_id();
            track_url = trackArrayList.get(Integer.parseInt(index)).getTrk_audio();

            if (trackArrayList.get(Integer.parseInt(index)).isFav_status() == false) {
                favrit.setImageResource(R.mipmap.fav);
            } else {
                favrit.setImageResource(R.mipmap.fav_fill);
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putBoolean("serviceStatus", serviceBound);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("serviceStatus");
    }


    private void playAudioItemclick(int audioIndex) {
        if (click == false) {
            if (mApp.ismIsServiceRunning() == true) {
                playSongServiceRunning(audioIndex);
            } else {
                playSongStartService(audioIndex);
            }
        } else {
            callClickSong(audioIndex);
        }
    }

    private void playAudio(int audioIndex) {
        if (click == false) {
            if (mApp.ismIsServiceRunning() == true) {
                playSongServiceRunning(audioIndex);
            } else {
                playSongStartService(audioIndex);
            }
        } else {
            Controls.playControl(getApplicationContext());
        }
    }

    private void showPlayerUI() {

        int padding_in_dp = 175;
        final float scale = getResources().getDisplayMetrics().density;
        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);


        Pref.setValue(AppConstants.PreftDataKey.songCalledFrom, "PlaylistPage");
        PlayerConstants.SONG_PAUSED = false;
        player1.setVisibility(View.VISIBLE);
        rv.setPadding(0, 0, 0, padding_in_px);
        changeButton();
    }

    private void playSongStartService(int audioIndex) {
        mProgressDialog.setVisibility(View.VISIBLE);
        Common.setSongPlay(false);
        new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();
        StorageUtil storage = new StorageUtil(getApplicationContext());
        storage.storeAudio(trackArrayList);
        storage.storeAudioIndex(audioIndex);

        setPlayerValue(storage);

        Intent playerIntent = new Intent(this, MediaPlayerService.class);
        startService(playerIntent);

        mApp.startService();
        click = true;
    }

    private void playSongServiceRunning(int audioIndex) {
        progressBarHandler.removeCallbacks(mUpdateTimeTask);
        songProgressBar.setProgress(0);

        btnPlay.setVisibility(View.GONE);

        mProgressDialog.setVisibility(View.VISIBLE);
        Common.setSongPlay(false);
        mApp.getService().mediaPlayer.stop();

        new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();

        StorageUtil storage = new StorageUtil(getApplicationContext());
        storage.storeAudio(trackArrayList);
        storage.storeAudioIndex(audioIndex);

        //set media player value like title,art name
        setPlayerValue(storage);

        Controls.nextPlaylistControl(getApplicationContext());

        click = true;
    }


    private void callNextSong() {

        btnPlay.setVisibility(View.GONE);
        mProgressDialog.setVisibility(View.VISIBLE);
        Common.setSongPlay(false);

        songProgressBar.setProgress(0);
        progressBarHandler.removeCallbacks(mUpdateTimeTask);
        songCurrentDurationLabel.setText("0.00");
        songTotalDurationLabel.setText("0.00");

        Controls.nextControl(getApplicationContext());
    }

    private void callClickSong(int audioIndex) {
        btnPlay.setVisibility(View.GONE);
        mProgressDialog.setVisibility(View.VISIBLE);
        Common.setSongPlay(false);

        songProgressBar.setProgress(0);
        progressBarHandler.removeCallbacks(mUpdateTimeTask);
        songCurrentDurationLabel.setText("0.00");
        songTotalDurationLabel.setText("0.00");

        StorageUtil storage = new StorageUtil(getApplicationContext());
        storage.storeAudioIndex(audioIndex);

        Controls.songClickControl(getApplicationContext());
    }


    private void setPlayerValue(StorageUtil storage) {

        track_title.setText(storage.loadAudio().get(storage.loadAudioIndex()).getTrk_title());
        art_name.setText(storage.loadAudio().get(storage.loadAudioIndex()).getArt_name());
        track_id = storage.loadAudio().get(storage.loadAudioIndex()).getTrk_id();
        track_url = storage.loadAudio().get(storage.loadAudioIndex()).getTrk_audio();

        if (storage.loadAudio().get(storage.loadAudioIndex()).isFav_status() == false) {
            favrit.setImageResource(R.mipmap.fav);
        } else {
            favrit.setImageResource(R.mipmap.fav_fill);
        }
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.tracklist:

                        TrackResponse trackResponse = gson.fromJson(response, TrackResponse.class);

                        ArrayList<Track> results = trackResponse.DATA.Track;
                        ArrayList<Show> results1 = trackResponse.DATA.Show;
                        originalTrackArrayList=results;
                        shuffelTrackArrayList=results;
                        trackArrayList = results;
                        showArrayList = results1;

                        System.out.println("track size" + results.size());

                        total_show.setText("" + results1.size() + " Concerts");
                        total_track.setText("" + results.size() + " Tracks");
                        play_title.setText(results.get(0).getPlay_name());

                        if (play_id_intent.equals("fav_playlist")) {
                            linMain.setVisibility(View.GONE);
                        }

                        for (int i = 0; i <= results.size(); i++) {

                            Track track = results.get(i);

                            HashMap<String, String> song = new HashMap<String, String>();
                            song.put("songTitle", track.getTrk_title());
                            song.put("songPath", track.getTrk_audio());

                            songsList.add(song);

                            if (i == results.size() - 1) break;
                        }
                        if (results.size() > 0) {
                            playLayout.setVisibility(View.VISIBLE);
                        } else {
                            playLayout.setVisibility(View.GONE);
                        }
                        setupAdapter(results, results1, play_id_intent);

                        break;

                    case AppConstants.APICode.track_fav:

                        AddDelFavTrkResponse addDelFavTrkResponse = new AddDelFavTrkResponse();
                        addDelFavTrkResponse = gson.fromJson(response, AddDelFavTrkResponse.class);

                        if (addDelFavTrkResponse.getSTATUS().equals("SUCCESS")) {
                            Toast.makeText(PlaylistActivity.this, addDelFavTrkResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Track.class, new AddDelFavTrkResponse())
                                    .create();

                            Track track = gson.fromJson(response, Track.class);
                            if (track.getFp_status().equals("2")) {
                                favrit.setImageResource(R.mipmap.fav);
                            } else {
                                favrit.setImageResource(R.mipmap.fav_fill);
                            }
                            Pref.setValue("Refresh", "0");
                        }
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    private void setupAdapter(final ArrayList<Track> mData, ArrayList<Show> mData1, String play_id_intent) {

        if (mData.size() > 0) {
            adapter = new PlayListAllAdapter(this, this, mData, play_id_intent);
            rv.setAdapter(adapter);
            adapter.setClickListener(this);
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        // Pref.setValue(AppConstants.PreftDataKey.backgroundApp, "false");

        if (mIsCreating == false) {

            progressBarHandler.postDelayed(mUpdateTimeTask, 100);
            mIsCreating = false;

            Log.e("onResume task call", "true");

        }

        //Update the seekbar.
        if (Common.isSongPlay() == true) {
            try {
                Log.e("set seekbar call", "true");
                setSeekbarDuration(mApp.getService().getMediaPlayer().getDuration() / 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private void setSeekbarDuration(int duration) {
        try {
            progressBarHandler.postDelayed(mUpdateTimeTask, 100);
        } catch (Exception e) {
        }
    }


    static Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = 0;
            long currentDuration = 0;


            //Log.e("call update time", "playlist page");

            try {
                totalDuration = mApp.getService().getMediaPlayer().getDuration();
                currentDuration = mApp.getService().getMediaPlayer().getCurrentPosition();


                songCurrentDurationLabel.setText(Utility.milliSecondsToTimer(currentDuration));

                songTotalDurationLabel.setText(Utility.milliSecondsToTimer(totalDuration));


                int progress = (int) (Utility.getProgressPercentage(currentDuration, totalDuration));


                songProgressBar.setProgress(progress);

                progressBarHandler.postDelayed(this, 100);

            } catch (
                    Exception e
                    )

            {
                e.printStackTrace();
            }
        }
    };

    public void onStart() {
        super.onStart();

        //Initialize the broadcast manager that will listen for track changes.
        LocalBroadcastManager.getInstance(mContext)
                .registerReceiver((mReceiverSeekbar),
                        new IntentFilter(Common.UPDATE_UI_BROADCAST1));


        if (mApp.ismIsServiceRunning()) {


            int padding_in_dp = 175;
            final float scale = getResources().getDisplayMetrics().density;
            int padding_in_px = (int) (padding_in_dp * scale + 0.5f);


            player1.setVisibility(View.VISIBLE);
            //btnPlay.setVisibility(View.VISIBLE);
            rv.setPadding(0, 0, 0, padding_in_px);

            if (Common.isSongPlay() == true) {
                btnPlay.setVisibility(View.VISIBLE);
            } else {
                mProgressDialog.setVisibility(View.VISIBLE);
            }


            String[] updateFlags = new String[]{
                    // Common.UPDATE_SEEKBAR_DURATION,
                    Common.UPDATE_TRACK_TITLE,
                    Common.UPDATE_ARTIST_NAME,
                    Common.UPDATE_TRACK_ID,
                    Common.UPDATE_FAVOURITE,
                    Common.UPDATE_TRACKURL
            };

            String[] flagValues = new String[]{"" +
                    mApp.getService().audioList.get(mApp.getService().audioIndex).getTrk_title(),
                    mApp.getService().audioList.get(mApp.getService().audioIndex).getArt_name(),
                    mApp.getService().audioList.get(mApp.getService().audioIndex).getTrk_id(),
                    mApp.getService().audioList.get(mApp.getService().audioIndex).getFavourite_status(),
                    mApp.getService().audioList.get(mApp.getService().audioIndex).getTrk_audio(),
            };

            mApp.broadcastUpdateUICommand(updateFlags, flagValues);

            changeButton();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // if (serviceBound) {
        //service is active
        if (mApp.ismIsServiceRunning()) {
            progressBarHandler.removeCallbacks(mUpdateTimeTask);
            LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReceiverSeekbar);
        }
    }
    BroadcastReceiver mReceiverSeekbar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //Grab the bundle from the intent.
            Bundle bundle = intent.getExtras();


            if (intent.hasExtra(Common.UPDATE_TRACK_TITLE)) {
                track_title.setText(bundle.getString(Common.UPDATE_TRACK_TITLE));
            }
            if (intent.hasExtra(Common.UPDATE_ARTIST_NAME)) {
                art_name.setText(bundle.getString(Common.UPDATE_ARTIST_NAME));
            }
            if (intent.hasExtra(Common.UPDATE_TRACK_ID)) {
                track_id = String.valueOf(bundle.get(Common.UPDATE_TRACK_ID));
            }
            if (intent.hasExtra(Common.UPDATE_TRACKURL)) {
                track_url = String.valueOf(bundle.get(Common.UPDATE_TRACKURL));
            }
            if (intent.hasExtra(Common.UPDATE_FAVOURITE)) {
                if (String.valueOf(bundle.get(Common.UPDATE_TRACK_ID)).equals("false")) {
                    favrit.setImageResource(R.mipmap.fav);
                } else {
                    favrit.setImageResource(R.mipmap.fav_fill);
                }
            }

            if (intent.hasExtra(Common.UPDATE_HIDE_PROGRESS_DIALOG)) {

                mProgressDialog.setVisibility(View.GONE);
                //  progressdialog.hide();
                //progressBar.setVisibility(View.GONE);

                Log.e("get call of song play", "playlist true");

                btnPlay.setVisibility(View.VISIBLE);

                //set song is playing now
                Common.setSongPlay(true);

            }


            if (intent.hasExtra(Common.UPDATE_PLAYERUI_NEXT)) {

                Log.e("call next", "call player next");

                changePlayerUI(String.valueOf(bundle.get(Common.UPDATE_PLAYERUI_NEXT)));
            }


            if (intent.hasExtra(Common.UPDATE_SONG_COMPLETE)) {
                callNextSong();
            }


            //Updates the duration of the SeekBar.
            if (intent.hasExtra(Common.UPDATE_SEEKBAR_DURATION))
                setSeekbarDuration(Integer.parseInt(
                        bundle.getString(
                                Common.UPDATE_SEEKBAR_DURATION)));


        }
    };

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Bundle bundle = new Bundle();
        Utility.RedirectToActivity(PlaylistActivity.this, HomeScreenActivity.class, false, bundle);
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        progressBarHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        progressBarHandler.removeCallbacks(mUpdateTimeTask);

        if (Common.isSongPlay() == true) {

            int totalDuration = mApp.getService().mediaPlayer.getDuration();

            Log.e("totalduration", String.valueOf(totalDuration));

            int currentPosition = Utility.progressToTimer(seekBar.getProgress(), totalDuration);

            Log.e("totalduration", String.valueOf(currentPosition));

            mApp.getService().mediaPlayer.seekTo(currentPosition);
            setSeekbarDuration(mApp.getService().getMediaPlayer().getDuration() / 1000);
        }
    }
}
