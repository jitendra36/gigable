package com.gigable.io.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.Models.ServiceResponse.PlaylistResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.adapters.FeatureArtistHomeAdapter;
import com.gigable.io.adapters.LocalPlaylistHomeAdapter;
import com.gigable.io.adapters.PlaylistHomeAdapter;
import com.gigable.io.app.Config;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import io.fabric.sdk.android.Fabric;


public class HomeScreenActivity extends BaseActivity implements ApiResponse {

    GridView gridView_locatl;
    GridView gridView_playlist;
    GridView gridView_feature;
    Context mContext;
    TextView total_track;
    TextView total_show;
    TextView artist_name;
    TextView upartist_name;
    TextView upconrt_total;
    TextView upconrt_total_trck;
    TextView chng_location;
    TextView location;
    ImageView img;

    String lgn_id = "";
    String totaltrack_local = "";
    String totalshow_local = "";
    String totalshow_Upcoming = "";


    PlaylistResponse playlistResponse;
    public ArrayList<PlayListRequest> playlistArray = new ArrayList<>();
    public ArrayList<Track> featureArray = new ArrayList<>();

    PlaylistHomeAdapter adapter;
    FeatureArtistHomeAdapter featureArtistHomeAdapter;
    LocalPlaylistHomeAdapter localPlaylistHomeAdapter;

    private ArrayList<HashMap<String, String>> artistlist = new ArrayList<HashMap<String, String>>();

    private static final String TAG1 = HomeScreenActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_home_screen);

        mContext = this;
        AppConstants.CONTEXT = mContext;

        init();
        getPlayList();
        displayFirebaseRegId();
    }


    private void displayFirebaseRegId() {
        String regId = Pref.getValue(AppConstants.PreftDataKey.PREF_GCM_REGISTRATION_ID, "");
        Log.e(TAG1, "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId)) {

        }
        else
            Toast.makeText(this, "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
    }


    private void init() {

        mContext = this;
        AppConstants.CONTEXT = mContext;
        lgn_id = Pref.getValue(AppConstants.PreftDataKey.lgn_id, "");
        artist_name = (TextView) findViewById(R.id.artist_name);
        total_track = (TextView) findViewById(R.id.total_track);
        total_show = (TextView) findViewById(R.id.total_show);
        upartist_name = (TextView) findViewById(R.id.upartist_name);
        upconrt_total = (TextView) findViewById(R.id.upconrt_total);
        chng_location = (TextView) findViewById(R.id.chng_location);
        upconrt_total_trck = (TextView) findViewById(R.id.upconrt_total_trck);
        location = (TextView) findViewById(R.id.location);
        img = (ImageView) findViewById(R.id.img);
        gridView_locatl = (GridView) findViewById(R.id.gridView);
        gridView_playlist = (GridView) findViewById(R.id.gridView2);
        gridView_feature = (GridView) findViewById(R.id.gridView1);


        gridView_feature.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridView_playlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Pref.setValue(AppConstants.PreftDataKey.play_name, playlistArray.get(position + 1).getPlay_name().toString() + "");

                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.play_id, playlistArray.get(position + 1).getPlay_id().toString());
                bundle.putString(AppConstants.SetGetBundleData.play_name, playlistArray.get(position + 1).getPlay_name().toString());
                bundle.putString(AppConstants.SetGetBundleData.totaltrack, playlistArray.get(position + 1).getTotaltrack().toString());
                bundle.putString(AppConstants.SetGetBundleData.totalshow, playlistArray.get(position + 1).getTotalshow().toString());

                Utility.RedirectToActivity(HomeScreenActivity.this, PlaylistActivity.class, false, bundle);
            }
        });

        gridView_locatl.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Pref.setValue(AppConstants.PreftDataKey.play_name, "On-Tour Playlist" + "");

                Log.e("PlayName", playlistArray.get(0).getPlay_name().toString() + "");

                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.play_id, playlistArray.get(0).getPlay_id().toString());
                bundle.putString(AppConstants.SetGetBundleData.play_name, playlistArray.get(0).getPlay_name().toString());
                bundle.putString(AppConstants.SetGetBundleData.totaltrack, playlistArray.get(0).getTotaltrack().toString());
                bundle.putString(AppConstants.SetGetBundleData.totalshow, playlistArray.get(0).getTotalshow().toString());
                Utility.RedirectToActivity(HomeScreenActivity.this, PlaylistActivity.class, false, bundle);
            }
        });

        gridView_feature.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.art_id, featureArray.get(position).getArt_id().toString());
                bundle.putString(AppConstants.SetGetBundleData.art_name, featureArray.get(position).getArt_name().toString());
                bundle.putString(AppConstants.SetGetBundleData.art_img, featureArray.get(position).getArt_image().toString());
                Utility.RedirectToActivity(HomeScreenActivity.this, ArtistDetailsActivity.class, false, bundle);
            }
        });


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.totalshow, totalshow_Upcoming);
                Utility.RedirectToActivity(HomeScreenActivity.this, UpcomingConcerts.class, false, bundle);
            }
        });

        chng_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.from_loc, "1");
                bundle.putString(AppConstants.SetGetBundleData.totalshow, "");
                Utility.RedirectToActivity(HomeScreenActivity.this, ChangeLocationActivity.class, false, bundle);
            }
        });
    }


    private void getPlayList() {

        String latitude;
        String longitude;

        if (Pref.getValue(AppConstants.PreftDataKey.latitude, "").equals("")){
            latitude = "0";
        }else {
            latitude = Pref.getValue(AppConstants.PreftDataKey.latitude, "");
        }

        if (Pref.getValue(AppConstants.PreftDataKey.longitude, "").equals("")){
            longitude = "0";
         }else {
            longitude = Pref.getValue(AppConstants.PreftDataKey.longitude, "");
        }


        PlayListRequest playListRequest = new PlayListRequest();
        playListRequest.setLgn_id(lgn_id);
        playListRequest.setDistance_type("2");
        playListRequest.setLatitude(latitude);
        playListRequest.setLongitude(longitude);
        playListRequest.setMonth_range("12");
        playListRequest.setPlaylist_distance("1000");

        HTTPWebRequest.GetPlaylistHome(this, playListRequest, AppConstants.APICode.playlist_home, HomeScreenActivity.this);
    }

    private void getAddress() {

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        try {
            List<Address> addresses = null;


            String lat_home = Pref.getValue(AppConstants.PreftDataKey.latitude, "");
            String long_home = Pref.getValue(AppConstants.PreftDataKey.longitude, "");

            Log.e("lat_home", Pref.getValue(AppConstants.PreftDataKey.latitude, ""));
            Log.e("long_home", Pref.getValue(AppConstants.PreftDataKey.longitude, ""));


            if (!(lat_home.equals("") & long_home.equals(""))) {
                addresses = geocoder.getFromLocation(Double.parseDouble(lat_home), Double.parseDouble(long_home), 1);
            }
            if (addresses != null && !addresses.isEmpty()) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                //for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                strReturnedAddress.append(returnedAddress.getLocality());
                // }
                location.setText(strReturnedAddress.toString());
            } else {
                location.setText("No Address returned!");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            location.setText("Canont get Address!");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        getAddress();

        if (Pref.getValue("Refresh", "").equals("0")) {
            getPlayList();

            Pref.setValue("Refresh", "1");
        }

        // Pref.setValue(AppConstants.PreftDataKey.backgroundApp, "true");*/

        // getPlayList();
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
        startActivity(intent);
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.playlist_home:

                        playlistResponse = gson.fromJson(response, PlaylistResponse.class);


                        gson = new GsonBuilder()
                                .registerTypeAdapter(PlayListRequest.class, new PlaylistResponse())
                                .create();
                        PlayListRequest playListRequest = gson.fromJson(response, PlayListRequest.class);

                        if (!playListRequest.getTotalshow().equals(null)) {
                            totalshow_Upcoming = playListRequest.getTotalshow();
                        }
                        if (!playListRequest.getArtistname().equals(null)) {
                            upartist_name.setText(playListRequest.getArtistname());
                        }
                        if (!playListRequest.getTotalshow().equals(null)) {
                            upconrt_total.setText(playListRequest.getTotalshow() + " Concerts");
                        }
                        if (!playListRequest.getTotaltrack().equals(null)) {
                            upconrt_total_trck.setText(playListRequest.getTotaltrack() + " Tracks");
                        }

                       /* if (playListRequest.getImage().length() > 0) {
                            Picasso.with(this)
                                    .load(playListRequest.getImage())
                                    .placeholder(R.mipmap.placeholder)
                                    .error(R.mipmap.placeholder)
                                    .into(img);
                        }*/


                        Glide.with(mContext)
                                .load(playListRequest.getImage())
                                .thumbnail(0.5f)
                                .apply(new RequestOptions()
                                        .placeholder(R.mipmap.placeholder)
                                        .error(R.mipmap.placeholder)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                                .into(img);


                        playlistArray = playlistResponse.getDATA();
                        featureArray = playlistResponse.getFEATURE();

                        Pref.setValue(AppConstants.PreftDataKey.fav_playlist_id, playlistArray.get(1).getPlay_id());
                        Pref.setValue(AppConstants.PreftDataKey.fav_playlist_name, playlistArray.get(1).getPlay_name());

                        totaltrack_local = playlistArray.get(0).getTotaltrack();
                        totalshow_local = playlistArray.get(0).getTotalshow();

                        total_track.setText(totaltrack_local + " Track");
                        total_show.setText(totalshow_local + " Concerts");


                        ArrayList<Track> arrayList = playlistArray.get(0).getArtist();


                        for (int i = 0; i <= arrayList.size(); i++) {

                            Track track = arrayList.get(i);

                            HashMap<String, String> artist = new HashMap<String, String>();
                            artist.put("artist_name", track.getName());

                            artistlist.add(artist);

                            if (i == arrayList.size() - 1) break;
                        }

                        for (int i = 0; i <= artistlist.size(); i++) {
                            artist_name.append(artistlist.get(i).get("artist_name").toString() + ", ");
                            if (i == artistlist.size() - 1) break;
                        }

                        if (playlistArray.size() > 0) {
                            setupLocalAdapter(playlistArray);
                        }
                        if (featureArray.size() > 0) {
                            setupAdapterFeature(featureArray);
                        }
                        if (playlistArray.size() > 0) {
                            setupAdapter(playlistArray);
                        }
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    private void setupLocalAdapter(ArrayList<PlayListRequest> mData2) {
        if (mData2.get(0).getArtist().size()>0) {

            localPlaylistHomeAdapter = new LocalPlaylistHomeAdapter(this, this, mData2);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gridView_locatl.setAdapter(localPlaylistHomeAdapter);
                }
            });
        }
    }

    private void setupAdapter(ArrayList<PlayListRequest> mData) {
        if (mData.size() > 0) {
            adapter = new PlaylistHomeAdapter(this, this, mData);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gridView_playlist.setAdapter(adapter);
                }
            });


        }
    }

    private void setupAdapterFeature(ArrayList<Track> mData1) {
        if (mData1.size() > 0) {
            featureArtistHomeAdapter = new FeatureArtistHomeAdapter(this, this, mData1);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gridView_feature.setAdapter(featureArtistHomeAdapter);
                }
            });
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }
}
