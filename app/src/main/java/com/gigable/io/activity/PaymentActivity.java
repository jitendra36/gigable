package com.gigable.io.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.OrderRequest;
import com.gigable.io.Models.ServiceRequest.Tickets;
import com.gigable.io.Models.ServiceResponse.PaymentResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.security.PrivateKey;
import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class PaymentActivity extends AppCompatActivity implements ApiResponse, AdapterView.OnItemSelectedListener, View.OnClickListener {

    TextView name, number, mm, yy, secur_code, totl_cost, pay_tkt, show_title;
    Spinner card_typ_spin;
    Intent intent;
    ImageView back;

    Spinner mm_spinner;
    Spinner yy_spinner;

    String show_titl_int, show_place_int, show_date_int, totl_cost_int, od_id_int;
    Toolbar toolbar;

    public static final String PUBLISHABLE_KEY = "pk_live_14b3owRRGPFiSYfoukx8vO3U";
    static String strToken = "";

    private String month_selected = "";
    private String year_selected = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_payment);


        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            show_titl_int = bundle.getString(AppConstants.SetGetBundleData.show_title);
            show_place_int = bundle.getString(AppConstants.SetGetBundleData.show_place);
            show_date_int = bundle.getString(AppConstants.SetGetBundleData.show_date);
            totl_cost_int = bundle.getString(AppConstants.SetGetBundleData.total_cost);
            od_id_int = bundle.getString(AppConstants.SetGetBundleData.OrderId);
        }

        init();


        //this is for logout
       /* IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/
    }

    // initailize elements
    private void init() {

        toolbar = (Toolbar) findViewById(R.id.action_bar);
        setSupportActionBar(toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);

        show_title = (TextView) findViewById(R.id.show_title);
        name = (TextView) findViewById(R.id.name);
        number = (TextView) findViewById(R.id.number);

        //mm = (TextView) findViewById(R.id.mm);
        //yy = (TextView) findViewById(R.id.yy);

        secur_code = (TextView) findViewById(R.id.secur);
        totl_cost = (TextView) findViewById(R.id.totl_cost);
        pay_tkt = (TextView) findViewById(R.id.pay_tkt);
        card_typ_spin = (Spinner) findViewById(R.id.card_typ);
        back = (ImageView) findViewById(R.id.back);

        mm_spinner = (Spinner) findViewById(R.id.mm);
        yy_spinner = (Spinner) findViewById(R.id.yy);

        String[] values = {"Master", "Visa"};

        String[] month = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
        String[] year = {"2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026"};


        //Creating the ArrayAdapter instance having the month list

        ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, month);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        mm_spinner.setAdapter(aa);
        mm_spinner.setSelection(0);
       // mm_spinner.setOnItemSelectedListener(this);


        mm_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                mm_spinner.setSelection(position);

                month_selected = adapter.getItemAtPosition(position).toString();

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });




        //Creating the ArrayAdapter instance having the month list

        ArrayAdapter aaa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, year);
        aaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        yy_spinner.setAdapter(aaa);
        yy_spinner.setSelection(0);
      //  yy_spinner.setOnItemSelectedListener(this);


        yy_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                yy_spinner.setSelection(position);

                year_selected = adapter.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        ArrayAdapter<String> tkytpe_adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, values);
        tkytpe_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        //tkytpe_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        card_typ_spin.setAdapter(tkytpe_adapter);
        card_typ_spin.setSelection(0);
        card_typ_spin.setOnItemSelectedListener(this);


        totl_cost.setText("$" + totl_cost_int);
        show_title.setText(show_titl_int);


        pay_tkt.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_tkt:
                getStripeToken();
                break;
            case R.id.back:
                finish();
                break;
        }
    }

    public void getStripeToken() {
        if (name.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter Card Holder Name", Toast.LENGTH_SHORT).show();
        } else if (number.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter Card Number", Toast.LENGTH_SHORT).show();
        }/* else if (mm.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter Card Month", Toast.LENGTH_SHORT).show();
        } else if (yy.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter Card Year", Toast.LENGTH_SHORT).show();
        } */else if (secur_code.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter Card CVV", Toast.LENGTH_SHORT).show();
        } else {
            final ProgressDialog mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setTitle("");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setMessage("Please Wait...");
            mProgressDialog.show();
            try {
                Card card = new Card(number.getText().toString(), Integer.parseInt(month_selected), Integer.parseInt(year_selected), secur_code.getText().toString());
                if (!card.validateCard()) {

                    mProgressDialog.dismiss();
                    Toast.makeText(this, "Please enter valid card detail", Toast.LENGTH_SHORT).show();
                } else {
                    Stripe stripe = new Stripe(PaymentActivity.this);
                    stripe.setDefaultPublishableKey(PUBLISHABLE_KEY);
                    stripe.createToken(card,
                            new TokenCallback() {
                                public void onSuccess(Token token) {
                                    // Send token to your server
                                    mProgressDialog.dismiss();
                                    strToken = token.getId();

                                    //Toast.makeText(PaymentActivity.this, strToken, Toast.LENGTH_LONG).show();

                                    setCardDetails(strToken);
                                }

                                public void onError(Exception error) {
                                    // Show localized error message
                                    mProgressDialog.dismiss();
                                    Toast.makeText(PaymentActivity.this, error.toString(),
                                            Toast.LENGTH_LONG
                                    ).show();
                                }
                            }
                    );
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setCardDetails(String strToken) {

        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setLgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));
        orderRequest.setOd_id(od_id_int);
        orderRequest.setToken(strToken);

        HTTPWebRequest.PayForTkt(PaymentActivity.this, orderRequest, AppConstants.APICode.tkt_pay, PaymentActivity.this);

    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.tkt_pay:

                        PaymentResponse paymentResponse = new PaymentResponse();
                        paymentResponse = gson.fromJson(response, PaymentResponse.class);

                        if (paymentResponse.getSTATUS().equals("Success")) {
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Tickets.class, new PaymentResponse())
                                    .create();
                            Tickets tickets = gson.fromJson(response, Tickets.class);

                            Intent intent = new Intent(PaymentActivity.this, PaymentInfoActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.SetGetBundleData.show_title, tickets.getShow_title());
                            bundle.putString(AppConstants.SetGetBundleData.show_place, show_place_int);
                            bundle.putString(AppConstants.SetGetBundleData.show_date, show_date_int);
                            bundle.putString(AppConstants.SetGetBundleData.total_cost, tickets.getTotal_amount());
                            bundle.putString(AppConstants.SetGetBundleData.ticket_qty, tickets.getTicket_qty());
                            bundle.putString(AppConstants.SetGetBundleData.pay_msg, paymentResponse.getMESSAGES());
                            bundle.putString(AppConstants.SetGetBundleData.pay_status, paymentResponse.getSTATUS());
                            bundle.putString(AppConstants.SetGetBundleData.utkt_id, tickets.getUtkt_id());


                            intent.putExtras(bundle);
                            startActivity(intent);
                        } else
                            Toast.makeText(PaymentActivity.this, paymentResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        card_typ_spin.setSelection(position);
    }


    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
