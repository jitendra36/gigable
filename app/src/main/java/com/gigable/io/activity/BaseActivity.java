package com.gigable.io.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Models.ServiceRequest.Items;
import com.gigable.io.R;
import com.gigable.io.Utility.Common;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.adapters.NavigationDrawerListAdapter;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.util.ArrayList;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;

public class BaseActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, GoogleApiClient.ConnectionCallbacks {

    protected FrameLayout frameLayout;

    protected ListView mDrawerList;

    protected String[] listArray = {"Home", "Home", "Favorites", "My shows", "Notification", "Profile", "Change passsword", "Logout"};

    protected ArrayList<Items> _items;
    protected static int position;
    protected static int position1;

    private static boolean isLaunch = true;
    private DrawerLayout fullLayout;

    private DrawerLayout mDrawerLayout;

    private android.support.v7.app.ActionBarDrawerToggle actionBarDrawerToggle;

    Bundle bundle = new Bundle();

    boolean mSlideState = false;

    private Toolbar mToolbar;

    Intent i;

    GoogleApiClient google_api_client;
    private GoogleSignInOptions gso;


    private static Common mApp;

    public static final String NOTIFY_DELETE = "com.gigable.io.delete";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.navigation_drawer_base_layout);

        mApp = (Common) getApplicationContext();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);

        Fabric.with(this, new Crashlytics());

        fullLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.navigation_drawer_base_layout, null);

        frameLayout = (FrameLayout) fullLayout.findViewById(R.id.content_frame);
        frameLayout.setPadding(0, 0, 0, 0);

        getLayoutInflater().inflate(layoutResID, frameLayout, true);
        super.setContentView(fullLayout);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);


        if (Pref.getValue(AppConstants.PreftDataKey.lgn_typ, "").equals("1")) {
            _items = new ArrayList<Items>();
            _items.add(new Items("Home", R.mipmap.music_side2x));
            _items.add(new Items("Favorites", R.mipmap.fav_side2x));
            _items.add(new Items("My shows", R.mipmap.fav_side2x));
            _items.add(new Items("Profile", R.mipmap.profile2x));
            _items.add(new Items("Change Password", R.mipmap.pwd_side2x));
            _items.add(new Items("Notification", R.mipmap.notification_icon));
            _items.add(new Items("Logout", R.mipmap.logout2x));
        } else if (Pref.getValue(AppConstants.PreftDataKey.lgn_typ, "").equals("2")) {
            _items = new ArrayList<Items>();
            _items.add(new Items("Home", R.mipmap.music_side2x));
            _items.add(new Items("Favorites", R.mipmap.fav_side2x));
            _items.add(new Items("My shows", R.mipmap.fav_side2x));
            _items.add(new Items("Notification", R.mipmap.notification_icon));
            _items.add(new Items("Logout", R.mipmap.logout2x));
        }

        //Adding header on list view
        // View header = (View) getLayoutInflater().inflate(R.layout.list_view_header_layout, null);
        //mDrawerList.addHeaderView(header);

        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new NavigationDrawerListAdapter(this, _items));

        mDrawerList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                if (Pref.getValue(AppConstants.PreftDataKey.lgn_typ, "").equals("1"))
                    openActivity(position);
                else if (Pref.getValue(AppConstants.PreftDataKey.lgn_typ, "").equals("2"))
                    openActivity1(position);

            }
        });


        mToolbar = (Toolbar) findViewById(R.id.action_bar);
        setSupportActionBar(mToolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);


        ImageView menu = (ImageView) findViewById(R.id.menu);


        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mSlideState) {
                    mDrawerLayout.closeDrawer(Gravity.START);
                } else {
                    mDrawerLayout.openDrawer(Gravity.START);
                }
            }
        });

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        actionBarDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        ;
        //drawer.addDrawerListener(actionBarDrawerToggle);
        //actionBarDrawerToggle.syncState();




        /*
        actionBarDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open_drawer, R.string.close_drawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };*/


        // ActionBarDrawerToggle ties together the the proper interactions between the sliding drawer and the action bar app icon
        /*actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,						/* host Activity */
        // mDrawerLayout, 				/* DrawerLayout object */
        //  R.drawable.ic_launcher,     /* nav drawer image to replace 'Up' caret */
        //   R.string.open_drawer,       /* "open drawer" description for accessibility */
        //    R.string.close_drawer)      /* "close drawer" description for accessibility */
        /*
        {


            @Override
            public void onDrawerClosed(View drawerView) {
                getActionBar().setTitle(listArray[position]);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
                super.onDrawerClosed(drawerView);

                mSlideState = false;
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(getString(R.string.app_name));
                invalidateOptionsMenu();
                super.onDrawerOpened(drawerView);

                mSlideState = true;
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }


        };*/


        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        if (isLaunch) {
            isLaunch = false;

            if (Pref.getValue(AppConstants.PreftDataKey.lgn_typ, "").equals("1")) openActivity(0);
            if (Pref.getValue(AppConstants.PreftDataKey.lgn_typ, "").equals("2")) openActivity1(0);

        }


        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing google api client
        google_api_client = new GoogleApiClient.Builder(BaseActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this).build();

        google_api_client.connect();

        //this is for logout
      /*  IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/
    }

    private void openActivity1(int position1) {

        mDrawerLayout.closeDrawer(mDrawerList);
        BaseActivity.position1 = position1;

        switch (position1) {
            case 0:
                i = new Intent(BaseActivity.this, HomeScreenActivity.class);
                startActivity(i);
                break;
            case 1:

                Pref.setValue(AppConstants.PreftDataKey.play_name, Pref.getValue(AppConstants.PreftDataKey.fav_playlist_name, "") + "");
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.play_id, Pref.getValue(AppConstants.PreftDataKey.fav_playlist_id, ""));
                Utility.RedirectToActivity(BaseActivity.this, PlaylistActivity.class, false, bundle);
                break;
            case 2:
                Bundle bundle1 = new Bundle();
                Utility.RedirectToActivity(BaseActivity.this, ShowFragment.class, false, bundle1);
                break;
            case 3:
                Bundle bundle2 = new Bundle();
                Utility.RedirectToActivity(BaseActivity.this, NotificationListActivity.class, false, bundle2);
                break;
            case 4:

                if (mApp.ismIsServiceRunning()){

                    Intent i = new Intent(BaseActivity.this, MediaPlayerService.class);
                    this.stopService(i);

                }


                if (google_api_client.isConnected()) {
                    Auth.GoogleSignInApi.signOut(google_api_client).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    // ...
                                    Toast.makeText(getApplicationContext(), "Logged Out successfully", Toast.LENGTH_SHORT).show();
                                }
                            });
                }

                Pref.setValue(AppConstants.PreftDataKey.lgn_id, "");

                SharedPreferences pref1 = SaveSharedPreference.getSharedPreferences(BaseActivity.this);
                SharedPreferences.Editor editor1 = pref1.edit();
                editor1.clear();
                editor1.commit();

             /*   Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                sendBroadcast(broadcastIntent);*/

                Arrays.fill(listArray, null);

                FacebookSdk.sdkInitialize(getApplicationContext());
                LoginManager.getInstance().logOut();

              /*  Intent i = new Intent(BaseActivity.this, StartScreen.class);
                startActivity(i);*/

                Intent intent = new Intent(BaseActivity.this, StartScreen.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);


                break;
        }
    }

    /**
     * @param position Launching activity when any list item is clicked.
     */
    protected void openActivity(int position) {
        mDrawerLayout.closeDrawer(mDrawerList);
        BaseActivity.position = position;

        switch (position) {
            case 0:
                i = new Intent(BaseActivity.this, HomeScreenActivity.class);
                startActivity(i);
                break;
            case 1:
                Pref.setValue(AppConstants.PreftDataKey.play_name, Pref.getValue(AppConstants.PreftDataKey.fav_playlist_name, "") + "");
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.play_id, Pref.getValue(AppConstants.PreftDataKey.fav_playlist_id, ""));
                Utility.RedirectToActivity(BaseActivity.this, PlaylistActivity.class, false, bundle);
                break;
            case 2:
                Bundle bundle1 = new Bundle();
                Utility.RedirectToActivity(BaseActivity.this, ShowFragment.class, false, bundle1);
                break;

            case 3:
                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
                i = new Intent(BaseActivity.this, UpdateProfileActivity.class);
                startActivity(i);
                break;
            case 4:
                i = new Intent(BaseActivity.this, ChangePasswordActivity.class);
                startActivity(i);
                break;

            case 5:
                Bundle bundle2 = new Bundle();
                Utility.RedirectToActivity(BaseActivity.this, NotificationListActivity.class, false, bundle2);
                break;

            case 6:
                if (google_api_client.isConnected()) {
                    Auth.GoogleSignInApi.signOut(google_api_client).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    // ...
                                    Toast.makeText(getApplicationContext(), "Logged Out successfully", Toast.LENGTH_SHORT).show();
                                }
                            });
                }


                SharedPreferences pref1 = SaveSharedPreference.getSharedPreferences(BaseActivity.this);
                SharedPreferences.Editor editor1 = pref1.edit();
                editor1.clear();
                editor1.commit();

               /* Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.package.ACTION_LOGOUT");
                sendBroadcast(broadcastIntent);*/

                Pref.setValue(AppConstants.PreftDataKey.lgn_id, "" );

                Arrays.fill(listArray, null);

                FacebookSdk.sdkInitialize(getApplicationContext());
                LoginManager.getInstance().logOut();

                Intent intent = new Intent(BaseActivity.this, StartScreen.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

               /* Intent i = new Intent(BaseActivity.this, StartScreen.class);
                startActivity(i);*/
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        return super.onPrepareOptionsMenu(menu);
    }

    /* We can override onBackPressed method to toggle navigation drawer*/
    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (mDrawerLayout.isDrawerOpen(GravityCompat.END)) {
            mDrawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
