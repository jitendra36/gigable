package com.gigable.io.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.LoginRequest;
import com.gigable.io.Models.ServiceRequest.RegisterRequest;
import com.gigable.io.Models.ServiceResponse.LoginGoogleResponse;
import com.gigable.io.Models.ServiceResponse.RegisterResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.app.Config;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.fabric.sdk.android.Fabric;


public class RegisterActivity extends Activity implements View.OnClickListener, ApiResponse, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    EditText name;
    EditText email;
    EditText password;
    EditText confirm_password;
    TextView login_fb;
    private TextView signInButton;
    TextView register, title;
    String device_id = "123456";


    private ArrayList<String> imageURIArray = new ArrayList<>();
    private ArrayList<HashMap<String, String>> imageArry = new ArrayList<>();

    String gcm = "";

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private GoogleSignInOptions gso;
    GoogleApiClient google_api_client;

    String numberToPass = null;

    private static final int REQUEST_PERMISSIONS = 20;
    private SparseIntArray mErrorString;

    LoginButton loginButton;

    boolean internetConnection = false;

    private int RC_SIGN_IN = 100;


    //Facebook login button
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {

        @Override
        public void onSuccess(LoginResult loginResult) {
            Profile profile = Profile.getCurrentProfile();
            //nextActivity(profile);
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    private static final String TAG1 = RegisterActivity.class.getSimpleName();

    IntentFilter intentFilter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        //initialize of fb sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                // nextActivity(newProfile);
            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();


        setContentView(R.layout.activity_register);


        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing google api client
        google_api_client = new GoogleApiClient.Builder(RegisterActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this).build();


        //called fb login button
        loginButton = (LoginButton) findViewById(R.id.login_button);
        callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                //Profile profile = Profile.getCurrentProfile();

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        // Bundle bFacebookData = getFacebookData(object);
                        //Profile profile = Profile.getCurrentProfile();

                        gcm = Pref.getValue(AppConstants.PreftDataKey.PREF_GCM_REGISTRATION_ID, "");

                        try {
                            RegisterRequest registerRequest = new RegisterRequest();
                            registerRequest.setUsr_type("2");
                            registerRequest.setUsr_name(object.getString("first_name"));
                            registerRequest.setUsr_imei_number("123456");
                            registerRequest.setUsr_gcm_number(gcm);
                            registerRequest.setLgn_fb_auth_id(object.getString("id"));
                            HTTPWebRequest.LoginFb(RegisterActivity.this, registerRequest, AppConstants.APICode.fb_login, RegisterActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();


                //nextActivity(profile);


                //  Toast.makeText(getApplicationContext(), "Logging in...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
            }
        };

        List<String> permissions = Arrays.asList("user_photos", "email", "user_birthday", "user_friends");


        loginButton.setReadPermissions(permissions);
        loginButton.registerCallback(callbackManager, callback);


        mErrorString = new SparseIntArray();

      /*  intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter); */
        // set circle bitmap
        init();


        /**
         * Get firebse registrationId
         */

        //displayFirebaseRegId();

    }


    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        // fcm_id = regId;

        Log.e(TAG1, "Firebase reg id: " + regId);


        if (!TextUtils.isEmpty(regId)) {
            gcm = regId;

        }
        // txtRegId.setText("Firebase Reg Id: " + regId);
        //Toast.makeText(this, regId, Toast.LENGTH_LONG).show();
        else
            // txtRegId.setText("Firebase Reg Id is not received yet!");
            Toast.makeText(this, "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
    }


    //initialization of elements
    private void init() {

        name = (EditText) findViewById(R.id.name_reg);
        email = (EditText) findViewById(R.id.email_reg);
        password = (EditText) findViewById(R.id.pwd_reg);
        password = (EditText) findViewById(R.id.pwd_reg);
        confirm_password = (EditText) findViewById(R.id.cpwd_reg);
        register = (TextView) findViewById(R.id.register);

        login_fb = (TextView) findViewById(R.id.login_fb);
        //signup = (TextView) findViewById(R.id.signup);
        signInButton = (TextView) findViewById(R.id.sign_in_button);

        title = (TextView) findViewById(R.id.title);
        register.setOnClickListener(this);
        signInButton.setOnClickListener(this);
        login_fb.setOnClickListener(this);

    }

    // click event of elements
    @Override
    public void onClick(View view) {

        switch (view.getId()) {


            case R.id.register:


                validation();
                if (validation() == true) {

                    gcm = Pref.getValue(AppConstants.PreftDataKey.PREF_GCM_REGISTRATION_ID, "");

                    RegisterRequest registerRequest = new RegisterRequest();
                    registerRequest.setUsr_type("2");
                    registerRequest.setUsr_name(name.getText().toString());
                    registerRequest.setUsr_phone("1234567890");
                    registerRequest.setLgn_email(email.getText().toString());
                    registerRequest.setLgn_password(password.getText().toString());
                    registerRequest.setConfirm_password(confirm_password.getText().toString());
                    registerRequest.setUsr_imei_number(device_id);
                    registerRequest.setUsr_gcm_number(gcm);


                    HTTPWebRequest.Register(RegisterActivity.this, registerRequest, AppConstants.APICode.signup, RegisterActivity.this);
                }

                break;

            case R.id.sign_in_button:
                signInGoogle();
                break;

            case R.id.login_fb:
                if (view == login_fb) {
                    loginButton.performClick();
                }
                break;
        }
    }

    private void signInGoogle() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(google_api_client);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //activity result by of fb and google
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //sign in for google function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();

            gcm = Pref.getValue(AppConstants.PreftDataKey.PREF_GCM_REGISTRATION_ID, "");

            //  Log.d("myTag", result.getSignInAccount().toString());

            //Toast.makeText(LoginActivity.this,acct.toString(),Toast.LENGTH_LONG).show();

            //Displaying name and email
            //Toast.makeText(LoginActivity.this, acct.getDisplayName() + acct.getEmail()+acct.getId()+acct.getPhotoUrl().toString(), Toast.LENGTH_LONG).show();
            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.setUsr_type("2");
            registerRequest.setUsr_name(acct.getDisplayName());
            registerRequest.setUsr_imei_number("123456");
            registerRequest.setUsr_gcm_number(gcm);
            registerRequest.setLgn_google_auth_id(acct.getId());
            registerRequest.setLgn_email(acct.getEmail());

            HTTPWebRequest.LoginGoogle(RegisterActivity.this, registerRequest, AppConstants.APICode.google_login, RegisterActivity.this);
        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }


    //here manage api responce
    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.signup:
                        RegisterResponse registerResponse1 = new RegisterResponse();
                        registerResponse1 = gson.fromJson(response, RegisterResponse.class);

                        if (registerResponse1.getSTATUS().equals("SUCCESS")) {
                            //Toast.makeText(RegisterActivity.this, registerResponse1.getMESSAGES(), Toast.LENGTH_LONG).show();
                            //set responcer data object in loginrequest
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(RegisterRequest.class, new RegisterResponse())
                                    .create();
                            RegisterRequest registerRequest = gson.fromJson(response, RegisterRequest.class);
                            //Toast.makeText(RegisterActivity.this, "id :"+registerRequest.getLgn_id()+"name :"+registerRequest.getUsr_name()+"phone :"+registerRequest.getUsr_phone()+"gender"+registerRequest.getUsr_gender(), Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(this, LoginActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(RegisterActivity.this, registerResponse1.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case AppConstants.APICode.fb_login:
                        LoginGoogleResponse loginGoogleResponse1 = new LoginGoogleResponse();
                        loginGoogleResponse1 = gson.fromJson(response, LoginGoogleResponse.class);

                        if (loginGoogleResponse1.getSTATUS().equals("SUCCESS")) {

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(LoginRequest.class, new LoginGoogleResponse())
                                    .create();
                            LoginRequest loginRequest = gson.fromJson(response, LoginRequest.class);

                            //SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());
                            //SaveSharedPreference.setLgnTyp(this, "2");

                            Pref.setValue(AppConstants.PreftDataKey.lgn_typ, "2" + "");
                            Pref.setValue(AppConstants.PreftDataKey.lgn_id, loginRequest.getLgn_id() + "");

                            SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("id", loginRequest.getLgn_id());
                            //editor.putString(AppConstants.SharedPreferenceKeys.login_from, "2");
                            editor.commit();

                            Toast.makeText(RegisterActivity.this, loginGoogleResponse1.getMESSAGES(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(RegisterActivity.this, GpsGetLocation.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(RegisterActivity.this, loginGoogleResponse1.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case AppConstants.APICode.google_login:
                        LoginGoogleResponse loginGoogleResponse = new LoginGoogleResponse();
                        loginGoogleResponse = gson.fromJson(response, LoginGoogleResponse.class);

                        if (loginGoogleResponse.getSTATUS().equals("SUCCESS")) {

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(LoginRequest.class, new LoginGoogleResponse())
                                    .create();
                            LoginRequest loginRequest = gson.fromJson(response, LoginRequest.class);


                            //SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());
                            // SaveSharedPreference.setLgnTyp(this, "2");

                            Pref.setValue(AppConstants.PreftDataKey.lgn_typ, "2" + "");
                            Pref.setValue(AppConstants.PreftDataKey.lgn_id, loginRequest.getLgn_id() + "");

                            SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            //editor.putString(AppConstants.SharedPreferenceKeys.login_from, "2");
                            editor.putString("id", loginRequest.getLgn_id());
                            editor.commit();

                            Toast.makeText(RegisterActivity.this, loginGoogleResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(RegisterActivity.this, GpsGetLocation.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(RegisterActivity.this, loginGoogleResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            } catch (Exception e) {
            }
        }
    }


    //validation of form
    private boolean validation() {

        boolean value = true;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-z]+";

        String PhoneNo = "+123-456 7890";
        String Regex = "[^\\d]";
        String PhoneDigits = "/\\(?([0-9]{3})\\)?([ .-]?)([0-9]{3})\\2([0-9]{4})/";

        if (!(name.getText().toString().length() > 0)) {
            name.setError("required field");
            value = false;
        } else if (!(email.getText().toString().matches(emailPattern) && email.getText().toString().length() > 0)) {
            email.setError("email invalid");
            value = false;
        } else if (!(password.getText().toString().length() >= 3)) {
            password.setError("minimum three digit required");
            value = false;
        } else if (!(confirm_password.getText().toString().equals(password.getText().toString()))) {
            confirm_password.setError("Not match the password");
            value = false;
        } else {
            value = true;
        }
        return value;
    }


    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            internetConnection = true;
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            internetConnection = false;
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }
        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        google_api_client.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Facebook login
        Profile profile = Profile.getCurrentProfile();
        //nextActivity(profile);
    }

    protected void onStop() {
        super.onStop();
        //Facebook login
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
        google_api_client.disconnect();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {
    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
