package com.gigable.io.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ActivityCommunicator;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.PlayListRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceResponse.UpcomingConcertsResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.adapters.UpcomingConcertstAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Citrsubug on 11/10/2016.
 */
public class UpcomingConcerts extends AppCompatActivity implements ApiResponse, View.OnClickListener {


    UpcomingConcertstAdapter adapter;
    Context context;
    RecyclerView rv;
    Context mContext;

    String play_id_intent;
    String total_track_intent;

    TextView total_show;
    Intent intent;
    ImageView back;
    private Toolbar mToolbar;
    private TextView play_title;
    private TextView total_track;
    private View view;
    private TextView chng_location;
    private TextView location;


    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();

    private ActivityCommunicator activityCommunicator;

    public static Activity fa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.showlist);

        mContext = this;
        AppConstants.CONTEXT = mContext;

        fa = this;

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);

        // total_show = (TextView) findViewById(R.id.total_show);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);

        chng_location = (TextView) findViewById(R.id.chng_loc);
        location = (TextView) findViewById(R.id.location);

        chng_location.setOnClickListener(this);


        //toolbar elements
        play_title = (TextView) findViewById(R.id.play_title);
        total_show = (TextView) findViewById(R.id.total_show);
        total_track = (TextView) findViewById(R.id.total_track);
        view = (View) findViewById(R.id.viewCT);

        total_track.setVisibility(View.GONE);
        view.setVisibility(View.GONE);


        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (!bundle.getString(AppConstants.SetGetBundleData.totalshow).equals("")) ;
            total_show.setText(bundle.getString(AppConstants.SetGetBundleData.totalshow) + " Concerts");
        }
        play_title.setText("Upcoming Concerts");


        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        final LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);





        //this is for logout
     /*   IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/

        getAddress();

        getUpcomingConcerts();

    }

    private void getUpcomingConcerts() {

        PlayListRequest playListRequest =  new PlayListRequest();
        playListRequest.setLatitude(Pref.getValue(AppConstants.PreftDataKey.latitude, ""));
        playListRequest.setLongitude(Pref.getValue(AppConstants.PreftDataKey.longitude, ""));

        HTTPWebRequest.GetUpConcerts(this,playListRequest, AppConstants.APICode.up_concerts, UpcomingConcerts.this);
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (Pref.getValue("Refresh_concerts","").equals("0")){

            getUpcomingConcerts();

            Pref.setValue("Refresh_concerts","1");
        }
    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.up_concerts:

                        gson = new GsonBuilder()
                                .registerTypeAdapter(PlayListRequest.class, new UpcomingConcertsResponse())
                                .create();
                        PlayListRequest playListRequest = gson.fromJson(response, PlayListRequest.class);

                        ArrayList<Show> results = playListRequest.getShow();

                        setupAdapter(results);

                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }


    private void setupAdapter(final ArrayList<Show> mData) {
        if (mData.size() > 0) {
            adapter = new UpcomingConcertstAdapter(context, this, mData);
            rv.setAdapter(adapter);
        }
    }


    private void getAddress() {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);

        try {
            List<Address> addresses = null;

            String lat_home = Pref.getValue(AppConstants.PreftDataKey.latitude, "");
            String long_home = Pref.getValue(AppConstants.PreftDataKey.longitude, "");

            Log.e("lat_home", Pref.getValue(AppConstants.PreftDataKey.latitude, ""));
            Log.e("long_home", Pref.getValue(AppConstants.PreftDataKey.longitude, ""));


            if (!(lat_home.equals("") & long_home.equals(""))) {
                addresses = geocoder.getFromLocation(Double.parseDouble(lat_home), Double.parseDouble(long_home), 1);
            }

            if (addresses != null && !addresses.isEmpty()) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                //for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                strReturnedAddress.append(returnedAddress.getLocality());
                // }
                location.setText(strReturnedAddress.toString());
            } else {
                location.setText("No Address returned!");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            location.setText("Canont get Address!");
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.back:

                finish();

                break;

            case R.id.chng_loc :

                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.from_loc, "2");
                bundle.putString(AppConstants.SetGetBundleData.totalshow, total_show.getText().toString());
                Utility.RedirectToActivity(UpcomingConcerts.this, ChangeLocationActivity.class, false, bundle);

                break;
        }
    }
}
