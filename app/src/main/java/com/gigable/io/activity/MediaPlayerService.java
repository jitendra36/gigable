package com.gigable.io.activity;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.RemoteControlClient;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Healper.EqualizerHelper;
import com.gigable.io.Models.ServiceRequest.Track;
import com.gigable.io.R;
import com.gigable.io.RemoteControlClient.RemoteControlClientCompat;
import com.gigable.io.RemoteControlClient.RemoteControlHelper;
import com.gigable.io.Utility.Common;
import com.gigable.io.Utility.PlayerConstants;
import com.gigable.io.Utility.Utility;
import com.gigable.io.app.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Valdio Veliu on 16-07-11.
 */
public class MediaPlayerService extends Service implements MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnSeekCompleteListener,
        MediaPlayer.OnInfoListener,
        AudioManager.OnAudioFocusChangeListener {

    public static final String BROADCAST_ACTION = "com.gigable.io.displayevent";


    public static MediaPlayer mediaPlayer;

    //PrepareServiceListener instance.
    private PrepareServiceListener mPrepareServiceListener;

    //Context and Intent.
    private Context mContext;
    private Service mService;

    //Handler object.
    private Handler mHandler;

    //Used to pause/resume MediaPlayer
    private int resumePosition;

    //AudioFocus
    private AudioManager audioManager;

    // Binder given to clients
    private final IBinder iBinder = new LocalBinder();

    //List of available Audio files
    public ArrayList<Track> audioList;
    public int audioIndex = -1;


    Intent intent_brodcast;
    Intent intent_brodcast_UI;
    Intent intent_brodcast_seekbar;
    private LocalBroadcastManager mLocalBroadcastManager;

    private Common mApp;


    //AudioManager.
    private AudioManager mAudioManager;
    private AudioManagerHelper mAudioManagerHelper;
    //Equalizer/Audio FX helpers.
    private EqualizerHelper mEqualizerHelper;

    public static final String UPDATE_UI_BROADCAST = "com.gigable.io.NEW_SONG_UPDATE_UI";
    public static final String UPDATE_UI_SEEKBAR = "com.gigable.io.NEW_SONG_UPDATE_SEEKBAR";


    static Handler progressBarHandler = new Handler();

    Common app;


    /***
     * This is for notification
     */

    //AudioPlayer notification ID
    private static final int NOTIFICATION_ID = 101;

    public static final String NOTIFY_PREVIOUS = "com.gigable.io.previous";
    public static final String NOTIFY_DELETE = "com.gigable.io.delete";
    public static final String NOTIFY_PAUSE = "com.gigable.io.pause";
    public static final String NOTIFY_PLAY = "com.gigable.io.play";
    public static final String NOTIFY_NEXT = "com.gigable.io.next";

    private ComponentName remoteComponentName;
    private RemoteControlClient remoteControlClient;

    private static Timer timer;
    private static boolean currentVersionSupportBigNotification = false;
    private static boolean currentVersionSupportLockScreenControls = false;


    //RemoteControlClient for use with remote controls and ICS+ lockscreen controls.
    private RemoteControlClientCompat mRemoteControlClientCompat;
    private ComponentName mMediaButtonReceiverComponent;



    //Storage of the mediaplayer
    StorageUtil storage;





    /**
     * Service lifecycle methods
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

        registerBecomingNoisyReceiver();

        intent_brodcast = new Intent(BROADCAST_ACTION);
        intent_brodcast_UI = new Intent(UPDATE_UI_BROADCAST);
        intent_brodcast_seekbar = new Intent(UPDATE_UI_SEEKBAR);

        app = (Common) getApplication();

        /**
         *
         * songservice
         */
        currentVersionSupportBigNotification = Utility.currentVersionSupportBigNotification();
        currentVersionSupportLockScreenControls = Utility.currentVersionSupportLockScreenControls();

        timer = new Timer();
    }

    //The system calls this method when an activity, requests the service be started
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mContext = getApplicationContext();
        mService = this;
        mHandler = new Handler();

        AppConstants.CONTEXT = mContext;

        //songProgressBar = new WeakReference(PlaylistActivity.songProgressBar);

        mAudioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        mApp = (Common) getApplicationContext();
        mApp.setService(MediaPlayerService.this);
        mApp.setIsServiceRunning(true);
        mAudioManagerHelper = new AudioManagerHelper();

        mAudioManagerHelper.setHasAudioFocus(requestAudioFocus());


        //Initialize audio effects (equalizer, virtualizer, bass boost) for this session.
        //  initAudioFX();

        try {
            //Load data from SharedPreferences
            storage = new StorageUtil(getApplicationContext());

            audioList = storage.loadAudio();
            audioIndex = storage.loadAudioIndex();

           /* if (audioIndex != -1 && audioIndex < audioList.size()) {
                //index is in a valid range
                activeAudio = audioList.get(audioIndex);
            } else {
                stopSelf();
            }*/
        } catch (NullPointerException e) {
            stopSelf();
        }

        //Request audio focus


       /* if (requestAudioFocus() == false) {
            //Could not gain focus
            stopSelf();
        }*/


      /*  if (mediaSessionManager == null) {
            try {
                initMediaSession();
                initMediaPlayer();
            } catch (RemoteException e) {
                e.printStackTrace();
                stopSelf();
            }
            buildNotification(PlaybackStatus.PLAYING);
        }*/

        mMediaButtonReceiverComponent = new ComponentName(this.getPackageName(), new NotificationBroadcast().ComponentName());
        mAudioManager.registerMediaButtonEventReceiver(mMediaButtonReceiverComponent);

        if (currentVersionSupportLockScreenControls) {

            initRemoteControlClient();
        }

        initMediaPlayer();
        newNotification();


        //Handle Intent action from MediaSession.TransportControls
        //   handleIncomingActions(intent);

        /**
         *
         * SongService
         */

        PlayerConstants.SONG_CHANGE_HANDLER = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                try {
                    skipToNext();
                    newNotification();
                    PlaylistActivity.changeButton();

                    ArtistDetailsActivity.changeButton();

                    //  AudioPlayerActivity.changeUI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });


        PlayerConstants.SONG_CLICK_HANDLER = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                try {

                    skipToClickSong();

                    newNotification();
                    PlaylistActivity.changeButton();

                    ArtistDetailsActivity.changeButton();


                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });


        PlayerConstants.PLAYLIST_CHANGE_HANDLER = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                try {
                    audioList.clear();

                    audioList = new StorageUtil(getApplicationContext()).loadAudio();
                    audioIndex = new StorageUtil(getApplicationContext()).loadAudioIndex();
                    stopMedia();

                    mediaPlayer.reset();

                    initMediaPlayer();

                    newNotification();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });



        PlayerConstants.PLAY_PAUSE_HANDLER = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                String message = (String) msg.obj;
                if (mediaPlayer == null)
                    return false;
                if (message.equalsIgnoreCase(getResources().getString(R.string.play))) {
                    PlayerConstants.SONG_PAUSED = false;

                    //  if (checkAndRequestAudioFocus() == true) {

                    if (currentVersionSupportLockScreenControls) {
                        //   remoteControlClient.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
                        mRemoteControlClientCompat.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
                    }

                    mediaPlayer.start();

                    String[] updateFlags = new String[]{
                            Common.UPDATE_SEEKBAR_DURATION,
                    };

                    String[] flagValues = new String[]{"" + mApp.getService().getMediaPlayer().getDuration()
                    };
                    mApp.broadcastUpdateUICommand(updateFlags, flagValues);


                    //  }
                } else if (message.equalsIgnoreCase(getResources().getString(R.string.pause))) {
                    PlayerConstants.SONG_PAUSED = true;
                    if (currentVersionSupportLockScreenControls) {
                        //   remoteControlClient.setPlaybackState(RemoteControlClient.PLAYSTATE_PAUSED);

                        mRemoteControlClientCompat.setPlaybackState(RemoteControlClient.PLAYSTATE_PAUSED);
                    }
                    mediaPlayer.pause();
                }
                newNotification();

                try {
                    PlaylistActivity.changeButton();
                    ArtistDetailsActivity.changeButton();

                } catch (Exception e) {
                }
                Log.d("TAG", "TAG Pressed: " + message);

                return false;
            }
        });

        return START_STICKY;
    }


    public void initRemoteControlClient() {
        if (mRemoteControlClientCompat == null) {
            Intent remoteControlIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
            remoteControlIntent.setComponent(mMediaButtonReceiverComponent);

            mRemoteControlClientCompat = new RemoteControlClientCompat(PendingIntent.getBroadcast(mContext, 0, remoteControlIntent, 0));
            RemoteControlHelper.registerRemoteControlClient(mAudioManager, mRemoteControlClientCompat);

        }

        mRemoteControlClientCompat.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
        mRemoteControlClientCompat.setTransportControlFlags(RemoteControlClient.FLAG_KEY_MEDIA_PLAY |
                RemoteControlClient.FLAG_KEY_MEDIA_PAUSE |
                RemoteControlClient.FLAG_KEY_MEDIA_NEXT |
                RemoteControlClient.FLAG_KEY_MEDIA_STOP |
                RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS);

    }

    @SuppressLint("NewApi")
    private void UpdateMetadata() {
        if (remoteControlClient == null)
            return;
        RemoteControlClient.MetadataEditor metadataEditor = remoteControlClient.editMetadata(true);
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_ALBUM, audioList.get(audioIndex).getPlay_name());
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_ARTIST, audioList.get(audioIndex).getArt_name());
        metadataEditor.putString(MediaMetadataRetriever.METADATA_KEY_TITLE, audioList.get(audioIndex).getTrk_title());

       /* mDummyAlbumArt = UtilFunctions.getAlbumart(getApplicationContext(), data.getAlbumId());
        if(mDummyAlbumArt == null){
            mDummyAlbumArt = BitmapFactory.decodeResource(getResources(), R.drawable.default_album_art);
        }*/

        //  metadataEditor.putBitmap(RemoteControlClient.MetadataEditor.BITMAP_KEY_ARTWORK, mDummyAlbumArt);

        metadataEditor.apply();
        //mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        mAudioManagerHelper.setHasAudioFocus(requestAudioFocus());
    }


    /**
     * Notification
     * Custom Bignotification is available from API 16
     */


    @SuppressLint("NewApi")
    private void newNotification() {
        String songName = audioList.get(audioIndex).getTrk_title();
        String albumName = audioList.get(audioIndex).getPlay_name();
        RemoteViews simpleContentView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.custom_notification);
        RemoteViews expandedView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.big_notification);

        Notification notification = new NotificationCompat.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.btn_play)
                .setContentTitle(songName).getNotification();

        setListeners(simpleContentView);
        setListeners(expandedView);

        notification.contentView = simpleContentView;
        if (currentVersionSupportBigNotification) {
            notification.bigContentView = expandedView;
        }

       try {

            Log.e("Artist link", audioList.get(audioIndex).getArtist_image());

            //Bitmap icon = getBitmapFromURL(audioList.get(audioIndex).getArtist_image());


           Bitmap icon = BitmapFactory.decodeResource(this.getResources(),
                   R.mipmap.icon_gigable);

            //long albumId = PlayerConstants.SONGS_LIST.get(PlayerConstants.SONG_NUMBER).getAlbumId();
            //  Bitmap albumArt = UtilFunctions.getAlbumart(getApplicationContext(), albumId);
            if (icon != null) {

                notification.contentView.setImageViewBitmap(R.id.imageViewAlbumArt, icon);
                if (currentVersionSupportBigNotification) {
                    notification.bigContentView.setImageViewBitmap(R.id.imageViewAlbumArt, icon);
                }
            } else {
                notification.contentView.setImageViewResource(R.id.imageViewAlbumArt, R.drawable.image5);
                if (currentVersionSupportBigNotification) {
                    notification.bigContentView.setImageViewResource(R.id.imageViewAlbumArt, R.drawable.image5);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (PlayerConstants.SONG_PAUSED) {
            notification.contentView.setViewVisibility(R.id.btnPause, View.GONE);
            notification.contentView.setViewVisibility(R.id.btnPlay, View.VISIBLE);

            if (currentVersionSupportBigNotification) {
                notification.bigContentView.setViewVisibility(R.id.btnPause, View.GONE);
                notification.bigContentView.setViewVisibility(R.id.btnPlay, View.VISIBLE);
            }
        } else {
            notification.contentView.setViewVisibility(R.id.btnPause, View.VISIBLE);
            notification.contentView.setViewVisibility(R.id.btnPlay, View.GONE);

            if (currentVersionSupportBigNotification) {
                notification.bigContentView.setViewVisibility(R.id.btnPause, View.VISIBLE);
                notification.bigContentView.setViewVisibility(R.id.btnPlay, View.GONE);
            }
        }

        notification.contentView.setTextViewText(R.id.textSongName, songName);
        notification.contentView.setTextViewText(R.id.textAlbumName, albumName);
        if (currentVersionSupportBigNotification) {
            notification.bigContentView.setTextViewText(R.id.textSongName, songName);
            notification.bigContentView.setTextViewText(R.id.textAlbumName, albumName);
        }
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        startForeground(NOTIFICATION_ID, notification);
    }


    /*public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }*/

    /**
     * Notification click listeners
     *
     * @param view
     */
    public void setListeners(RemoteViews view) {
        Intent previous = new Intent(NOTIFY_PREVIOUS);
        Intent delete = new Intent(NOTIFY_DELETE);
        Intent pause = new Intent(NOTIFY_PAUSE);
        Intent next = new Intent(NOTIFY_NEXT);
        Intent play = new Intent(NOTIFY_PLAY);

        PendingIntent pPrevious = PendingIntent.getBroadcast(getApplicationContext(), 0, previous, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnPrevious, pPrevious);

        PendingIntent pDelete = PendingIntent.getBroadcast(getApplicationContext(), 0, delete, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnDelete, pDelete);

        PendingIntent pPause = PendingIntent.getBroadcast(getApplicationContext(), 0, pause, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnPause, pPause);

        PendingIntent pNext = PendingIntent.getBroadcast(getApplicationContext(), 0, next, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnNext, pNext);

        PendingIntent pPlay = PendingIntent.getBroadcast(getApplicationContext(), 0, play, PendingIntent.FLAG_UPDATE_CURRENT);
        view.setOnClickPendingIntent(R.id.btnPlay, pPlay);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        //   mediaSession.release();
        //    removeNotification();
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            stopMedia();
            mediaPlayer.release();
        }

        mediaPlayer = null;

        // removeAudioFocus();


        //Remove audio focus and unregister the audio buttons receiver.
        mAudioManagerHelper.setHasAudioFocus(false);
        mAudioManager.abandonAudioFocus(audioFocusChangeListener);
        mAudioManager.unregisterMediaButtonEventReceiver(new ComponentName(getPackageName(), new NotificationBroadcast().ComponentName()));
        mAudioManager = null;
        mMediaButtonReceiverComponent = null;
        mRemoteControlClientCompat = null;

        //Nullify the service object.
        mApp.setService(null);
        mApp.setIsServiceRunning(false);
        mApp = null;


        try {
            RemoteControlHelper.unregisterRemoteControlClient(mAudioManager, mRemoteControlClientCompat);
        } catch (Exception e) {

        }

        //unregister BroadcastReceivers
        unregisterReceiver(becomingNoisyReceiver);

        //clear cached playlist
        new StorageUtil(getApplicationContext()).clearCachedAudioPlaylist();
    }

    @Override
    public void onAudioFocusChange(int focusChange) {

    }

    /**
     * Service Binder
     */
    public class LocalBinder extends Binder {
        public MediaPlayerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return MediaPlayerService.this;
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        //Invoked when there has been an error during an asynchronous operation
        switch (what) {
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                Log.d("MediaPlayer Error", "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                Log.d("MediaPlayer Error", "MEDIA ERROR SERVER DIED " + extra);
                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                Log.d("MediaPlayer Error", "MEDIA ERROR UNKNOWN " + extra);
                break;
        }
        return false;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {

        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        //Invoked when the media source is ready for playback.

        // if (checkAndRequestAudioFocus() == true) {

        Log.e("onPrepare call","true");


        playMedia();
        // }
    }

    @Override
    public void onSeekComplete(MediaPlayer mp) {
        //Invoked indicating the completion of a seek operation.
    }


    /**
     * AudioFocus
     */
    private boolean requestAudioFocus() {
        int result = mAudioManager.requestAudioFocus(audioFocusChangeListener,
                AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);

        if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            //Stop the service.
            mService.stopSelf();
            // Toast.makeText(mContext, R.string.close_other_audio_apps, Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    /**
     * Listens for audio focus changes and reacts accordingly.
     */
    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {

        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                //We've temporarily lost focus, so pause the mMediaPlayer, wherever it's at.
                try {

                    Controls.pauseControl(getApplicationContext());

                    mAudioManagerHelper.setHasAudioFocus(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                //Lower the current mMediaPlayer volume.
                mAudioManagerHelper.setAudioDucked(true);
                mAudioManagerHelper.setTargetVolume(5);
                mAudioManagerHelper.setStepDownIncrement(1);
                mAudioManagerHelper.setCurrentVolume(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                mAudioManagerHelper.setOriginalVolume(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                mHandler.post(duckDownVolumeRunnable);

            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {

                if (mAudioManagerHelper.isAudioDucked()) {
                    //Crank the volume back up again.
                    mAudioManagerHelper.setTargetVolume(mAudioManagerHelper.getOriginalVolume());
                    mAudioManagerHelper.setStepUpIncrement(1);
                    mAudioManagerHelper.setCurrentVolume(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

                    mHandler.post(duckUpVolumeRunnable);
                    mAudioManagerHelper.setAudioDucked(false);
                } else {
                    //We've regained focus. Update the audioFocus tag, but don't start the mMediaPlayer.
                    mAudioManagerHelper.setHasAudioFocus(true);

                }

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {

                Controls.pauseControl(getApplicationContext());

                mAudioManagerHelper.setHasAudioFocus(false);
            }
        }
    };


    /**
     * Fades out volume before a duck operation.
     */
    private Runnable duckDownVolumeRunnable = new Runnable() {

        @Override
        public void run() {
            if (mAudioManagerHelper.getCurrentVolume() > mAudioManagerHelper.getTargetVolume()) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        (mAudioManagerHelper.getCurrentVolume() - mAudioManagerHelper.getStepDownIncrement()),
                        0);

                mAudioManagerHelper.setCurrentVolume(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                mHandler.postDelayed(this, 50);
            }
        }
    };

    /**
     * Fades in volume after a duck operation.
     */
    private Runnable duckUpVolumeRunnable = new Runnable() {

        @Override
        public void run() {
            if (mAudioManagerHelper.getCurrentVolume() < mAudioManagerHelper.getTargetVolume()) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        (mAudioManagerHelper.getCurrentVolume() + mAudioManagerHelper.getStepUpIncrement()),
                        0);

                mAudioManagerHelper.setCurrentVolume(mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
                mHandler.postDelayed(this, 50);
            }

        }

    };


    /**
     * MediaPlayer actions
     */
    private void initMediaPlayer() {


        try {
            if (currentVersionSupportLockScreenControls) {
                UpdateMetadata();
                mRemoteControlClientCompat.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
                //  remoteControlClient.setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
            }

            if (mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //Set up MediaPlayer event listeners
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
        mediaPlayer.setOnPreparedListener(this);
        //mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnSeekCompleteListener(this);
        mediaPlayer.setOnInfoListener(this);

        //Reset so that the MediaPlayer is not pointing to another data source
        mediaPlayer.reset();


        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            // Set the data source to the mediaFile location
            mediaPlayer.setDataSource(audioList.get(audioIndex).getTrk_audio());

            storage.storeAudioIndex(audioIndex);

            Log.e("song url", audioList.get(audioIndex).getTrk_audio());
            Log.e("song index", String.valueOf(audioIndex));

        } catch (IOException e) {
            e.printStackTrace();
            stopSelf();
        }

        //  newNotification();

        mediaPlayer.prepareAsync();
    }


    private void playMedia() {

        if (!mediaPlayer.isPlaying()) {

            mediaPlayer.start();

            Log.d("call update seekbar","true");

            String[] updateFlags = new String[]{
                    Common.UPDATE_SEEKBAR_DURATION,
                    Common.UPDATE_HIDE_PROGRESS_DIALOG,
            };

            String[] flagValues = new String[]{"" + mApp.getService().getMediaPlayer().getDuration(),
                    "" + "true",
            };

            mApp.broadcastUpdateUICommand(updateFlags, flagValues);
        }
    }


    private void stopMedia() {
        if (mediaPlayer == null) return;
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
    }

    public void pauseMedia() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            resumePosition = mediaPlayer.getCurrentPosition();
        }
    }

    public void resumeMedia() {

        // if (checkAndRequestAudioFocus()) {
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(resumePosition);
            mediaPlayer.start();
        }
        // }
    }

    protected void skipToNext() {

        if (audioIndex == audioList.size() - 1) {
            //if last in playlist
            audioIndex = 0;

        } else {
            audioIndex = ++audioIndex;
        }

        //Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();

        mediaPlayer.reset();

        String[] updateFlags = new String[]{
                Common.UPDATE_PLAYERUI_NEXT,
        };

        String[] flagValues = new String[]{"" + String.valueOf(audioIndex),
        };
        mApp.broadcastUpdateUICommand(updateFlags, flagValues);


        initMediaPlayer();
    }



    protected void skipToClickSong() {

        audioIndex = new StorageUtil(getApplicationContext()).loadAudioIndex();

        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);

        stopMedia();

        mediaPlayer.reset();

        String[] updateFlags = new String[]{
                Common.UPDATE_PLAYERUI_NEXT,
        };

        String[] flagValues = new String[]{"" + String.valueOf(audioIndex),
        };
        mApp.broadcastUpdateUICommand(updateFlags, flagValues);


        initMediaPlayer();
    }

    public void skipToPrevious() {

        if (audioIndex == 0) {
            //if first in playlist
            //set index to the last of audioList
            audioIndex = audioList.size() - 1;

        } else {
            //get previous in playlist

        }

        //Update stored index
        new StorageUtil(getApplicationContext()).storeAudioIndex(audioIndex);


        stopMedia();
        //reset mediaPlayer
        mediaPlayer.reset();
        initMediaPlayer();
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }


    @Override
    public void onCompletion(MediaPlayer mp) {


        String[] updateFlags = new String[]{
                Common.UPDATE_SONG_COMPLETE,
        };

        String[] flagValues = new String[]{"" + String.valueOf(audioIndex),
        };
        mApp.broadcastUpdateUICommand(updateFlags, flagValues);


        //Controls.nextControl(getApplicationContext());
    }

    /**
     * ACTION_AUDIO_BECOMING_NOISY -- change in audio outputs
     */
    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //pause audio on ACTION_AUDIO_BECOMING_NOISY
            pauseMedia();
            //  buildNotification(PlaybackStatus.PAUSED);
        }
    };

    private void registerBecomingNoisyReceiver() {
        //register after getting audio focus
        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        registerReceiver(becomingNoisyReceiver, intentFilter);
    }

    /**
     * Initializes the equalizer and audio effects for this service session.
     */
    public void initAudioFX() {

        try {
            //Instatiate the equalizer helper object.
            mEqualizerHelper = new EqualizerHelper(mContext, mediaPlayer.getAudioSessionId(),
                    mediaPlayer.getAudioSessionId(),
                    common.isEqualizerEnabled());

        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
            mEqualizerHelper.setIsEqualizerSupported(false);
        } catch (Exception e) {
            e.printStackTrace();
            mEqualizerHelper.setIsEqualizerSupported(false);
        }
    }

    /**
     * Public interface that provides access to
     * major events during the service startup
     * process.
     *
     * @author Saravan Pantham
     */
    public interface PrepareServiceListener {

        /**
         * Called when the service is up and running.
         */
        public void onServiceRunning(MediaPlayerService service);

        /**
         * Called when the service failed to start.
         * Also returns the failure reason via the exception
         * parameter.
         */
        public void onServiceFailed(Exception exception);
    }

    /**
     * Returns an instance of the PrepareServiceListener.
     */
    public PrepareServiceListener getPrepareServiceListener() {
        return mPrepareServiceListener;
    }

    /**
     * Sets the mPrepareServiceListener object.
     */
    public void setPrepareServiceListener(PrepareServiceListener listener) {
        mPrepareServiceListener = listener;
    }


}


