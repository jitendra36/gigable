package com.gigable.io.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.NewPasswordRequest;
import com.gigable.io.Models.ServiceResponse.ForgotPasswordResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.google.gson.Gson;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;

public class NewPasswordActivity extends Activity implements View.OnClickListener, ApiResponse {
    EditText Confirmpassword, password;
    TextView submit, title;
    String id;
    boolean internetConnection = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");

        setContentView(R.layout.activity_new_password);


      /*  IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/

         init();


        //disable actionbar items(title,icon)
        //getActionBar().setDisplayShowHomeEnabled(false);
        //getActionBar().setDisplayShowTitleEnabled(false);
    }

    //initialization of elements
    private void init() {

        password = (EditText) findViewById(R.id.pwd_change);
        Confirmpassword = (EditText) findViewById(R.id.cpwd_change);
        title = (TextView) findViewById(R.id.title);

        submit = (TextView) findViewById(R.id.submit_change);
        submit.setOnClickListener(this);
    }


    //click event of elements
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.submit_change:
                validation();

                if (validation() == true) {

                    NewPasswordRequest newPasswordRequest = new NewPasswordRequest();
                    newPasswordRequest.setLgn_id(id);
                    newPasswordRequest.setNew_password(password.getText().toString());
                    newPasswordRequest.setCnfm_password(Confirmpassword.getText().toString());
                    HTTPWebRequest.NewPassword(NewPasswordActivity.this, newPasswordRequest, AppConstants.APICode.new_pwd, NewPasswordActivity.this);
                }
                break;

        }
    }


    //here manage api responce
    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {

                    case AppConstants.APICode.new_pwd:
                        ForgotPasswordResponse forgotPasswordResponse = new ForgotPasswordResponse();
                        forgotPasswordResponse = gson.fromJson(response, ForgotPasswordResponse.class);

                        if (forgotPasswordResponse.getSTATUS().equals("SUCCESS")) {
                            Toast.makeText(NewPasswordActivity.this, forgotPasswordResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(NewPasswordActivity.this, LoginActivity.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(NewPasswordActivity.this, forgotPasswordResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            } catch (Exception e) {

                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    //validation of form
    private boolean validation() {

        boolean value = true;

        if (!(password.getText().toString().length() >= 3)) {
            password.setError("minimum three digit required");
            value = false;
        } else if (!(Confirmpassword.getText().toString().equals(password.getText().toString()))) {
            Confirmpassword.setError("not match password");
            value = false;
        } else {
            value = true;
        }
        return value;
    }


    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            internetConnection = true;
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            internetConnection = false;
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }
        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void networkError(int apiCode) {
    }

    @Override
    public void responseError(String responseError, int apiCode) {
    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {
    }

}
