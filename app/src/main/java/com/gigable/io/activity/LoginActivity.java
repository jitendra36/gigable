package com.gigable.io.activity;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.LoginRequest;
import com.gigable.io.Models.ServiceRequest.RegisterRequest;
import com.gigable.io.Models.ServiceResponse.LoginGoogleResponse;
import com.gigable.io.Models.ServiceResponse.LoginResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.app.Config;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ApiResponse, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    EditText email, password;
    TextView login, signup, forgotpwd, login_fb, login_google, orTxt;
    GoogleApiClient google_api_client;
    private TextView signInButton;
    private GoogleSignInOptions gso;
    private int RC_SIGN_IN = 100;
    boolean fblogin_status = false;

    String gcm = "";
    String imei_number = "123456";
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    String device_id;

    private SparseIntArray mErrorString;
    private static final int REQUEST_PERMISSIONS = 20;

    LoginButton loginButton;
    Context mContext;

    private static final String TAG1 = LoginActivity.class.getSimpleName();

    //Facebook login button
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {

        @Override
        public void onSuccess(LoginResult loginResult) {
            Profile profile = Profile.getCurrentProfile();
            //nextActivity(profile);
        }

        @Override
        public void onCancel() {
        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mErrorString = new SparseIntArray();

        mContext = this;
        AppConstants.CONTEXT = mContext;

        Fabric.with(this, new Crashlytics());

        //initialize of fb sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                // nextActivity(newProfile);
            }
        };
        accessTokenTracker.startTracking();
        profileTracker.startTracking();


        //inflate layout
        setContentView(R.layout.activity_login1);


        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        //Initializing google api client
        google_api_client = new GoogleApiClient.Builder(LoginActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addConnectionCallbacks(this).build();


        //called fb login button
        loginButton = (LoginButton) findViewById(R.id.login_button);
        callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                //Profile profile = Profile.getCurrentProfile();

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        // Bundle bFacebookData = getFacebookData(object);
                        //Profile profile = Profile.getCurrentProfile();

                        gcm = Pref.getValue(AppConstants.PreftDataKey.PREF_GCM_REGISTRATION_ID, "");
                        try {
                            RegisterRequest registerRequest = new RegisterRequest();
                            registerRequest.setUsr_type("2");
                            registerRequest.setUsr_name(object.getString("first_name"));
                            registerRequest.setUsr_imei_number("123456");
                            registerRequest.setUsr_gcm_number(gcm);
                            registerRequest.setLgn_fb_auth_id(object.getString("id"));
                            HTTPWebRequest.LoginFb(LoginActivity.this, registerRequest, AppConstants.APICode.fb_login, LoginActivity.this);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();


                //nextActivity(profile);


                //  Toast.makeText(getApplicationContext(), "Logging in...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {
            }
        };

        List<String> permissions = Arrays.asList("user_photos", "email", "user_birthday", "user_friends");


        loginButton.setReadPermissions(permissions);
        loginButton.registerCallback(callbackManager, callback);

        init();

    }

    @Override
    protected void onStart() {
        super.onStart();
        google_api_client.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Facebook login
        Profile profile = Profile.getCurrentProfile();
        //nextActivity(profile);

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
        //Facebook login
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
        google_api_client.disconnect();
    }

    // initialization of activity elements
    private void init() {

        email = (EditText) findViewById(R.id.email_login);
        password = (EditText) findViewById(R.id.pwd_login);
        login = (TextView) findViewById(R.id.login);
        login_fb = (TextView) findViewById(R.id.login_fb);
        forgotpwd = (TextView) findViewById(R.id.forgotpwd);
        //signup = (TextView) findViewById(R.id.signup);
        signInButton = (TextView) findViewById(R.id.sign_in_button);


        login.setOnClickListener(this);
        // signup.setOnClickListener(this);
        signInButton.setOnClickListener(this);
        forgotpwd.setOnClickListener(this);
        login_fb.setOnClickListener(this);

    }


    //request permission result for marshmallow

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        for (int permission : grantResults) {
            permissionCheck = permissionCheck + permission;
        }
        if ((grantResults.length > 0) && permissionCheck == PackageManager.PERMISSION_GRANTED) {
            onPermissionsGranted(requestCode);
        } else {
            Snackbar.make(findViewById(android.R.id.content), mErrorString.get(requestCode),
                    Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + getPackageName()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                            startActivity(intent);
                        }
                    }).show();
        }
    }


    //request permission for marshmallow
    public void requestAppPermissions(final String[] requestedPermissions,
                                      final int stringId, final int requestCode) {
        mErrorString.put(requestCode, stringId);
        int permissionCheck = PackageManager.PERMISSION_GRANTED;
        boolean shouldShowRequestPermissionRationale = false;
        for (String permission : requestedPermissions) {
            permissionCheck = permissionCheck + ContextCompat.checkSelfPermission(this, permission);
            shouldShowRequestPermissionRationale = shouldShowRequestPermissionRationale || ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
        }
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale) {
                Snackbar.make(findViewById(android.R.id.content), stringId,
                        Snackbar.LENGTH_INDEFINITE).setAction("GRANT",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ActivityCompat.requestPermissions(LoginActivity.this, requestedPermissions, requestCode);
                            }
                        }).show();
            } else {
                ActivityCompat.requestPermissions(this, requestedPermissions, requestCode);
            }
        } else {
            onPermissionsGranted(requestCode);
        }
    }

    public void onPermissionsGranted(int requestCode) {
        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        device_id = tm.getDeviceId();
    }

    //all click event
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.login:

                validation();

                if (validation() == true) {

                    gcm = Pref.getValue(AppConstants.PreftDataKey.PREF_GCM_REGISTRATION_ID, "");

                    LoginRequest loginRequest = new LoginRequest();
                    loginRequest.setUsr_type("2");
                    loginRequest.setUsr_imei_number(imei_number);
                    loginRequest.setUsr_gcm_number(gcm);
                    loginRequest.setLgn_email(email.getText().toString());
                    loginRequest.setLgn_password(password.getText().toString());

                    HTTPWebRequest.Login(LoginActivity.this, loginRequest, AppConstants.APICode.login, LoginActivity.this);
                }

                break;

            case R.id.sign_in_button:
                signInGoogle();
                break;

            case R.id.forgotpwd:
                Intent intent = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivity(intent);

                break;

            case R.id.login_fb:
                if (view == login_fb) {
                    loginButton.performClick();
                }
                break;
        }
    }

    private void signInGoogle() {
        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(google_api_client);

        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    //activity result by of fb and google
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If signin
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //sign in for google function
    private void handleSignInResult(GoogleSignInResult result) {
        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            gcm = Pref.getValue(AppConstants.PreftDataKey.PREF_GCM_REGISTRATION_ID, "");
            GoogleSignInAccount acct = result.getSignInAccount();
            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.setUsr_type("2");
            registerRequest.setUsr_name(acct.getDisplayName());
            registerRequest.setUsr_imei_number("123456");
            registerRequest.setUsr_gcm_number(gcm);
            registerRequest.setLgn_google_auth_id(acct.getId());
            registerRequest.setLgn_email(acct.getEmail());

            HTTPWebRequest.LoginGoogle(LoginActivity.this, registerRequest, AppConstants.APICode.google_login, LoginActivity.this);
        } else {
            //If login fails
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show();
        }
    }

    //get api responce  here
    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {

                    case AppConstants.APICode.login:
                        LoginResponse loginResponse2 = new LoginResponse();
                        loginResponse2 = gson.fromJson(response, LoginResponse.class);

                        if (loginResponse2.getSTATUS().equals("SUCCESS")) {
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(LoginRequest.class, new LoginResponse())
                                    .create();
                            LoginRequest loginRequest = gson.fromJson(response, LoginRequest.class);

                            SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());
                            Pref.setValue(AppConstants.PreftDataKey.usr_name, loginRequest.getUsr_name() + "");
                            Pref.setValue(AppConstants.PreftDataKey.lgn_email, loginRequest.getLgn_email() + "");
                            Pref.setValue(AppConstants.PreftDataKey.lgn_id, loginRequest.getLgn_id() + "");
                            Pref.setValue(AppConstants.PreftDataKey.lgn_typ, "1" + "");
                            Pref.setValue(AppConstants.PreftDataKey.android_link, loginRequest.getAndroid_link() + "");

                            Intent intent = new Intent(this, GpsGetLocation.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(LoginActivity.this, loginResponse2.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;

                    case AppConstants.APICode.fb_login:
                        LoginGoogleResponse loginGoogleResponse1 = new LoginGoogleResponse();
                        loginGoogleResponse1 = gson.fromJson(response, LoginGoogleResponse.class);

                        if (loginGoogleResponse1.getSTATUS().equals("SUCCESS")) {

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(LoginRequest.class, new LoginGoogleResponse())
                                    .create();
                            LoginRequest loginRequest = gson.fromJson(response, LoginRequest.class);

                            //SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());
                            //SaveSharedPreference.setLgnTyp(this, "2");

                            Pref.setValue(AppConstants.PreftDataKey.lgn_typ, "2" + "");
                            Pref.setValue(AppConstants.PreftDataKey.lgn_id, loginRequest.getLgn_id() + "");
                            Pref.setValue(AppConstants.PreftDataKey.android_link, loginRequest.getAndroid_link() + "");

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("id", loginRequest.getLgn_id());
                            //editor.putString(AppConstants.SharedPreferenceKeys.login_from, "2");
                            editor.commit();

                            SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());

                            Toast.makeText(LoginActivity.this, loginGoogleResponse1.getMESSAGES(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(LoginActivity.this, GpsGetLocation.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(LoginActivity.this, loginGoogleResponse1.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;
                    case AppConstants.APICode.google_login:
                        LoginGoogleResponse loginGoogleResponse = new LoginGoogleResponse();
                        loginGoogleResponse = gson.fromJson(response, LoginGoogleResponse.class);

                        if (loginGoogleResponse.getSTATUS().equals("SUCCESS")) {

                            gson = new GsonBuilder()
                                    .registerTypeAdapter(LoginRequest.class, new LoginGoogleResponse())
                                    .create();
                            LoginRequest loginRequest = gson.fromJson(response, LoginRequest.class);


                            //SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());
                            // SaveSharedPreference.setLgnTyp(this, "2");

                            Pref.setValue(AppConstants.PreftDataKey.lgn_typ, "2" + "");
                            Pref.setValue(AppConstants.PreftDataKey.lgn_id, loginRequest.getLgn_id() + "");
                            Pref.setValue(AppConstants.PreftDataKey.android_link, loginRequest.getAndroid_link() + "");

                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref.edit();
                            //editor.putString(AppConstants.SharedPreferenceKeys.login_from, "2");
                            editor.putString("id", loginRequest.getLgn_id());
                            editor.commit();

                            SaveSharedPreference.setUserID(this, loginRequest.getLgn_id());

                            Toast.makeText(LoginActivity.this, loginGoogleResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                            Intent i = new Intent(LoginActivity.this, GpsGetLocation.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(LoginActivity.this, loginGoogleResponse.getMESSAGES(), Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    //validation of form
    private boolean validation() {
        boolean value = true;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z]+\\.+[a-z]+";
        if (!(email.getText().toString().matches(emailPattern) && email.getText().toString().length() > 0)) {
            email.setError("email invalid");
            value = false;
        } else if (!(password.getText().toString().length() >= 3)) {
            password.setError("minimum three digit required");
            value = false;
        } else {
            value = true;
        }
        return value;
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }

    @Override
    public void onConnected(Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

}
