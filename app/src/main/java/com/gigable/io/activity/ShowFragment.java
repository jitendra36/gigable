package com.gigable.io.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.ExpandableRecycle.ExpandableRecyclerAdapter;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.FavShowRequest;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceRequest.ShowExpand;
import com.gigable.io.Models.ServiceResponse.FavShowResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.gigable.io.adapters.FavShowAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Citrsubug on 11/10/2016.
 */
public class ShowFragment extends AppCompatActivity implements ApiResponse, View.OnClickListener {

    RecyclerView rv;
    FavShowAdapter adapter;
    Context context;

    private Toolbar mToolbar;

    private TextView title;
    private TextView total_show;
    private TextView total_track;
    private View view;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.show_fragment);


        rv = (RecyclerView) findViewById(R.id.rv);
        rv.setHasFixedSize(true);


        Show show = new Show();
        show.setMsf_lgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));

        HTTPWebRequest.GetFavShow(this, show, AppConstants.APICode.fav_showlst, ShowFragment.this);


        //Set toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);

        //Set title of the toolbar
        title = (TextView) findViewById(R.id.play_title);
        title.setText("My Shows");

        //toolbar elements
        back = (ImageView) findViewById(R.id.back);
        total_show = (TextView) findViewById(R.id.total_show);
        total_track = (TextView) findViewById(R.id.total_track);
        view = (View) findViewById(R.id.viewCT);

        total_show.setVisibility(View.GONE);
        total_track.setVisibility(View.GONE);
        view.setVisibility(View.GONE);

        back.setOnClickListener(this);


        //this is for logout
    /*    IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/

    }

    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.fav_showlst:

                        FavShowResponse favShowResponse = new FavShowResponse();
                        favShowResponse = gson.fromJson(response, FavShowResponse.class);

                        if (favShowResponse.getSTATUS().equals("SUCCESS")) {
                            //Toast.makeText(LoginActivity.this, loginResponse2.getMESSAGES(), Toast.LENGTH_LONG).show();

                            //set responcer data object in loginrequest
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Show.class, new FavShowResponse())
                                    .create();
                            Show show = gson.fromJson(response, Show.class);

                            //final ArrayList<Show> shows1 = new ArrayList<>();

                            ArrayList<FavShowRequest> ticketArray = show.getTicketArray();
                            ArrayList<FavShowRequest> currentArray = show.getCurrentArray();
                            ArrayList<FavShowRequest> pastArray = show.getPastArray();

                            ShowExpand ticket = new ShowExpand("Ticket", ticketArray, String.valueOf(ticketArray.size()));
                            ShowExpand current = new ShowExpand("Watching . . . ", currentArray, String.valueOf(currentArray.size()));
                            ShowExpand past = new ShowExpand("Past shows", pastArray, String.valueOf(pastArray.size()));

                            final List<ShowExpand> showExpands = Arrays.asList(ticket, current, past);

                            setupAdapter(showExpands);
                        }
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    private void setupAdapter(final List<ShowExpand> showExpands) {

        adapter = new FavShowAdapter(context, this, showExpands);
        adapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @UiThread
            @Override
            public void onParentExpanded(int parentPosition) {

            }

            @UiThread
            @Override
            public void onParentCollapsed(int parentPosition) {

            }
        });

        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this));
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        adapter.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.back:
                Bundle bundle = new Bundle();
                Utility.RedirectToActivity(ShowFragment.this, HomeScreenActivity.class, false, bundle);
                break;


        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }



}
