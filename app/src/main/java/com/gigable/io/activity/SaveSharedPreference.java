package com.gigable.io.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Citrsubug on 11/11/2016.
 */

public class SaveSharedPreference {


    static final String latitude_main= "latitude";
    static final String longitude_main= "longitude";
    static final String lgn_id_prf= "lgn_id";



    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }


    public static void setLatitude(Context ctx, String latitude)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(latitude_main, latitude);
        editor.commit();
    }
    public static String getLatitude(Context ctx)
    {
        return getSharedPreferences(ctx).getString(latitude_main,"");
    }
    public static void setLongitude(Context ctx, String longitude)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(longitude_main, longitude);
        editor.commit();
    }
    public static String getLongitude(Context ctx)
    {
        return getSharedPreferences(ctx).getString(longitude_main,"");
    }

    public static void setUserID(Context ctx, String lgn_id) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(lgn_id_prf, lgn_id);
        editor.commit();
    }
    public static String getUserID(Context ctx)
    {
        return getSharedPreferences(ctx).getString(lgn_id_prf,"");
    }
}
