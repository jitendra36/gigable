package com.gigable.io.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.crashlytics.android.Crashlytics;
import com.gigable.io.AppConstants.AppConstants;
import com.gigable.io.Listner.ApiResponse;
import com.gigable.io.Models.ServiceRequest.Show;
import com.gigable.io.Models.ServiceResponse.ShowDetailsResponse;
import com.gigable.io.Network.HTTPWebRequest;
import com.gigable.io.R;
import com.gigable.io.Utility.Pref;
import com.gigable.io.Utility.Utility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import io.fabric.sdk.android.Fabric;

public class Show_detailsActivity extends Activity implements ApiResponse, View.OnClickListener {

    TextView show_title;
    TextView show_venu;
    TextView show_location;
    TextView show_city;
    TextView show_date;
    TextView art_name;
    TextView show_time;
    TextView get_tkt;
    ImageView art_img;
    ImageView favourite;
    ImageView close;
    ImageView tkt;
    String show_id = "";
    String art_name1 = "";
    String art_img1 = "";
    LinearLayout linear_show;
    RelativeLayout fav_relative;


    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_show_details);

        init();

        Show show = new Show();

        //show.setShow_id(show_id);

        show.setShow_id(show_id);
        show.setLgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));

        HTTPWebRequest.ShowDetails(Show_detailsActivity.this, show, AppConstants.APICode.show_detail, Show_detailsActivity.this);

        //this is for logout
      /*  IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive", "Logout in progress");
                //At this point you should start the login activity and finish this one
                finish();
            }
        }, intentFilter);*/
    }

    private void init() {

        art_img = (ImageView) findViewById(R.id.art_img);
        favourite = (ImageView) findViewById(R.id.favourite);
        show_title = (TextView) findViewById(R.id.show_title);
        show_venu = (TextView) findViewById(R.id.show_venu);
        show_location = (TextView) findViewById(R.id.show_location);
        show_city = (TextView) findViewById(R.id.show_city);
        show_date = (TextView) findViewById(R.id.show_date);
        show_time = (TextView) findViewById(R.id.show_time);
        art_name = (TextView) findViewById(R.id.art_name);
        get_tkt = (TextView) findViewById(R.id.get_tkt);
        close = (ImageView) findViewById(R.id.close);
        tkt = (ImageView) findViewById(R.id.tkt);
        linear_show = (LinearLayout)findViewById(R.id.linear_show);
        fav_relative = (RelativeLayout)findViewById(R.id.fav_relative);


        intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            show_id = bundle.getString(AppConstants.SetGetBundleData.show_id);
          //  art_name1 = bundle.getString(AppConstants.SetGetBundleData.art_name);
           // art_img1 = bundle.getString(AppConstants.SetGetBundleData.art_img);
        }


       /* if (!art_name1.equals("")) art_name.setText(art_name1);
        if (!art_img1.equals("")) {
            Picasso.with(this)
                    .load(art_img1)
                    .placeholder(R.mipmap.placeholder)
                    .error(R.mipmap.placeholder)
                    .into(art_img);
        }*/


        get_tkt.setOnClickListener(this);
        close.setOnClickListener(this);
        tkt.setOnClickListener(this);
        favourite.setOnClickListener(this);
        fav_relative.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.get_tkt:
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.SetGetBundleData.show_id, show_id);
                Utility.RedirectToActivity(this, PurchaseActivity.class, false, bundle);
                break;

            case R.id.close:
                finish();
                break;

            case R.id.tkt:
                Bundle bundle1 = new Bundle();
                bundle1.putString(AppConstants.SetGetBundleData.show_id, show_id);
                Utility.RedirectToActivity(this, PurchaseActivity.class, false, bundle1);
                break;

            case R.id.favourite:

                Show show = new Show();
                show.setMsf_lgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));
                show.setMsf_show_id(show_id);

                HTTPWebRequest.AddShowFav(Show_detailsActivity.this, show, AppConstants.APICode.show_fav, Show_detailsActivity.this);
                break;

            case R.id.fav_relative :

                Show show1 = new Show();
                show1.setMsf_lgn_id(Pref.getValue(AppConstants.PreftDataKey.lgn_id, ""));
                show1.setMsf_show_id(show_id);

                HTTPWebRequest.AddShowFav(Show_detailsActivity.this, show1, AppConstants.APICode.show_fav, Show_detailsActivity.this);
                break;

        }
    }


    @Override
    public void apiResponsePostProcessing(String response, int apiCode) {
        Log.d("Login Responce", "response=====" + response);
        if (response != null) {
            Gson gson = new Gson();
            try {
                switch (apiCode) {
                    case AppConstants.APICode.show_detail:

                        ShowDetailsResponse showDetailsResponse = new ShowDetailsResponse();
                        showDetailsResponse = gson.fromJson(response, ShowDetailsResponse.class);

                        if (showDetailsResponse.getSTATUS().equals("SUCCESS")) {
                            //Toast.makeText(LoginActivity.this, loginResponse2.getMESSAGES(), Toast.LENGTH_LONG).show();

                            //set responcer data object in loginrequest
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Show.class, new ShowDetailsResponse())
                                    .create();

                            Show show = gson.fromJson(response, Show.class);

                            if (show.getShow_title() != null)
                                show_title.setText(show.getShow_title());
                            if (show.getVen_name() != null) show_venu.setText(show.getVen_name());
                            if (show.getVen_location() != null)
                                show_location.setText(show.getVen_location());
                            if (show.getVen_city() != null) show_city.setText(show.getVen_city());
                            if (show.getShow_date() != null) {

                                String currantdate = new SimpleDateFormat("dd MMM yyyy").format(new Date());

                                Date date_currant = new SimpleDateFormat("dd MMM yyyy").parse(currantdate);


                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                                DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
                                String inputDateStr = show.getShow_date();
                                Date date = null;
                                try {
                                    date = inputFormat.parse(inputDateStr);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String outputDateStr = outputFormat.format(date);

                                Date date_show = new SimpleDateFormat("dd MMM yyyy").parse(outputDateStr);

                                show_date.setText(outputDateStr);


                                if (date_currant.compareTo(date_show)<=0)
                                {
                                    linear_show.setVisibility(View.VISIBLE);
                                }

                            }
                            if (show.getShow_time() != null) {

                                DateFormat inputFormat = new SimpleDateFormat("hh:mm:ss");
                                DateFormat outputFormat = new SimpleDateFormat("hh:mm a");
                                String inputDateStr = show.getShow_time();
                                Date date = null;
                                try {
                                    date = inputFormat.parse(inputDateStr);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String outputDateStr = outputFormat.format(date);


                                show_time.setText(outputDateStr);
                            }

                            if (show.getMsf_show_fav_status().equals("false")) {

                                favourite.setImageResource(R.mipmap.fav);
                            } else {
                                favourite.setImageResource(R.mipmap.fav_fill);
                            }




                           if (!show.getArtist().get(0).getArt_name().equals("")) {
                                art_name.setText(show.getArtist().get(0).getArt_name());
                            }

                            if (!show.getArtist().get(0).getArt_image().equals("")) {
                                //art_name.setText(show.getArtist().get(0).getArt_name());

                                Picasso.with(this)
                                        .load(show.getArtist().get(0).getArt_image())
                                        .placeholder(R.mipmap.placeholder)
                                        .error(R.mipmap.placeholder)
                                        .into(art_img);

                            }

                            if (!show.getTickets().get(0).getTicket_price().equals("")) {
                                get_tkt.setText("GET TICKETS - " +"$"+show.getTickets().get(0).getTicket_price()+"+");
                            }

                        }
                        break;

                    case AppConstants.APICode.show_fav:

                        ShowDetailsResponse showDetailsResponse1 = new ShowDetailsResponse();
                        showDetailsResponse1 = gson.fromJson(response, ShowDetailsResponse.class);

                        if (showDetailsResponse1.getSTATUS().equals("SUCCESS")) {
                            //Toast.makeText(LoginActivity.this, loginResponse2.getMESSAGES(), Toast.LENGTH_LONG).show();

                            //set responcer data object in loginrequest
                            gson = new GsonBuilder()
                                    .registerTypeAdapter(Show.class, new ShowDetailsResponse())
                                    .create();

                            Show show = gson.fromJson(response, Show.class);


                            if (show.getMsf_show_fav_status().equals("false")) {
                                favourite.setImageResource(R.mipmap.fav);
                            } else {
                                favourite.setImageResource(R.mipmap.fav_fill);
                            }
                        }
                        break;
                }
            } catch (Exception e) {
                Log.e("OrderListActivity ", "Error in response" + e);
            }
        }
    }

    @Override
    public void networkError(int apiCode) {

    }

    @Override
    public void responseError(String responseError, int apiCode) {

    }

    @Override
    public void responseError(String responseError, ArrayList<Integer> successedUrlList, int apiCode) {

    }
}
