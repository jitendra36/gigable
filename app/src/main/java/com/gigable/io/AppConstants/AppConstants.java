package com.gigable.io.AppConstants;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by abc on 25-Jun-16.
 */
public class AppConstants {


    public static Context CONTEXT;

    // APPLICATION PREFERENCE FILE NAME
    public static final String PREF_FILE = "PREF_VMSS";


    //Broadcast elements.
    private LocalBroadcastManager mLocalBroadcastManager;
    public static final String UPDATE_UI_BROADCAST = "com.gigable.io.NEW_SONG_UPDATE_UI";

    public static String baseURL = "https://www.gigable.io/app/service/";

   // public static String baseURL = "http://52.34.59.210/app/service/";


    //api number
    public class APICode {
        public static final int playlist = 1;
        public static final int tracklist = 2;
        public static final int login = 3;
        public static final int google_login = 4;
        public static final int fb_login = 5;
        public static final int signup = 6;
        public static final int forgot_pwd = 7;
        public static final int forgot_pwd_verification = 8;
        public static final int new_pwd = 9;
        public static final int change_pwd = 10;
        public static final int get_profile = 11;
        public static final int update_profile = 12;
        public static final int show_details = 13;
        public static final int tkt_purchase = 14;
        public static final int tkt_pay = 15;

        public static final int playlist_home = 16;
        public static final int art_detail = 17;
        public static final int fav_trklst = 18;

        public static final int addDelTrack = 19;
        public static final int fav_showlst = 20;
        public static final int delShow = 21;
        public static final int show_detail = 22;
        public static final int showlist = 23;
        public static final int change_location = 24;
        public static final int up_concerts = 25;
        public static final int show_fav = 26;
        public static final int track_fav = 27;
        public static final int pay_info = 28;
        public static final int notification = 29;
        public static final int getVersion = 30;

    }

    //all api url
    public static class APIURL {

        public static final String getPlaylist = baseURL + "playlist/fetchplaylistbylocation";
        public static final String gettracklist = baseURL + "playlist/index";

        public static final String login = baseURL + "user/login";
        public static final String signup = baseURL + "user/signup";
        public static final String forgot_pwd = baseURL + "user/forgotpassword";
        public static final String forgot_pwd_verification = baseURL + "user/verifycode";
        public static final String newPassword = baseURL + "user/newpassword";
        public static final String changepwd = baseURL + "user/changepassword";
        public static final String fblogin = baseURL + "user/facebooklogin";
        public static final String googlelogin = baseURL + "user/googlelogin";
        // public static final String get_profile = baseURL + "user/getprofile";
        public static final String updateProfile = baseURL + "user/profile";
        public static final String showDetails = baseURL + "playlist/showdetails";
        public static final String tkt_purchase = baseURL + "ticket/bookticket";
        public static final String tkt_pay = baseURL + "ticket/paymentprocess";
        public static final String art_detl = baseURL + "playlist/artistdetails";
        public static final String get_favtracklist = baseURL + "playlist/fetchuserfavourite";
        public static final String addDelTrack = baseURL + "playlist/favourite";
        public static final String get_favShowlist = baseURL + "show/listfavouriteshowdev";
        public static final String delShow = baseURL + "show/deletefavouriteshow";
        public static final String get_location = baseURL + "location/getalllocation";
        public static final String up_concerts = baseURL + "playlist/futureplaylist";
        public static final String showFav = baseURL + "show/addfavouriteshow";
        public static final String getShowList = baseURL + "ticket/getticket";
        public static final String trackFav = baseURL + "playlist/favourite";
        public static final String ticket_info = baseURL + "ticket/orderdetails";
        public static final String getNotification = baseURL + "user/notificationlist";
        public static final String getVersion = baseURL + "playlist/getdroidversion";

    }

    //shared preferancekeys
    public class SharedPreferenceKeys {
        public static final String IS_LOGGED_IN = "login_in";
        public static final String PERMISSION = "permission";
    }

    //api parameters
    public class RequestDataKey {
        public static final String lgn_id = "lgn_id";
        public static final String latitude = "latitude";
        public static final String longitude = "longitude";
        public static final String playlist_distance = "playlist_distance";
        public static final String distance_type = "distance_type";
        public static final String month_range = "month_range";
        public static final String utp_play_id = "utp_play_id";

        public static final String usr_name = "usr_name";
        public static final String lgn_email = "lgn_email";
        public static final String lgn_password = "lgn_password";
        public static final String confirm_password = "confirm_password";
        public static final String usr_imei_number = "usr_imei_number";
        public static final String usr_type = "usr_type";
        public static final String usr_gcm_number = "usr_gcm_number";
        public static final String usr_phone = "usr_phone";
        public static final String lgn_fgtpass_verifycode = "lgn_fgtpass_verifycode";
        public static final String new_password = "new_password";
        public static final String cnfm_password = "cnfm_password";
        public static final String lgn_fb_auth_id = "lgn_fb_auth_id";
        public static final String lgn_google_auth_id = "lgn_google_auth_id";
        public static final String usr_gender = "usr_gender";
        public static final String show_id = "show_id";
        public static final String utkt_lgn_id = "utkt_lgn_id";
        public static final String utkt_tkt_id = "utkt_tkt_id";
        public static final String utkt_qty = "utkt_qty";
        public static final String ticket_type_id = "ticket_type_id";
        public static final String od_id = "od_id";
        public static final String token = "token";
        public static final String art_id = "art_id";
        public static final String usrplay_lgn_id = "usrplay_lgn_id";
        public static final String utp_trk_id = "utp_trk_id";
        public static final String msf_lgn_id = "msf_lgn_id";
        public static final String msf_show_id = "msf_show_id";
        public static final String trk_id = "trk_id";
        public static final String utkt_id = "utkt_id";


    }

    public class PreftDataKey {
        public static final String usr_name = "usr_name";
        public static final String lgn_email = "lgn_email";
        public static final String lgn_id = "lgn_id";
        public static final String fav_playlist_id = "fav_playlist_id";
        public static final String fav_playlist_name = "fav_playlist_name";
        public static final String lgn_typ = "lgn_typ";
        public static final String latitude = "latitude";
        public static final String longitude = "longitude";
        public static final String play_name = "play_name";
        public static final String art_name = "art_name";
        public static final String trk_name = "trk_name";
        public static final String service_status = "service_status";
        public static final String refresh_fav_fromPlaylist = "refresh_fav_fromPlaylist";
        public static final String backgroundApp = "backgroundApp";
        public static final String songCalledFrom = "songCalledFrom";
        public static final String android_link = "android_link";
        public static final String INSTALL_DETAIL = "install_details";
        public static final String PREF_GCM_REGISTRATION_ID = "PREF_GCM_REGISTRATION_ID";

    }

    public class SetGetBundleData {

        public static final String show_title = "show_title";
        public static final String total_cost = "total_cost";
        public static final String OrderId = "OrderId";
        public static final String show_place = "show_place";
        public static final String show_date = "show_date";
        public static final String pay_msg = "pay_msg";
        public static final String pay_status = "pay_status";
        public static final String ticket_qty = "ticket_qty";
        public static final String show_id = "show_id";
        public static final String art_id = "art_id";
        public static final String art_name = "art_name";
        public static final String art_img = "art_img";
        public static final String play_id = "play_id";
        public static final String totaltrack = "totaltrack";
        public static final String play_name = "play_name";
        public static final String totalshow = "totalshow";
        public static final String from_loc = "from_loc";
        public static final String trk_id = "trk_id";
        public static final String utkt_id = "utkt_id";


    }

    //dialog message
    public class DialogMessage {
        public static final String PLEASE_WAIT = "Please wait...";
    }


    /**
     * Sends out a local broadcast that notifies all receivers to update
     * their respective UI elements.
     */
    public void broadcastUpdateUICommand(String[] updateFlags, String[] flagValues) {
        Intent intent = new Intent(UPDATE_UI_BROADCAST);
        for (int i = 0; i < updateFlags.length; i++) {
            intent.putExtra(updateFlags[i], flagValues[i]);
        }

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(CONTEXT);
        mLocalBroadcastManager.sendBroadcast(intent);

    }


}
